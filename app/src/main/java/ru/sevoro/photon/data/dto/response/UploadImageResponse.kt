package ru.sevoro.photon.data.dto.response

/**
 * @author Sergey Vorobyev
 */
data class UploadImageResponse(val image: String)