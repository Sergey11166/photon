package ru.sevoro.photon.data.transformers

import android.content.Context
import retrofit2.Response
import ru.sevoro.photon.dagger.AppComponent
import ru.sevoro.photon.data.error.ApiError
import ru.sevoro.photon.data.error.ForbiddenApiError
import ru.sevoro.photon.data.error.NetworkNotAvailableError
import ru.sevoro.photon.util.isNetworkAvailable
import rx.Observable

/**
 * @author Sergey Vorobyev.
 */

const val LAST_MODIFIED_HEADER = "Last-Modified"

open class RestCallTransformer<R>(val context: Context) : Observable.Transformer<Response<R>, R> {

    override fun call(response: Observable<Response<R>>): Observable<R> {
        return context.isNetworkAvailable()
                .flatMap { isAvailable ->
                    if (isAvailable) response else Observable.error<Response<R>>(NetworkNotAvailableError())
                }
                .flatMap<R> { resp ->
                    when (resp.code()) {
                        200 -> {
                            val entity: R? = resp.body()
                            saveLastModifiedHeader(resp?.headers()?.get(LAST_MODIFIED_HEADER), entity)
                            Observable.just(resp.body())
                        }
                        201, 202 -> Observable.just(resp.body())
                        304 -> Observable.empty()
                        403 -> Observable.error(ForbiddenApiError())
                        else -> Observable.error(ApiError(resp.code()))
                    }
                }
    }

    open fun saveLastModifiedHeader(lastModified: String?, entity: R?) {}

    open fun initDagger(component: AppComponent) {}
}
