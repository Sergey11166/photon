package ru.sevoro.photon.data

import ru.sevoro.photon.MIN_LOADED_COUNT_ON_SPLASH_SCREEN
import ru.sevoro.photon.UPDATE_DATA_INTERVAL
import ru.sevoro.photon.data.dto.realm.PhotoCardRealm
import ru.sevoro.photon.data.dto.response.PhotoCardResponse
import ru.sevoro.photon.data.transformers.PhotoCardTransformer
import ru.sevoro.photon.data.transformers.RestCallTransformer
import rx.Observable
import rx.Subscriber
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import java.util.concurrent.TimeUnit

/**
 * @author Sergey Vorobyev
 */
class DataManager(
        private val restService: RestService,
        private val restCallTransformer: RestCallTransformer<*>,
        private val photoCardTransformer: PhotoCardTransformer,
        private val realmManager: RealmManager,
        private val preferencesManager: PreferencesManager) {

    fun startUpdatePhotoCardsWithTimer() {
        Observable.interval(UPDATE_DATA_INTERVAL, UPDATE_DATA_INTERVAL, TimeUnit.SECONDS)
                .flatMap { updateAllPhotoCards() }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Subscriber<Any>() {
                    override fun onNext(t: Any?) {}
                    override fun onCompleted() {}
                    override fun onError(e: Throwable?) {
                        startUpdatePhotoCardsWithTimer()
                        e?.printStackTrace()
                    }
                })
    }

    fun updateAllPhotoCards(): Observable<*> {
        val limit = if (realmManager.count(PhotoCardRealm::class.java) == 0L)
            MIN_LOADED_COUNT_ON_SPLASH_SCREEN else 0

        @Suppress("UNCHECKED_CAST")
        val transformer: RestCallTransformer<List<PhotoCardResponse>> = if (limit == 0) photoCardTransformer
        else restCallTransformer as RestCallTransformer<List<PhotoCardResponse>>

        return restService.getAllPhotoCards(preferencesManager.getLastPhotoCardsUpdate(), limit)
                .compose(transformer)
                .flatMap { Observable.from(it) }
                .doOnNext {
                    if (it.isActive) {
                        realmManager.saveEntity(PhotoCardRealm(it))
                    } else {
                        realmManager.deleteEntityById(PhotoCardRealm::class.java, it.id)
                    }
                }
    }
}