package ru.sevoro.photon.data

import android.content.ContentValues
import android.content.Context
import android.graphics.Bitmap
import android.net.Uri
import android.os.Environment.getExternalStoragePublicDirectory
import android.provider.MediaStore
import rx.Emitter
import rx.Observable
import java.io.File
import java.io.FileOutputStream
import java.io.IOException

/**
 * @author Sergey Vorobyev
 */
class FileManager(private val context: Context) {

    fun writeBitmapToFile(bitmap: Bitmap, dir: File, fileName: String): Observable<Uri> {
        return Observable.create<Uri>({ emitter ->
            var fos: FileOutputStream? = null
            try {
                val file = File(dir, "$fileName.jpg")
                file.parentFile.mkdirs()
                fos = FileOutputStream(file)
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos)
                fos.flush()
                emitter.onNext(Uri.fromFile(file))
                emitter.onCompleted()
            } catch (e: IOException) {
                emitter.onError(e)
            } finally {
                fos?.close()
            }
        }, Emitter.BackpressureMode.BUFFER)
    }

    fun saveBitmapToCache(bitmap: Bitmap, imageId: String): Observable<Uri> {
        return writeBitmapToFile(bitmap, context.externalCacheDir, imageId)
    }

    fun saveBitmapToGallery(bitmap: Bitmap, imageId: String): Observable<Uri> {
        return writeBitmapToFile(bitmap,
                getExternalStoragePublicDirectory(context.getString(context.applicationInfo.labelRes)),
                imageId).doOnNext {
            val values = ContentValues()
            values.put(MediaStore.Images.Media.DATE_TAKEN, System.currentTimeMillis())
            values.put(MediaStore.Images.Media.MIME_TYPE, "image/jpeg")
            values.put(MediaStore.Images.Media.DATA, it.path)
            context.contentResolver.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values)
        }
    }

    @Throws(IOException::class)
    fun createFileInCache(fileName: String): File {
        return File(context.externalCacheDir, fileName)
    }
}
