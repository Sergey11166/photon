package ru.sevoro.photon.data.error

/**
 * @author Sergey Vorobyev
 */
class NetworkNotAvailableError : Throwable("Интернет недоступен, проверьте настройки сети")
