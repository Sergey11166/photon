package ru.sevoro.photon.data.dto.request

import ru.sevoro.photon.data.dto.Filter

/**
 * @author Sergey Vorobyev
 */
data class CreatePhotoCardRequest(
        val title: String,
        val album: String,
        val photo: String,
        val tags: List<String>,
        val filters: Filter)