package ru.sevoro.photon.data.dto.response

import com.squareup.moshi.Json
import ru.sevoro.photon.data.dto.Filter

/**
 * @author Sergey Vorobyev
 */
data class PhotoCardResponse(
        val id: String,
        val owner: String,
        val title: String,
        val photo: String,
        @Json(name = "active") val isActive: Boolean,
        val updated: String,
        val created: String,
        val views: Int,
        @Json(name = "favorits") val favorites: Int,
        val filters: Filter,
        val tags: List<String>)