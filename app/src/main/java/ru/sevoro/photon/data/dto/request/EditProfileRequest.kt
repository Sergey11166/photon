package ru.sevoro.photon.data.dto.request

/**
 * @author Sergey Vorobyev
 */
data class EditProfileRequest(
        val name: String? = null,
        val login: String? = null,
        val avatar: String? = null)