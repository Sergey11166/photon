package ru.sevoro.photon.data;

public class JobPriority {

    public static final int LOW = 100;
    public static final int MID = 500;
    public static final int HIGH = 1000;
}