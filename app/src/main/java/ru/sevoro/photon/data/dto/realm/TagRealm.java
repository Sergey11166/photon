package ru.sevoro.photon.data.dto.realm;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * @author Sergey Vorobyev
 */

public class TagRealm extends RealmObject {

    @PrimaryKey
    private String value = "";

    public TagRealm() {
    }

    public TagRealm(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
