package ru.sevoro.photon.data

import okhttp3.MultipartBody
import retrofit2.Response
import retrofit2.http.*
import ru.sevoro.photon.data.dto.request.*
import ru.sevoro.photon.data.dto.response.*
import rx.Observable

/**
 * @author Sergey Vorobyev
 */

const val QUERY_LIMIT = "limit"
const val HEADER_AUTHORISATION = "Authorization"
const val HEADER_IF_MODIFIED_SINCE = "If-Modified-Since"

interface RestService {

    @GET("photocard/list")
    fun getAllPhotoCards(
            @Header(value = HEADER_IF_MODIFIED_SINCE) lastEntityUpdate: String,
            @Query(value = QUERY_LIMIT) limit: Int
    ): Observable<Response<List<PhotoCardResponse>>>

    @POST("photocard/{photoCardId}/view")
    fun addPhotoCardView(@Path(value = "photoCardId") photoCardId: String): Observable<Response<IsSuccessResponse>>

    @GET("user/{userId}")
    fun getUser(
            @Header(value = HEADER_IF_MODIFIED_SINCE) lastEntityUpdate: String,
            @Path(value = "userId") userId: String
    ): Observable<Response<UserResponse>>

    @POST("user/SignUp")
    fun signUp(@Body request: SignUpRequest): Observable<Response<UserResponse>>

    @POST("user/SignIn")
    fun signIn(@Body request: SignInRequest): Observable<Response<UserResponse>>

    @POST("user/{userId}/favorite/{photoCardId}")
    fun addToFavorites(
            @Header(value = HEADER_AUTHORISATION) token: String,
            @Path(value = "userId") userId: String,
            @Path(value = "photoCardId") photoCardId: String
    ): Observable<Response<IsSuccessResponse>>

    @Multipart
    @POST("user/{userId}/image/upload")
    fun uploadImage(
            @Header(value = HEADER_AUTHORISATION) token: String,
            @Path(value = "userId") userId: String,
            @Part part: MultipartBody.Part): Observable<Response<UploadImageResponse>>

    @PUT("user/{userId}")
    fun editUser(
            @Header(value = HEADER_AUTHORISATION) token: String,
            @Path(value = "userId") userId: String,
            @Body request: EditProfileRequest): Observable<Response<UserResponse>>

    @POST("user/{userId}/album")
    fun createAlbum(
            @Header(value = HEADER_AUTHORISATION) token: String,
            @Path(value = "userId") userId: String,
            @Body request: CreateAlbumRequest
    ): Observable<Response<CreatedIdResponse>>

    @PUT("user/{userId}/album/{albumId}")
    fun editAlbum(
            @Header(value = HEADER_AUTHORISATION) token: String,
            @Path(value = "userId") userId: String,
            @Path(value = "albumId") albumId: String,
            @Body request: EditAlbumRequest): Observable<Response<AlbumResponse>>

    @POST("user/{userId}/photocard")
    fun createPhotoCard(
            @Header(value = HEADER_AUTHORISATION) token: String,
            @Path(value = "userId") userId: String,
            @Body request: CreatePhotoCardRequest
    ): Observable<Response<CreatedIdResponse>>

    @DELETE("user/{userId}/album/{albumId}")
    fun deleteAlbum(
            @Header(value = HEADER_AUTHORISATION) token: String,
            @Path(value = "userId") userId: String,
            @Path(value = "albumId") albumId: String): Observable<Response<Any>>

    @GET("photocard/tags")
    fun getTags(): Observable<Response<List<String>>>
}
