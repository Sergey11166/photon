package ru.sevoro.photon.data.error

/**
 * @author Sergey Vorobyev
 */
class ForbiddenApiError : ApiError("Неверный логин или пароль")
