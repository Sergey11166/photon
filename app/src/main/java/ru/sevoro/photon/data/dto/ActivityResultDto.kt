package ru.sevoro.photon.data.dto

import android.content.Intent

/**
 * @author Sergey Vorobyev
 */
data class ActivityResultDto(
        val requestCode: Int,
        val resultCode: Int,
        val intent: Intent?)
