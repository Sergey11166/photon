package ru.sevoro.photon.data.dto.response

import com.squareup.moshi.Json

/**
 * @author Sergey Vorobyev
 */
data class UserResponse(
        val id: String,
        val name: String,
        val login: String,
        @Json(name = "mail") val email: String,
        val avatar: String,
        val token: String,
        val albumCount: Int,
        val photocardCount: Int,
        var lastUpdate: String,
        val albums: List<AlbumResponse>)