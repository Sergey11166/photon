package ru.sevoro.photon.data.dto

/**
 * @author Sergey Vorobyev
 */
data class PermissionsResultDto(
        val requestCode: Int,
        val permissions: Array<String>,
        val grantResults: IntArray)
