package ru.sevoro.photon.data.dto.realm;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import ru.sevoro.photon.data.dto.response.UserResponse;
import ru.sevoro.photon.util.RealmExtentionsKt;

import static ru.sevoro.photon.data.PreferencesManagerKt.DEFAULT_LAST_MODIFIED;

/**
 * @author Sergey Vorobyev
 */

public class UserRealm extends RealmObject {

    @PrimaryKey
    private String id = "";
    private String username = "username";
    private String login = "login";
    private String avatar = "";
    private int albumCount;
    private int photoCardCount;
    private String lastUpdate = DEFAULT_LAST_MODIFIED;
    private boolean synced = true; // if synced with server after local edit
    private RealmList<AlbumRealm> albums = new RealmList<>();

    public UserRealm() {
    }

    public UserRealm(UserResponse response) {
        this.id = response.getId();
        this.username = response.getName();
        this.login = response.getLogin();
        this.avatar = response.getAvatar();
        this.albumCount = response.getAlbumCount();
        this.photoCardCount = response.getPhotocardCount();
        this.lastUpdate = response.getLastUpdate();
        this.albums = RealmExtentionsKt.toRealmList(response.getAlbums(), AlbumRealm::new);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public int getAlbumCount() {
        return albumCount;
    }

    public void setAlbumCount(int albumCount) {
        this.albumCount = albumCount;
    }

    public int getPhotoCardCount() {
        return photoCardCount;
    }

    public void setPhotoCardCount(int photoCardCount) {
        this.photoCardCount = photoCardCount;
    }

    public String getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(String lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public boolean isSynced() {
        return synced;
    }

    public void setSynced(boolean synced) {
        this.synced = synced;
    }

    public RealmList<AlbumRealm> getAlbums() {
        return albums;
    }

    public void setAlbums(RealmList<AlbumRealm> albums) {
        this.albums = albums;
    }
}
