package ru.sevoro.photon.data.dto.response

import com.squareup.moshi.Json

/**
 * @author Sergey Vorobyev
 */
data class IsSuccessResponse(@Json(name = "success") val isSuccess: Boolean)