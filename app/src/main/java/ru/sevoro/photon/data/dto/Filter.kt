package ru.sevoro.photon.data.dto

/**
 * @author Sergey Vorobyev
 */

data class Filter(
        val dish: String,
        val nuances: List<String>,
        val decor: String,
        val temperature: String,
        val light: String,
        val lightDirection: String,
        val lightSource: String
)