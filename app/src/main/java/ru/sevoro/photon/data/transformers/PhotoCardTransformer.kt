package ru.sevoro.photon.data.transformers

import android.content.Context
import ru.sevoro.photon.data.PreferencesManager
import ru.sevoro.photon.data.dto.response.PhotoCardResponse

/**
 * @author Sergey Vorobyev
 */
class PhotoCardTransformer(
        context: Context,
        val preferencesManager: PreferencesManager
) : RestCallTransformer<List<PhotoCardResponse>>(context) {

    override fun saveLastModifiedHeader(lastModified: String?, entity: List<PhotoCardResponse>?) {
        preferencesManager.savePhotoCardsLastModified(lastModified)
    }
}