package ru.sevoro.photon.data.dto.request

/**
 * @author Sergey Vorobyev
 */
open class SignInRequest(val email: String, val password: String)