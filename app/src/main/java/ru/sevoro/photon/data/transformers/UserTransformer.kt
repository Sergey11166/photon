package ru.sevoro.photon.data.transformers

import android.content.Context
import ru.sevoro.photon.data.dto.response.UserResponse

/**
 * @author Sergey Vorobyev
 */

class UserTransformer(context: Context) : RestCallTransformer<UserResponse>(context) {

    override fun saveLastModifiedHeader(lastModified: String?, entity: UserResponse?) {
        if (lastModified != null) entity?.lastUpdate = lastModified
    }
}