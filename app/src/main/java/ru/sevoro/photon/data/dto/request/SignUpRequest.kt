package ru.sevoro.photon.data.dto.request

/**
 * @author Sergey Vorobyev
 */
data class SignUpRequest(
        val name: String,
        val login: String,
        val email: String,
        val password: String)
