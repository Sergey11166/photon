package ru.sevoro.photon.data

import io.realm.Realm
import io.realm.RealmObject
import ru.sevoro.photon.data.dto.realm.*
import ru.sevoro.photon.screen.home.SearchPhotoCardsSpecification
import rx.Observable

/**
 * @author Sergey Vorobyev
 */
class RealmManager {

    private val queryRealmInstance: Realm = Realm.getDefaultInstance()

    fun <T : RealmObject> saveEntity(entity: T) {
        val realm: Realm = Realm.getDefaultInstance()
        realm.executeTransaction { realm.insertOrUpdate(entity) }
        realm.close()
    }

    fun <T : RealmObject> deleteEntityById(entityRealmClass: Class<T>, id: String) {
        val realm = Realm.getDefaultInstance()

        val entity = realm.where<T>(entityRealmClass)
                .equalTo("id", id)
                .findFirst()

        if (entity != null) {
            realm.executeTransaction { entity.deleteFromRealm() }
            realm.close()
        }
    }

    fun <T : RealmObject> getEntityById(entityRealmClass: Class<T>, id: String): T? {
        var result: T? = null
        val realm = Realm.getDefaultInstance()

        val entity = realm.where<T>(entityRealmClass)
                .equalTo("id", id)
                .findFirst()
        if (entity != null) {
            result = getCopyFromRealm(entity)
        }
        realm.close()
        return result
    }

    fun <T : RealmObject> count(entityRealmClass: Class<T>): Long {
        val realm = Realm.getDefaultInstance()
        val count = realm.where(entityRealmClass).count()
        realm.close()
        return count
    }

    fun <T : RealmObject> getCopyFromRealm(source: T): T {
        val result: T
        val realm = Realm.getDefaultInstance()
        result = realm.copyFromRealm(source)
        realm.close()
        return result
    }

    fun getAllPhotoCardsHot(specification: SearchPhotoCardsSpecification): Observable<PhotoCardRealm> {
        val query = queryRealmInstance.where(PhotoCardRealm::class.java)

        if (specification.title.isNotEmpty()) {
            if (specification.tags.isNotEmpty()) {
                query
                        .contains("titleLowerCase", specification.title.toLowerCase())
                        .or()
                        .contains("tags.value", specification.title.toLowerCase())
                        .or()
                        .`in`("tags.value", specification.tags.toTypedArray())
            } else {
                query
                        .contains("titleLowerCase", specification.title.toLowerCase())
                        .or()
                        .contains("tags.value", specification.title.toLowerCase())
            }
        }

        if (specification.tags.isNotEmpty()) query.`in`("tags.value", specification.tags.toTypedArray())
        if (specification.dish.isNotEmpty()) query.equalTo("dish", specification.dish)
        //if (specification.nuances.isNotEmpty()) query.`in`("nuances.value", specification.nuances.toTypedArray()) // entity have least one of specification.nuances
        if (specification.nuances.isNotEmpty()) specification.nuances.forEach { query.equalTo("nuances.value", it) } // entity have all of specification.nuances
        if (specification.decor.isNotEmpty()) query.equalTo("decor", specification.decor)
        if (specification.temperature.isNotEmpty()) query.equalTo("temperature", specification.temperature)
        if (specification.light.isNotEmpty()) query.equalTo("light", specification.light)
        if (specification.lightDirection.isNotEmpty()) query.equalTo("lightDirection", specification.lightDirection)
        if (specification.lightSource.isNotEmpty()) query.equalTo("lightSource", specification.lightSource)

        return query.findAllAsync()
                .asObservable()
                .filter { it.isLoaded }
                .flatMap { Observable.from(it) }
    }

    fun addAlbumToUser(userId: String, albumId: String) {
        val album = getEntityById(AlbumRealm::class.java, albumId)
        if (album != null) {
            val realm = Realm.getDefaultInstance()
            realm.executeTransaction {
                val user = getEntityById(UserRealm::class.java, userId)
                user?.albums?.add(album)
                realm.insertOrUpdate(user)
            }
            realm.close()
        } else {
            throw IllegalArgumentException("album with id $albumId not found")
        }
    }

    fun addToFavorite(photoCardId: String, currentUserId: String) {
        addToAlbum(photoCardId, getEntityById(UserRealm::class.java, currentUserId)
                ?.albums
                ?.filter { it.isFavorite }
                ?.get(0)?.id!!)
    }

    fun addToAlbum(photoCardId: String, albumId: String) {
        val realm = Realm.getDefaultInstance()
        realm.executeTransaction {
            val photoCard = getEntityById(PhotoCardRealm::class.java, photoCardId)
            photoCard?.favorites?.inc()

            val album = getEntityById(AlbumRealm::class.java, albumId)
            album?.photoCards?.add(photoCard)

            realm.insertOrUpdate(album)
        }
        realm.close()
    }

    fun deleteFromAlbum(photoCardId: String, albumId: String) {
        val realm: Realm = Realm.getDefaultInstance()
        realm.executeTransaction {
            val album = getEntityById(AlbumRealm::class.java, albumId)
            album?.photoCards?.remove(getEntityById(PhotoCardRealm::class.java, photoCardId))
            realm.insertOrUpdate(album)
        }
        realm.close()
    }

    fun isFavorite(photoCardId: String, currentUserId: String): Boolean? {
        return getEntityById(UserRealm::class.java, currentUserId)
                ?.albums
                ?.filter { it.isFavorite }
                ?.get(0)?.photoCards
                ?.filter { it.id == photoCardId }
                ?.isNotEmpty()
    }

    fun updateAvatar(userId: String, newAvatarPath: String) {
        val realm: Realm = Realm.getDefaultInstance()
        realm.executeTransaction {
            val user = getEntityById(UserRealm::class.java, userId)
            user?.avatar = newAvatarPath
            realm.insertOrUpdate(user)
        }
        realm.close()
    }

    fun updateUserInfo(userId: String, username: String, login: String) {
        val realm: Realm = Realm.getDefaultInstance()
        realm.executeTransaction {
            val user = getEntityById(UserRealm::class.java, userId)
            user?.username = username
            user?.login = login
            user?.isSynced = false
            realm.insertOrUpdate(user)
        }
        realm.close()
    }

    fun updateAlbumInfo(albumId: String, title: String, description: String) {
        val realm: Realm = Realm.getDefaultInstance()
        realm.executeTransaction {
            val album = getEntityById(AlbumRealm::class.java, albumId)
            album?.title = title
            album?.description = description
            album?.isSynced = false
            realm.insertOrUpdate(album)
        }
        realm.close()
    }

    fun deleteAlbum(albumId: String) {
        val realm = Realm.getDefaultInstance()
        realm.executeTransaction {
            val album = getEntityById(AlbumRealm::class.java, albumId)
            album?.isDeleted = true
            realm.insertOrUpdate(album)
        }
        realm.close()
    }

    fun getAllTags() = queryRealmInstance.where(TagRealm::class.java).findAll().toList()

    fun getAllPromts() = queryRealmInstance.where(PromtRealm::class.java).findAll().toList()
}
