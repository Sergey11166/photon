package ru.sevoro.photon.data.dto.response

import com.squareup.moshi.Json

/**
 * @author Sergey Vorobyev
 */
data class AlbumResponse(
        val id: String,
        val owner: String,
        val title: String,
        val views: Int,
        @Json(name = "active") val isActive: Boolean,
        @Json(name = "favorits") val favorites: Int,
        val isFavorite: Boolean,
        val description: String,
        @Json(name = "photocards") val photoCards: List<PhotoCardResponse>)