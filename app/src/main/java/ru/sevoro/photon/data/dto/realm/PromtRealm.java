package ru.sevoro.photon.data.dto.realm;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * @author Sergey Vorobyev
 */

public class PromtRealm extends RealmObject {

    @PrimaryKey
    private String value = "";

    public PromtRealm() {
    }

    public PromtRealm(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
