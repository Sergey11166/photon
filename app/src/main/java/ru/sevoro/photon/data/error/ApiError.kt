package ru.sevoro.photon.data.error

/**
 * @author Sergey Vorobyev
 */
open class ApiError : Throwable {

    var code: Int = 0

    constructor(statusCode: Int) : this(
            when (statusCode) {
                in 400..499 -> "Ошибка клиента $statusCode"
                in 500..599 -> "Ошибка сервера $statusCode"
                else -> "Неизвестная ошибка $statusCode"
            }) {
        code = statusCode
    }

    constructor(message: String) : super(message)
}