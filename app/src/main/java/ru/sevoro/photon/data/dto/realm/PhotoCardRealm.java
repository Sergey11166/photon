package ru.sevoro.photon.data.dto.realm;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import ru.sevoro.photon.data.dto.Filter;
import ru.sevoro.photon.data.dto.response.PhotoCardResponse;
import ru.sevoro.photon.util.RealmExtentionsKt;

import static ru.sevoro.photon.data.PreferencesManagerKt.DEFAULT_LAST_MODIFIED;

/**
 * @author Sergey Vorobyev
 */

public class PhotoCardRealm extends RealmObject {

    @PrimaryKey
    private String id = "";
    private String owner = "";
    private String title = "";
    private String titleLowerCase = ""; // for searching by title ignore case
    private String photo = "";
    private int views;
    private int favorites;
    private String updated = DEFAULT_LAST_MODIFIED;
    private String created = DEFAULT_LAST_MODIFIED;
    private RealmList<TagRealm> tags = new RealmList<>();
    private String dish = "";
    private RealmList<RealmString> nuances = new RealmList<>();
    private String decor = "";
    private String temperature = "";
    private String light = "";
    private String lightDirection = "";
    private String lightSource = "";

    public PhotoCardRealm() {
    }

    public PhotoCardRealm(PhotoCardResponse response) {
        this.id = response.getId();
        this.owner = response.getOwner();
        this.title = response.getTitle();
        this.titleLowerCase = response.getTitle().toLowerCase();
        this.photo = response.getPhoto();
        this.views = response.getViews();
        this.favorites = response.getFavorites();
        this.updated = response.getUpdated();
        this.created = response.getCreated();
        this.tags = RealmExtentionsKt.toRealmList(response.getTags(),
                tag -> new TagRealm(tag.startsWith("#") ? tag.substring(1) : tag));
        Filter filter = response.getFilters();
        //noinspection ConstantConditions
        if (filter != null) {
            this.dish = response.getFilters().getDish();
            this.nuances = RealmExtentionsKt.toRealmList(response.getFilters().getNuances(), RealmString::new);
            this.decor = response.getFilters().getDecor();
            this.temperature = response.getFilters().getTemperature();
            this.light = response.getFilters().getLight();
            this.lightDirection = response.getFilters().getLightDirection();
            this.lightSource = response.getFilters().getLightSource();
        }
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitleLowerCase() {
        return titleLowerCase;
    }

    public void setTitleLowerCase(String titleLowerCase) {
        this.titleLowerCase = titleLowerCase;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public int getViews() {
        return views;
    }

    public void setViews(int views) {
        this.views = views;
    }

    public int getFavorites() {
        return favorites;
    }

    public void setFavorites(int favorites) {
        this.favorites = favorites;
    }

    public String getUpdated() {
        return updated;
    }

    public void setUpdated(String updated) {
        this.updated = updated;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public RealmList<TagRealm> getTags() {
        return tags;
    }

    public void setTags(RealmList<TagRealm> tags) {
        this.tags = tags;
    }

    public String getDish() {
        return dish;
    }

    public void setDish(String dish) {
        this.dish = dish;
    }

    public RealmList<RealmString> getNuances() {
        return nuances;
    }

    public void setNuances(RealmList<RealmString> nuances) {
        this.nuances = nuances;
    }

    public String getDecor() {
        return decor;
    }

    public void setDecor(String decor) {
        this.decor = decor;
    }

    public String getTemperature() {
        return temperature;
    }

    public void setTemperature(String temperature) {
        this.temperature = temperature;
    }

    public String getLight() {
        return light;
    }

    public void setLight(String light) {
        this.light = light;
    }

    public String getLightDirection() {
        return lightDirection;
    }

    public void setLightDirection(String lightDirection) {
        this.lightDirection = lightDirection;
    }

    public String getLightSource() {
        return lightSource;
    }

    public void setLightSource(String lightSource) {
        this.lightSource = lightSource;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PhotoCardRealm that = (PhotoCardRealm) o;
        return id.equals(that.id);
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }
}
