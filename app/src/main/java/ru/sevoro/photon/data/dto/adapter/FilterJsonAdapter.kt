package ru.sevoro.photon.data.dto.adapter

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import ru.sevoro.photon.data.dto.Filter

/**
 * @author Sergey Vorobyev
 */

@Suppress("UNUSED")
class FilterJsonAdapter {

    @FromJson
    fun fromJson(json: FilterResJson): Filter = Filter(
            dish = json.dish,
            nuances = json.nuances.split(", "),
            decor = json.decor,
            temperature = json.temperature,
            light = json.light,
            lightDirection = json.lightDirection,
            lightSource = json.lightSource
    )

    @ToJson
    fun toJson(response: Filter): FilterResJson = FilterResJson(
            dish = response.dish,
            nuances = response.nuances.toString().substring(1, response.nuances.toString().length - 1),
            decor = response.decor,
            temperature = response.temperature,
            light = response.light,
            lightDirection = response.lightDirection,
            lightSource = response.lightSource
    )
}

class FilterResJson(
        val dish: String = "",
        val nuances: String = "",
        val decor: String = "",
        val temperature: String = "",
        val light: String = "",
        val lightDirection: String = "",
        val lightSource: String = ""
)