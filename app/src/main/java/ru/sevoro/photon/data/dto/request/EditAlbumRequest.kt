package ru.sevoro.photon.data.dto.request

/**
 * @author Sergey Vorobyev
 */
data class EditAlbumRequest(
        val title: String? = null,
        val description: String? = null)