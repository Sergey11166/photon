package ru.sevoro.photon.data.dto.realm;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import ru.sevoro.photon.data.dto.realm.PhotoCardRealm;
import ru.sevoro.photon.data.dto.response.AlbumResponse;
import ru.sevoro.photon.util.RealmExtentionsKt;

/**
 * @author Sergey Vorobyev
 */

public class AlbumRealm extends RealmObject {

    @PrimaryKey
    private String id = "";
    private String owner = "";
    private String title = "";
    private int views;
    private int favorites;
    private boolean isFavorite;
    private String description = "";
    private boolean synced = true; // true if synced with server after local edit
    private boolean deleted; // true if deleted in local but and not deleted on server
    private RealmList<PhotoCardRealm> photoCards = new RealmList<>();

    public AlbumRealm() {
    }

    public AlbumRealm(AlbumResponse response) {
        this.id = response.getId();
        this.owner = response.getOwner();
        this.title = response.getTitle();
        this.views = response.getViews();
        this.favorites = response.getFavorites();
        this.isFavorite = response.isFavorite();
        this.description = response.getDescription();
        this.photoCards = RealmExtentionsKt.toRealmList(response.getPhotoCards(), PhotoCardRealm::new);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getViews() {
        return views;
    }

    public void setViews(int views) {
        this.views = views;
    }

    public int getFavorites() {
        return favorites;
    }

    public void setFavorites(int favorites) {
        this.favorites = favorites;
    }

    public boolean isFavorite() {
        return isFavorite;
    }

    public void setFavorite(boolean favorite) {
        isFavorite = favorite;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isSynced() {
        return synced;
    }

    public void setSynced(boolean synced) {
        this.synced = synced;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public RealmList<PhotoCardRealm> getPhotoCards() {
        return photoCards;
    }

    public void setPhotoCards(RealmList<PhotoCardRealm> photoCards) {
        this.photoCards = photoCards;
    }
}
