package ru.sevoro.photon.data.dto.request

/**
 * @author Sergey Vorobyev
 */
data class CreateAlbumRequest(val title: String, val description: String)