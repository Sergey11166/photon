package ru.sevoro.photon.data.dto.response

/**
 * @author Sergey Vorobyev
 */
data class CreatedIdResponse(val id: String)