package ru.sevoro.photon.data

import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager

/**
 * @author Sergey Vorobyev
 */
const val PREF_KEY_PHOTO_CARDS_LAST_MODIFIED = "PREF_KEY_PHOTO_CARDS_LAST_MODIFIED"
const val PREF_KEY_CURRENT_USER_ID = "PREF_KEY_CURRENT_USER_ID"
const val PREF_KEY_AUTH_TOKEN = "PREF_KEY_AUTH_TOKEN"
const val DEFAULT_LAST_MODIFIED = "Thu, 01 Jan 1970 00:00:00 GMT"

class PreferencesManager(context: Context) {

    private val preferences: SharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)

    fun saveToken(token: String) {
        saveString(PREF_KEY_AUTH_TOKEN, token)
    }

    fun getToken(): String {
        return preferences.getString(PREF_KEY_AUTH_TOKEN, "")
    }

    fun deleteToken() {
        deleteString(PREF_KEY_AUTH_TOKEN)
        deleteString(PREF_KEY_CURRENT_USER_ID)
    }

    fun saveCurrentUserId(userId: String) {
        saveString(PREF_KEY_CURRENT_USER_ID, userId)
    }

    fun getCurrentUserId() = preferences.getString(PREF_KEY_CURRENT_USER_ID, "")!!//"5936b4e4569f847fd20b4f93")!!

    fun savePhotoCardsLastModified(lastModified: String?) {
        saveString(PREF_KEY_PHOTO_CARDS_LAST_MODIFIED, lastModified)
    }

    fun getLastPhotoCardsUpdate(): String {
        return preferences.getString(PREF_KEY_PHOTO_CARDS_LAST_MODIFIED, DEFAULT_LAST_MODIFIED)!!
    }

    private fun saveString(key: String, value: String?) {
        val editor = preferences.edit()
        editor.putString(key, value)
        editor.apply()
    }

    private fun deleteString(key: String) {
        val editor = preferences.edit()
        editor.remove(key)
        editor.apply()
    }
}
