package ru.sevoro.photon.data.dto

/**
 * @author Sergey Vorobyev
 */

// dish
val MEAT: String = "meat"
val FISH: String = "fish"
val VEGETABLES: String = "vegetables"
val FRUIT: String = "fruit"
val CHEESE: String = "cheese"
val DESSERTS: String = "dessert"
val DRINKS: String = "drinks"

// nuances
val RED: String = "red"
val ORANGE: String = "orange"
val YELLOW: String = "yellow"
val GREEN: String = "green"
val LIGHT_BLUE: String = "lightBlue"
val BLUE: String = "blue"
val VIOLET: String = "violet"
val BROWN: String = "brown"
val BLACK: String = "black"
val WHITE: String = "white"

// decor
val SIMPLE: String = "simple"
val HOLIDAY: String = "holiday"

// temperature
val HOT: String = "hot"
val MIDDLE: String = "middle"
val COLD: String = "cold"

// light
val NATURAL: String = "natural"
val SYNTHETIC: String = "synthetic"
val MIXED: String = "mixed"

// lightDirection
val DIRECT: String = "direct"
val BACK_LIGHT: String = "backlight"
val SIDE_LIGHT: String = "sideLight"

// lightSource
val ONE: String = "one"
val TWO: String = "two"
val THREE: String = "three"