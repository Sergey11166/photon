package ru.sevoro.photon

import android.os.Bundle
import android.support.multidex.MultiDexApplication
import android.support.v7.app.AppCompatDelegate
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.crash.FirebaseCrash
import com.jakewharton.picasso.OkHttp3Downloader
import com.squareup.picasso.Picasso
import io.realm.Realm
import mortar.MortarScope
import mortar.bundler.BundleServiceRunner
import ru.sevoro.photon.dagger.*
import ru.sevoro.photon.flow_mortar.ScreenScoper
import ru.sevoro.photon.screen.root.DaggerRootActivity_RootComponent
import ru.sevoro.photon.screen.root.RootActivity
import ru.sevoro.photon.screen.root.RootActivity.RootComponent
import ru.sevoro.photon.util.SERVICE_NAME
import java.util.*

/**
 * @author Sergey Vorobyev
 */
class App : MultiDexApplication() {

    private var rootScope: MortarScope? = null

    override fun getSystemService(name: String): Any =
            if (rootScope != null && rootScope!!.hasService(name))
                rootScope!!.getService(name)
            else
                super.getSystemService(name)


    override fun onCreate() {
        super.onCreate()
        instance = this
        fireBaseAnalytics = FirebaseAnalytics.getInstance(this)
        initDaggerComponents()
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)
        initLibraries()

        val bundle = Bundle()
        bundle.putString(FirebaseAnalytics.Param.START_DATE, Date(System.currentTimeMillis()).toString())
        fireBaseAnalytics.logEvent(FirebaseAnalytics.Event.APP_OPEN, bundle)
        FirebaseCrash.log("App started")
        FirebaseCrash.report(Exception("My first Firebase non-fatal error on Android"))
    }

    private fun initLibraries() {
        Realm.init(this)
        try {
            Picasso.setSingletonInstance(Picasso.Builder(this)
                    .downloader(OkHttp3Downloader(this))
                    //.indicatorsEnabled(BuildConfig.DEBUG)
                    .build())
        } catch (e: IllegalStateException) {
            // ignore
            // to suppress java.lang.IllegalStateException: Singleton instance already exists
        }
    }

    private fun initDaggerComponents() {
        appComponent = DaggerAppComponent.builder()
                .networkModule(NetworkModule())
                .localModule(LocalModule())
                .appModule(AppModule(this))
                .build()

        val rootComponent: RootComponent = DaggerRootActivity_RootComponent.builder()
                .appComponent(appComponent)
                .rootModule(RootModule())
                .build()

        rootScope = MortarScope.buildRootScope()
                .withService(SERVICE_NAME, appComponent)
                .build("Root")

        val rootActivityScope: MortarScope = rootScope!!.buildChild()
                .withService(SERVICE_NAME, rootComponent)
                .withService(BundleServiceRunner.SERVICE_NAME, BundleServiceRunner())
                .build(RootActivity::class.java.name)

        ScreenScoper.registerScope(rootScope)
        ScreenScoper.registerScope(rootActivityScope)
    }

    companion object {
        lateinit var instance: App
        lateinit var appComponent: AppComponent
        lateinit var fireBaseAnalytics: FirebaseAnalytics
    }
}
