package ru.sevoro.photon.util

import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.provider.MediaStore
import android.provider.Settings
import android.support.v4.content.FileProvider
import ru.sevoro.photon.BuildConfig
import java.io.File

/**
 * @author Sergey Vorobyev
 */

/**
 * Open system settings of this app
 */
fun Activity.goToSettingsApp(requestCode: Int) {
    val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
            Uri.parse("package:" + this.packageName))
    this.startActivityForResult(intent, requestCode)
}

/**
 * Open camera app to take photo
 *
 * @param tempFile    File to write items from camera
 * *
 * @param requestCode Code for [Activity.onActivityResult]
 */
fun Activity.goToCameraApp(tempFile: File, requestCode: Int) {
    val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
    val photoUri = FileProvider.getUriForFile(this, BuildConfig.APPLICATION_ID + ".file_provider", tempFile)
    intent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri)

    val resolveInfoList = this.packageManager.queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY)
    resolveInfoList
            .map { it.activityInfo.packageName }
            .forEach {
                this.grantUriPermission(it, photoUri,
                        Intent.FLAG_GRANT_WRITE_URI_PERMISSION or Intent.FLAG_GRANT_READ_URI_PERMISSION)
            }

    this.startActivityForResult(intent, requestCode)
}

/**
 * Open gallery app to choose image
 *
 * @param requestCode Code for [Activity.onActivityResult]
 */
fun Activity.goToGalleryApp(requestCode: Int) {
    val i = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
    i.type = "image/*"
    this.startActivityForResult(i, requestCode)
}