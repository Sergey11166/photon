package ru.sevoro.photon.util

import android.content.Context

/**
 * @author Sergey Vorobyev
 */

val SERVICE_NAME = "DAGGER_SERVICE"

fun <T> Context.getComponent(): T {
    @Suppress("UNCHECKED_CAST")
    return this.getSystemService(SERVICE_NAME) as T
}
