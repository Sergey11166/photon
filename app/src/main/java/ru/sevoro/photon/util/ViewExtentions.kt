package ru.sevoro.photon.util

import android.view.View
import android.view.ViewTreeObserver

/**
 * @author Sergey Vorobyev
 */

fun View.waiteForMeasure(callback: (View, Int, Int) -> Unit) {
    val width = this.width
    val height = this.height

    if (width > 0 && height > 0) {
        callback(this, width, height)
    } else {
        this.viewTreeObserver.addOnPreDrawListener(object : ViewTreeObserver.OnPreDrawListener {
            override fun onPreDraw(): Boolean {
                val observer = this@waiteForMeasure.viewTreeObserver
                if (observer.isAlive) {
                    observer.removeOnPreDrawListener(this)
                }
                callback(this@waiteForMeasure,
                        this@waiteForMeasure.width, this@waiteForMeasure.height)
                return true
            }
        })
    }
}