package ru.sevoro.photon.util

import android.content.Context
import android.support.v7.app.AppCompatDelegate
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageView
import de.hdodenhof.circleimageview.CircleImageView

/**
 * @author Sergey Vorobyev
 */

class InflaterFactory2(private val appCompatDelegate: AppCompatDelegate) : LayoutInflater.Factory2 {

    override fun onCreateView(parent: View?, name: String?, context: Context, attrs: AttributeSet): View? {
        if (name.equals(ImageView::class.java.simpleName, ignoreCase = true)) {
            val view: View? = overrideImageViewIfNeed(context, attrs)
            if (view != null) return view
        }

        return appCompatDelegate.createView(parent, name, context, attrs)
    }

    override fun onCreateView(name: String?, context: Context, attrs: AttributeSet): View? {
        return onCreateView(null, name, context, attrs)
    }

    private fun overrideImageViewIfNeed(context: Context, attributeSet: AttributeSet): View? {
        val isCircular = attributeSet.getAttributeBooleanValue(
                "http://schemas.android.com/apk/res-auto", "circular", false)
        if (isCircular) return CircleImageView(context, attributeSet)
        return null
    }
}