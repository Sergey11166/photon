package ru.sevoro.photon.util

import android.text.TextWatcher

/**
 * @author Sergey Vorobyev.
 */
abstract class SimpleTextWatcher : TextWatcher {
    override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
    override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
}
