package ru.sevoro.photon.util

import io.realm.RealmList
import io.realm.RealmObject

/**
 * @author Sergey Vorobyev
 */

fun <E, R : RealmObject> List<E>.toRealmList(transform: (E) -> R): RealmList<R> {
    val result: RealmList<R> = RealmList()
    this.indices.mapTo(result) { transform(this[it]) }
    return result
}