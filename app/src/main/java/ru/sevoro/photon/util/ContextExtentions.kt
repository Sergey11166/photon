package ru.sevoro.photon.util

import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Context.WINDOW_SERVICE
import android.content.Intent
import android.net.ConnectivityManager
import android.net.Uri
import android.provider.MediaStore
import android.support.annotation.StringRes
import android.util.DisplayMetrics
import android.view.WindowManager
import android.widget.Toast
import ru.sevoro.photon.R
import rx.Observable

/**
 * @author Sergey Vorobyev
 */

/**
 * Go to sourceUrl
 *
 * @param sourceUrl Target sourceUrl
 */
fun Context.goToUrl(sourceUrl: String) {
    var url: String? = null
    if (!sourceUrl.startsWith("http://") && !sourceUrl.startsWith("https://"))
        url = "http://" + sourceUrl
    val i = Intent(Intent.ACTION_VIEW, Uri.parse(url))
    try {
        this.startActivity(i)
    } catch (ex: ActivityNotFoundException) {
        this.showToast("Browser not found")
    }
}

/**
 * Send email
 *
 * @param address Target email addresses
 */
fun Context.sendEmail(address: String) {
    val intent = Intent(Intent.ACTION_SENDTO)
    intent.data = Uri.parse("mailto:")
    intent.putExtra(Intent.EXTRA_EMAIL, arrayOf(address))
    try {
        this.startActivity(intent)
    } catch (ex: android.content.ActivityNotFoundException) {
        this.showToast("Email app not found")
    }
}

fun Context.shareImage(localUri: Uri) {
    val shareIntent = Intent(Intent.ACTION_SEND)
    shareIntent.putExtra(Intent.EXTRA_STREAM, localUri)
    shareIntent.type = "image/*"
    this.startActivity(Intent.createChooser(shareIntent, this.getString(R.string.photocard_menu_share)))
}

/**
 * Open phone app
 *
 * @param number  Target phone number
 */
fun Context.openPhoneApp(number: String) {
    val i = Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", number, null))
    try {
        this.startActivity(i)
    } catch (ex: android.content.ActivityNotFoundException) {
        this.showToast("Phone app not found")
    }
}

fun Context.getDensity(): Float {
    val displayMetrics = DisplayMetrics()
    (this.getSystemService(WINDOW_SERVICE) as WindowManager).defaultDisplay.getMetrics(displayMetrics)
    return displayMetrics.density
}

fun Context.showToast(message: String) {
    Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
}

fun Context.showToast(@StringRes message: Int) {
    Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
}

fun Context.isNetworkAvailable(): Observable<Boolean> {
    val cm = this.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    val networkInfo = cm.activeNetworkInfo
    return Observable.just(networkInfo != null && networkInfo.isConnectedOrConnecting)
}

fun Context.filePathFromUri(uri: Uri): String {
    var filePath: String = ""
    if ("content" == uri.scheme) {
        val cursor = this.contentResolver
                .query(uri,
                        arrayOf(MediaStore.Images.ImageColumns.DATA),
                        null,
                        null,
                        null)
        if (cursor != null) {
            cursor.moveToFirst()
            filePath = cursor.getString(0)
            cursor.close()
        }
    } else {
        filePath = uri.path
    }
    return filePath
}