@file:Suppress(names = "UNUSED")

package ru.sevoro.photon.util

import android.databinding.BindingAdapter
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.support.annotation.ColorInt
import android.support.annotation.StringRes
import android.support.design.widget.TextInputLayout
import android.support.v4.graphics.drawable.DrawableCompat
import android.text.TextUtils
import android.widget.ImageView
import android.widget.TextView
import com.squareup.picasso.Callback
import com.squareup.picasso.NetworkPolicy.OFFLINE
import com.squareup.picasso.Picasso
import de.hdodenhof.circleimageview.CircleImageView

/**
 * @author Sergey Vorobyev.
 */
@BindingAdapter("textRes")
fun bindTextRes(view: TextView, @StringRes resId: Int) {
    if (resId < 1) {
        return
    }
    view.setText(resId)
}

@BindingAdapter("hasError", "errorMessage")
fun bindErrorTextInputLayout(view: TextInputLayout, hasError: Boolean, errorMessage: String) {
    view.isErrorEnabled = hasError
    view.error = if (hasError) errorMessage else null
}

@BindingAdapter("civ_border_color")
fun bindBorderColor(view: ImageView?, @ColorInt color: Int?) {
    if (view != null && view is CircleImageView && color != null) {
        view.borderColor = color
    }
}

@BindingAdapter("tint")
fun bindTint(view: ImageView?, @ColorInt color: Int?) {
    if (view == null || color == null) return
    val drawable = view.drawable
    if (drawable != null) {
        DrawableCompat.setTint(drawable, color)
        view.setImageDrawable(drawable)
    }
}

@BindingAdapter(
        "picassoSrc",
        "picassoPlaceHolderDrawable",
        "picassoPlaceHolderColor",
        "picassoScale",
        requireAll = false)
fun loadImage(view: ImageView?,
              src: String?,
              placeHolderDrawable: Drawable?,
              @ColorInt placeHolderColor: Int?,
              scale: String?) {

    if (src == null || view == null) return

    val imageUri = if (!src.isEmpty()) src else "null"

    val srcResId = if (imageUri.startsWith("drawable"))
        view.resources.getIdentifier(imageUri.split("/")[1], "drawable", view.context.packageName) else 0

    if (srcResId != 0) {
        view.setImageResource(srcResId)
        return
    }

    val placeHolder = placeHolderDrawable ?:
            if (placeHolderColor != null) ColorDrawable(placeHolderColor)
            else null

    view.waiteForMeasure { _, width, height ->
        val creator = Picasso.with(view.context).load(imageUri)
        if (!TextUtils.isEmpty(scale) && (width > 0 || height > 0)) {
            creator.resize(width, height)
            when (scale) {
                "centerCrop" -> creator.centerCrop()
                "centerInside" -> creator.centerInside()
                "fit" -> creator.fit()
                else -> throw RuntimeException("wrong scale parameter")
            }
        }
        creator
                .placeholder(placeHolder)
                .networkPolicy(OFFLINE)
                .into(view, object : Callback {
                    override fun onSuccess() = L.d("load image from cache success")

                    override fun onError() {
                        Picasso.with(view.context)
                                .load(imageUri)
                                .placeholder(placeHolder)
                                .error(placeHolder)
                                .into(view, object : Callback {
                                    override fun onSuccess() = L.d("load image from network or resources success")
                                    override fun onError() = L.e("load image from network or resources error")
                                })
                    }
                })
    }
}
