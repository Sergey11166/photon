package ru.sevoro.photon.util

import android.support.v7.util.SortedList

/**
 * @author Sergey Vorobyev
 */
abstract class SimpleSortedListCallback<T> : SortedList.Callback<T>() {
    override fun onChanged(position: Int, count: Int) {}
    override fun areContentsTheSame(oldItem: T, newItem: T) = oldItem == newItem
    override fun areItemsTheSame(item1: T, item2: T) = item1 == item2
    override fun onInserted(position: Int, count: Int) {}
    override fun onRemoved(position: Int, count: Int) {}
    override fun onMoved(fromPosition: Int, toPosition: Int) {}
}
