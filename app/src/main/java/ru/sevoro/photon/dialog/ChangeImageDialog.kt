package ru.sevoro.photon.dialog

import android.app.AlertDialog
import android.app.Dialog
import android.app.DialogFragment
import android.os.Bundle
import ru.sevoro.photon.R

/**
 * @author Sergey Vorobyev
 */
fun getInstanceChangeImageDialog(clickListener: (item: Int) -> Unit): ChangeImageDialog {
    val dialog = ChangeImageDialog()
    dialog.setOnClickListener(clickListener)
    return dialog
}

class ChangeImageDialog : DialogFragment() {

    private var clickListener: (item: Int) -> Unit = {}

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        retainInstance = true
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val builder = AlertDialog.Builder(activity)
        val items = resources.getStringArray(R.array.items_change_image_dialog)
        builder.setItems(items, { _, item ->
            if (item == 3) {
                dismiss()
            } else {
                clickListener(item)
            }
        })

        val dialog = builder.create()
        dialog.setCanceledOnTouchOutside(false)
        return dialog
    }

    override fun onDestroyView() {
        //https://code.google.com/p/android/issues/detail?id=17423
        if (dialog != null && retainInstance) {
            dialog.setDismissMessage(null)
        }
        super.onDestroyView()
    }

    fun setOnClickListener(clickListener: (item: Int) -> Unit) {
        this.clickListener = clickListener
    }
}