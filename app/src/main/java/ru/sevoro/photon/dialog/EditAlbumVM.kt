package ru.sevoro.photon.dialog

import android.databinding.Bindable
import ru.sevoro.photon.BR
import ru.sevoro.photon.screen.base.BaseViewModel

/**
 * @author Sergey Vorobyev
 */

const val PATTERN_NEW_ALBUM_NAME = "^[^!@#\$%^&*()\\\\|=+-]*\$"

class EditAlbumVM : BaseViewModel() {

    @get:Bindable
    var name: String = ""
        set(value) {
            field = value
            notifyPropertyChanged(BR.name)
            isNameValid = name.length > 2 && name.matches(Regex(PATTERN_NEW_ALBUM_NAME))
        }

    @get:Bindable
    var isNameValid: Boolean = false
        set(value) {
            field = value
            notifyPropertyChanged(BR.nameValid)
        }

    @get:Bindable
    var description: String = ""
        set(value) {
            field = value
            notifyPropertyChanged(BR.description)
            isDescriptionValid = description.length in 3..399
        }

    @get:Bindable
    var isDescriptionValid: Boolean = false
        set(value) {
            field = value
            notifyPropertyChanged(BR.descriptionValid)
        }
}