package ru.sevoro.photon.dialog

import android.databinding.Bindable
import ru.sevoro.photon.BR

/**
 * @author Sergey Vorobyev
 */
const val PATTERN_LOGIN = "^[a-zA-Z0-9_]*\$"

class SignUpVM : SignInVM() {

    @get:Bindable
    var login: String = ""
        set(value) {
            field = value
            notifyPropertyChanged(BR.login)
            isLoginValid = !login.isEmpty() && login.matches(Regex(PATTERN_LOGIN))
        }

    @get:Bindable
    var isLoginValid: Boolean = false
        set(value) {
            field = value
            notifyPropertyChanged(BR.loginValid)
        }

    @get:Bindable
    var username: String = ""
        set(value) {
            field = value
            notifyPropertyChanged(BR.username)
            isUsernameValid = username.length > 2
        }

    @get:Bindable
    var isUsernameValid: Boolean = false
        set(value) {
            field = value
            notifyPropertyChanged(BR.usernameValid)
        }
}