package ru.sevoro.photon.dialog

import android.app.Dialog
import android.app.DialogFragment
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import ru.sevoro.photon.R
import ru.sevoro.photon.databinding.DialogEditProfileBinding


/**
 * @author Sergey Vorobyev
 */
fun getInstanceEditProfileDialog(clickListener: (view: View) -> Unit): EditProfileDialog {
    val dialog = EditProfileDialog()
    dialog.setOnClickListener(clickListener)
    return dialog
}

class EditProfileDialog : DialogFragment() {

    private var viewModel: EditProfileVM? = null
    private var onClickListener: (view: View) -> Unit = {}

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        retainInstance = true
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val binding = DialogEditProfileBinding.inflate(LayoutInflater.from(activity), null)
        binding.editProfileModel = viewModel

        val dialog = Dialog(activity, R.style.CustomDialogTheme)
        dialog.setCanceledOnTouchOutside(false)
        dialog.setContentView(binding.root)

        binding.okButton.setOnClickListener({ view -> onClickListener(view) })
        binding.cancelButton.setOnClickListener({ dialog.dismiss() })
        return dialog
    }

    override fun onDestroyView() {
        //https://code.google.com/p/android/issues/detail?id=17423
        if (dialog != null && retainInstance) {
            dialog.setDismissMessage(null)
        }
        super.onDestroyView()
    }

    fun setViewModel(vm: EditProfileVM) {
        viewModel = vm
    }

    fun setOnClickListener(clickListener: (view: View) -> Unit) {
        onClickListener = clickListener
    }
}