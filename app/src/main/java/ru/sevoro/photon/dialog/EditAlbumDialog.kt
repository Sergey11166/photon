package ru.sevoro.photon.dialog

import android.app.Dialog
import android.app.DialogFragment
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import ru.sevoro.photon.R
import ru.sevoro.photon.databinding.DialogEditAlbumBinding


/**
 * @author Sergey Vorobyev
 */
fun getInstanceNewAlbumDialog(title: String, clickListener: (view: View) -> Unit): NewAlbumDialog {
    val dialog = NewAlbumDialog()
    dialog.setTitle(title)
    dialog.setOnClickListener(clickListener)
    return dialog
}

class NewAlbumDialog : DialogFragment() {

    private var title: String = ""
    private var viewModel: EditAlbumVM? = null
    private var onClickListener: (view: View) -> Unit = {}

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        retainInstance = true
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val binding = DialogEditAlbumBinding.inflate(LayoutInflater.from(activity), null)
        binding.editAlbumModel = viewModel

        val dialog = Dialog(activity, R.style.CustomDialogTheme)
        dialog.setCanceledOnTouchOutside(false)
        dialog.setContentView(binding.root)

        binding.title.text = title
        binding.okButton.setOnClickListener({ view -> onClickListener(view) })
        binding.cancelButton.setOnClickListener({ dialog.dismiss() })
        return dialog
    }

    override fun onDestroyView() {
        //https://code.google.com/p/android/issues/detail?id=17423
        if (dialog != null && retainInstance) {
            dialog.setDismissMessage(null)
        }
        super.onDestroyView()
    }

    fun setTitle(title: String) {
        this.title = title
    }

    fun setViewModel(vm: EditAlbumVM) {
        viewModel = vm
    }

    fun setOnClickListener(clickListener: (view: View) -> Unit) {
        onClickListener = clickListener
    }
}