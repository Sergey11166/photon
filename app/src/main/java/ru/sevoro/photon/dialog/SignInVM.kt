package ru.sevoro.photon.dialog

import android.databinding.Bindable
import ru.sevoro.photon.BR
import ru.sevoro.photon.screen.base.BaseViewModel

/**
 * @author Sergey Vorobyev
 */
const val PATTERN_EMAIL = "^[\\w+.%\\-]{3,}@[a-zA-Z0-9][a-zA-Z0-9\\-]{1,64}(\\.[a-zA-Z0-9][a-zA-Z0-9\\-]{1,25})+$"
const val PATTERN_PASSWORD = "^[a-zA-Z0-9]*\$"

open class SignInVM : BaseViewModel() {

    @get:Bindable
    var email: String = ""
        set(value) {
            field = value
            notifyPropertyChanged(BR.email)
            isEmailValid = email.matches(Regex(PATTERN_EMAIL))
        }

    @get:Bindable
    var isEmailValid: Boolean = false
        set(value) {
            field = value
            notifyPropertyChanged(BR.emailValid)
        }

    @get:Bindable
    var password: String = ""
        set(value) {
            field = value
            notifyPropertyChanged(BR.password)
            isPasswordValid = password.length > 7 && password.matches(Regex(PATTERN_PASSWORD))
        }

    @get:Bindable
    var isPasswordValid: Boolean = false
        set(value) {
            field = value
            notifyPropertyChanged(BR.passwordValid)
        }
}