package ru.sevoro.photon.flow_mortar;


import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v4.view.animation.FastOutLinearInInterpolator;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;

import java.util.Collections;
import java.util.Map;

import flow.Direction;
import flow.Dispatcher;
import flow.KeyChanger;
import flow.State;
import flow.Traversal;
import flow.TraversalCallback;
import flow.TreeKey;
import kotlin.Unit;
import ru.sevoro.photon.R;
import ru.sevoro.photon.screen.base.AbsScreen;
import ru.sevoro.photon.util.ViewExtentionsKt;

/**
 * @author Sergey Vorobyev.
 */
public class TreeKeyDispatcher extends KeyChanger implements Dispatcher {

    private Activity activity;
    private Object inKey;
    private Object outKey;

    private FrameLayout container;

    public TreeKeyDispatcher(Activity activity) {
        this.activity = activity;
    }

    @Override
    public void dispatch(Traversal traversal, TraversalCallback callback) {
        Map<Object, Context> contexts;
        State inState = traversal.getState(traversal.destination.top());
        State outState = traversal.origin == null ? null : traversal.getState(traversal.origin.top());
        inKey = inState.getKey();
        outKey = outState == null ? null : outState.getKey();

        container = (FrameLayout) activity.findViewById(R.id.container);

        if (inKey.equals(outKey)) {
            callback.onTraversalCompleted();
            return;
        }

        Context flowContext = traversal.createContext(inKey, activity);
        Context mortarContext = ScreenScoper.getScreenScope((AbsScreen) inKey).createContext(flowContext);
        contexts = Collections.singletonMap(inKey, mortarContext);
        changeKey(outState, inState, traversal.direction, contexts, callback);
    }

    @Override
    public void changeKey(@Nullable State outgoingState, State incomingState, Direction direction,
                          Map<Object, Context> incomingContexts, TraversalCallback callback) {

        Context context = incomingContexts.get(inKey);

        if (outgoingState != null) {
            outgoingState.save(container.getChildAt(0));
        }

        Screen screen;
        screen = inKey.getClass().getAnnotation(Screen.class);
        if (screen == null) {
            throw new IllegalStateException("@Screen annotation is missing on screen " +
                    ((AbsScreen) inKey).getScopeName());
        }

        int layout = screen.value();

        LayoutInflater inflater = LayoutInflater.from(context);

        View newView = inflater.inflate(layout, container, false);
        View oldView = container.getChildAt(0);

        incomingState.restore(newView);

        if (outKey == null) { // when recreate view (orientation changed) or first view
            ((AbsScreen) inKey).activateOnlyFade();
        }

        if (outKey != null && outKey.equals(inKey)) {
            callback.onTraversalCompleted();
        } else {
            container.addView(newView);
            ViewExtentionsKt.waiteForMeasure(newView, (v, w, h) -> {
                runAnimation(container, oldView, newView, direction, () -> {
                    if (outKey != null && !(inKey instanceof TreeKey)) {
                        ((AbsScreen) outKey).unregisterScope();
                    }
                    callback.onTraversalCompleted();
                    ((AbsScreen) inKey).deactivateOnlyFade();
                });
                return Unit.INSTANCE;
            });
        }
    }

    private void runAnimation(FrameLayout container, View from, View to, Direction direction, TraversalCallback callback) {
        Animator animator = createAnimation(from, to, direction);
        animator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                if (from != null) {
                    container.removeView(from);
                }
                callback.onTraversalCompleted();
            }
        });

        animator.setInterpolator(new FastOutLinearInInterpolator());
        animator.setDuration(192);
        animator.start();
    }

    private Animator createAnimation(View from, View to, Direction direction) {
        boolean backward = direction == Direction.BACKWARD;

        AnimatorSet set = new AnimatorSet();
        int fromTranslation;
        if (from != null) {
            fromTranslation = backward ? from.getWidth() : -from.getWidth();

            Animator alphaOutAnimation = ObjectAnimator.ofFloat(from, "alpha", 1, 0);
            set.play(alphaOutAnimation);

            if (!((AbsScreen) inKey).isOnlyFade()) {
                Animator outAnimation = ObjectAnimator.ofFloat(from, "translationX", fromTranslation);
                set.play(outAnimation);
            }
        }

        int toTranslation = backward ? -to.getWidth() : to.getWidth();
        Animator alphaToAnimation = ObjectAnimator.ofFloat(to, "alpha", 0, 1);
        set.play(alphaToAnimation);

        if (!((AbsScreen) inKey).isOnlyFade()) {
            Animator toAnimation = ObjectAnimator.ofFloat(to, "translationX", toTranslation, 0);
            set.play(toAnimation);
        }
        return set;
    }
}
