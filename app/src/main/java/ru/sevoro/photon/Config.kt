package ru.sevoro.photon

/**
 * @author Sergey Vorobyev
 */

// http client settings
const val BASE_URL = "http://207.154.248.163:5000/"
const val MAX_CONNECT_TIMEOUT = 1000 * 5
const val MAX_WRITE_TIMEOUT = 1000 * 20
const val MAX_READ_TIMEOUT = 1000 * 20

// Jobs
const val MAX_CONSUMER_COUNT = 3
const val MIN_CONSUMER_COUNT = 1
const val LOAD_FACTOR = 3
const val CONSUMER_KEEP_ALIVE = 120
const val INITIAL_BACK_OFF_IN_MS = 1000L
const val UPDATE_DATA_INTERVAL = 30L
const val RETRY_REQUEST_COUNT = 3
const val RETRY_REQUEST_BASE_DELAY = 1000
const val MIN_SEC_DELAY_SPLASH_SCREEN = 3L
const val MIN_LOADED_COUNT_ON_SPLASH_SCREEN = 60
