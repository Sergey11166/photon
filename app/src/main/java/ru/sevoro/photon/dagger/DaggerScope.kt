package ru.sevoro.photon.dagger

import javax.inject.Scope
import kotlin.reflect.KClass

/**
 * @author Sergey Vorobyev
 */
@Scope
@Retention(AnnotationRetention.SOURCE)
annotation class DaggerScope(val value: KClass<*>)
