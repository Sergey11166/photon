package ru.sevoro.photon.dagger

import android.content.Context
import dagger.Module
import dagger.Provides
import ru.sevoro.photon.data.DataManager
import ru.sevoro.photon.data.PreferencesManager
import ru.sevoro.photon.data.RealmManager
import ru.sevoro.photon.data.RestService
import ru.sevoro.photon.data.transformers.PhotoCardTransformer
import ru.sevoro.photon.data.transformers.RestCallTransformer
import javax.inject.Singleton

/**
 * @author Sergey Vorobyev
 */
@Module
class AppModule(private val context: Context) {

    @Provides
    @Singleton
    fun provideApplicationContext(): Context {
        return context
    }

    @Provides
    @Singleton
    fun provideDataManager(
            restService: RestService,
            restCallTransformer: RestCallTransformer<*>,
            photoCardTransformer: PhotoCardTransformer,
            realmManager: RealmManager,
            preferencesManager: PreferencesManager
    ): DataManager {
        return DataManager(
                restService,
                restCallTransformer,
                photoCardTransformer,
                realmManager,
                preferencesManager)
    }
}