package ru.sevoro.photon.dagger

import android.content.Context
import com.birbit.android.jobqueue.JobManager
import com.birbit.android.jobqueue.config.Configuration
import com.facebook.stetho.okhttp3.StethoInterceptor
import com.squareup.moshi.Moshi
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import okhttp3.logging.HttpLoggingInterceptor.Level.BODY
import retrofit2.Converter
import retrofit2.Retrofit
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory
import ru.sevoro.photon.*
import ru.sevoro.photon.data.PreferencesManager
import ru.sevoro.photon.data.RestService
import ru.sevoro.photon.data.dto.adapter.FilterJsonAdapter
import ru.sevoro.photon.data.transformers.PhotoCardTransformer
import ru.sevoro.photon.data.transformers.RestCallTransformer
import ru.sevoro.photon.data.transformers.UserTransformer
import java.util.concurrent.TimeUnit.MILLISECONDS
import javax.inject.Singleton

/**
 * @author Sergey Vorobyev
 */
@Module
open class NetworkModule {

    @Provides
    @Singleton
    fun provideJobQueueManager(context: Context): JobManager {
        val config = Configuration.Builder(context)
                .minConsumerCount(MIN_CONSUMER_COUNT)
                .maxConsumerCount(MAX_CONSUMER_COUNT)
                .loadFactor(LOAD_FACTOR)
                .consumerKeepAlive(CONSUMER_KEEP_ALIVE)
                .build()
        return JobManager(config)
    }

    @Provides
    @Singleton
    fun provideRestCallTransformer(context: Context): RestCallTransformer<*> {
        return RestCallTransformer<Any>(context)
    }

    @Provides
    @Singleton
    fun providePhotoCardTransformer(context: Context,
                                    preferencesManager: PreferencesManager): PhotoCardTransformer {
        return PhotoCardTransformer(context, preferencesManager)
    }

    @Provides
    @Singleton
    fun provideUserTransformer(context: Context): UserTransformer {
        return UserTransformer(context)
    }

    @Provides
    @Singleton
    fun provideRestService(): RestService {
        val httpClient = createHttpClient()
        val retrofit = createRetrofit(httpClient)
        return retrofit.create(RestService::class.java)
    }

    private fun createRetrofit(httpClient: OkHttpClient): Retrofit {
        return Retrofit.Builder()
                .baseUrl(getBaseUrl())
                .addConverterFactory(createConverterFactory())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .client(httpClient)
                .build()
    }

    open fun getBaseUrl() = BASE_URL

    private fun createConverterFactory(): Converter.Factory {
        return MoshiConverterFactory.create(Moshi.Builder()
                .add(FilterJsonAdapter())
                .build())
    }

    private fun createHttpClient(): OkHttpClient {
        return OkHttpClient.Builder()
                .addInterceptor(HttpLoggingInterceptor().setLevel(BODY))
                .addInterceptor(StethoInterceptor())
                .connectTimeout(MAX_CONNECT_TIMEOUT.toLong(), MILLISECONDS)
                .readTimeout(MAX_READ_TIMEOUT.toLong(), MILLISECONDS)
                .writeTimeout(MAX_WRITE_TIMEOUT.toLong(), MILLISECONDS)
                .build()
    }
}
