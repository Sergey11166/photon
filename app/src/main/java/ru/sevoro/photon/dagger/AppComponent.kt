package ru.sevoro.photon.dagger

import dagger.Component
import ru.sevoro.photon.data.PreferencesManager
import ru.sevoro.photon.data.RealmManager
import ru.sevoro.photon.data.RestService
import ru.sevoro.photon.screen.base.AbsModel
import javax.inject.Singleton

/**
 * @author Sergey Vorobyev
 */
@Singleton
@Component(modules = arrayOf(AppModule::class, NetworkModule::class, LocalModule::class))
interface AppComponent {
    fun inject(model: AbsModel)
    fun restService(): RestService
    fun realmManager(): RealmManager
    fun preferencesManager(): PreferencesManager
}
