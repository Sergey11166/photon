package ru.sevoro.photon.dagger

import android.content.Context
import com.facebook.stetho.Stetho
import com.uphyca.stetho_realm.RealmInspectorModulesProvider
import dagger.Module
import dagger.Provides
import ru.sevoro.photon.data.FileManager
import ru.sevoro.photon.data.PreferencesManager
import ru.sevoro.photon.data.RealmManager
import javax.inject.Singleton

/**
 * @author Sergey Vorobyev
 */
@Module
class LocalModule {

    @Provides
    @Singleton
    fun provideFileManager(context: Context): FileManager {
        return FileManager(context)
    }

    @Provides
    @Singleton
    fun providePreferencesManager(context: Context): PreferencesManager {
        return PreferencesManager(context)
    }

    @Provides
    @Singleton
    fun provideRealmManager(context: Context): RealmManager {
        Stetho.initialize(Stetho.newInitializerBuilder(context)
                .enableDumpapp(Stetho.defaultDumperPluginsProvider(context))
                .enableWebKitInspector(RealmInspectorModulesProvider.builder(context).build())
                .build())
        return RealmManager()
    }
}
