package ru.sevoro.photon.dagger

import dagger.Module
import dagger.Provides
import ru.sevoro.photon.data.dto.ActivityResultDto
import ru.sevoro.photon.data.dto.PermissionsResultDto
import ru.sevoro.photon.screen.root.RootActivity
import ru.sevoro.photon.screen.root.RootPresenter
import ru.sevoro.photon.screen.root.RootPresenterImpl
import rx.subjects.PublishSubject

/**
 * @author Sergey Vorobyev.
 */
@Module
class RootModule {

    @Provides
    @DaggerScope(RootActivity::class)
    fun provideActivityResultSubject(): PublishSubject<ActivityResultDto> {
        return PublishSubject.create<ActivityResultDto>()
    }

    @Provides
    @DaggerScope(RootActivity::class)
    fun providePermissionsResultSubject(): PublishSubject<PermissionsResultDto> {
        return PublishSubject.create<PermissionsResultDto>()
    }

    @Provides
    @DaggerScope(RootActivity::class)
    fun providePresenter(
            activityResultSubject: PublishSubject<ActivityResultDto>,
            permissionsResultSubject: PublishSubject<PermissionsResultDto>
    ): RootPresenter {
        return RootPresenterImpl(activityResultSubject, permissionsResultSubject)
    }
}
