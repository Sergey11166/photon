package ru.sevoro.photon.screen.choice_photo

import dagger.Provides
import ru.sevoro.photon.R
import ru.sevoro.photon.dagger.DaggerScope
import ru.sevoro.photon.data.dto.ActivityResultDto
import ru.sevoro.photon.data.dto.PermissionsResultDto
import ru.sevoro.photon.flow_mortar.Screen
import ru.sevoro.photon.screen.base.AbsScreen
import ru.sevoro.photon.screen.root.RootActivity
import rx.subjects.PublishSubject

/**
 * @author Sergey Vorobyev
 */

@Screen(R.layout.screen_choice_photo)
class ChoicePhotoScreen : AbsScreen<RootActivity.RootComponent>() {

    override fun createScreenComponent(parentComponent: RootActivity.RootComponent?): Any {
        return DaggerChoicePhotoScreen_Component.builder()
                .rootComponent(parentComponent)
                .module(Module())
                .build()
    }

    @DaggerScope(ChoicePhotoScreen::class)
    @dagger.Component(dependencies = arrayOf(RootActivity.RootComponent::class),
            modules = arrayOf(Module::class))
    interface Component {
        fun inject(view: ChoicePhotoViewImpl)

        fun inject(presenter: ChoicePhotoPresenterImpl)
    }

    @dagger.Module
    class Module {

        @Provides
        @DaggerScope(ChoicePhotoScreen::class)
        fun providePresenter(activityResultSubject: PublishSubject<ActivityResultDto>,
                             permissionsResultSubject: PublishSubject<PermissionsResultDto>): ChoicePhotoPresenter {
            return ChoicePhotoPresenterImpl(activityResultSubject, permissionsResultSubject)
        }
    }
}