package ru.sevoro.photon.screen.user

import android.os.Bundle
import flow.Flow
import io.realm.RealmList
import mortar.MortarScope
import ru.sevoro.photon.data.dto.realm.AlbumRealm
import ru.sevoro.photon.data.dto.realm.UserRealm
import ru.sevoro.photon.screen.album.AlbumScreen
import ru.sevoro.photon.screen.base.AbsPresenter
import ru.sevoro.photon.util.L
import rx.Subscriber

/**
 * @author Sergey Vorobyev
 */
abstract class UserPresenterImpl<V : UserViewImpl<*>>(
        private val userId: String,
        private val isProfile: Boolean,
        private val model: UserModel,
        private val viewModel: UserVM
) : AbsPresenter<V>(), UserPresenter<V> {

    private var albums: RealmList<AlbumRealm> = RealmList()

    override fun onAlbumClick(album: AlbumRealm) {
        if (view != null) {
            Flow.get(view).set(AlbumScreen(album, isProfile))
        }
    }

    override fun onEnterScope(scope: MortarScope) {
        super.onEnterScope(scope)
        initViewModel(UserRealm())
        model.getUser(userId).subscribe(object : Subscriber<UserRealm?>() {
            override fun onCompleted() {}
            override fun onNext(user: UserRealm?) {
                L.d("onEnterScope getUser onNext: $user")
                if (user != null) {
                    initViewModel(user)
                    albums.clear()
                    user.albums.filter { !it.isDeleted }.forEach { albums.add(it) }
                    view?.showAlbums(albums)
                }
            }

            override fun onError(e: Throwable) {
                e.printStackTrace()
            }
        })
    }

    override fun onLoad(savedInstanceState: Bundle?) {
        super.onLoad(savedInstanceState)
        view?.bindViewModel(viewModel)
        view?.showAlbums(albums)
    }

    protected fun addAlbum(album: AlbumRealm) {
        albums.add(album)
        view?.showAlbums(albums)
    }

    private fun initViewModel(user: UserRealm) {
        viewModel.login = user.login
        viewModel.username = user.username
        viewModel.avatar = user.avatar
        viewModel.albumCount = user.albums.filter { !it.isDeleted }.size.toString()

        var photoCardCount = 0
        user.albums.filter { !it.isDeleted }.forEach { album -> photoCardCount += album.photoCards.size }
        viewModel.photoCardCount = photoCardCount.toString()
    }
}