package ru.sevoro.photon.screen.not_authorized

import ru.sevoro.photon.data.dto.request.SignInRequest
import ru.sevoro.photon.data.dto.request.SignUpRequest
import ru.sevoro.photon.screen.base.AbsModel
import rx.Observable
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers

open class AuthModelImpl : AbsModel(), AuthModel {

    override fun signUp(login: String, email: String, username: String, password: String): Observable<String> {
        return restService.signUp(SignUpRequest(username, login, email, password))
                .compose(userTransformer)
                .doOnNext {
                    prefsManager.saveCurrentUserId(it.id)
                    prefsManager.saveToken(it.token)
                    updateUserInRealm(it)
                }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map { it.id }
    }

    override fun signIn(email: String, password: String): Observable<String> {
        return restService.signIn(SignInRequest(email, password))
                .compose(userTransformer)
                .doOnNext {
                    prefsManager.saveCurrentUserId(it.id)
                    prefsManager.saveToken(it.token)
                    updateUserInRealm(it)
                }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map { it.id }
    }
}