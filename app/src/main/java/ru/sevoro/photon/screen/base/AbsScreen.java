package ru.sevoro.photon.screen.base;

import flow.ClassKey;
import ru.sevoro.photon.flow_mortar.ScreenScoper;
import ru.sevoro.photon.util.L;

/**
 * @author Sergey Vorobyev.
 */
public abstract class AbsScreen<T> extends ClassKey {

    private boolean isOnlyFade;

    public String getScopeName() {
        return getClass().getName();
    }

    public abstract Object createScreenComponent(T parentComponent);

    public void unregisterScope() {
        L.d("unregisterScope: " + getScopeName());
        ScreenScoper.destroyScreenScope(getScopeName());
    }

    public AbsScreen<T> activateOnlyFade() {
        isOnlyFade = true;
        return this;
    }

    public void deactivateOnlyFade() {
        isOnlyFade = false;
    }

    public boolean isOnlyFade() {
        return isOnlyFade;
    }
}
