package ru.sevoro.photon.screen.home

import ru.sevoro.photon.data.dto.realm.PhotoCardRealm
import ru.sevoro.photon.screen.not_authorized.AuthModel
import rx.Observable

/**
 * @author Sergey Vorobyev
 */
interface HomeModel : AuthModel {

    fun getPhotoCards(specification: SearchPhotoCardsSpecification): Observable<PhotoCardRealm>

    fun isAuthorized(): Boolean

    fun logout()
}