package ru.sevoro.photon.screen.user

/**
 * @author Sergey Vorobyev
 */
interface AuthorPresenter : UserPresenter<AuthorViewImpl>