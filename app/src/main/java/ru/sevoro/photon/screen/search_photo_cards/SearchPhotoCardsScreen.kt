package ru.sevoro.photon.screen.search_photo_cards

import dagger.Provides
import flow.TreeKey
import ru.sevoro.photon.R
import ru.sevoro.photon.dagger.DaggerScope
import ru.sevoro.photon.flow_mortar.Screen
import ru.sevoro.photon.screen.base.AbsScreen
import ru.sevoro.photon.screen.base.FiltersVM
import ru.sevoro.photon.screen.home.HomeScreen
import ru.sevoro.photon.screen.home.SearchPhotoCardsSpecification

/**
 * @author Sergey Vorobyev
 */
@Screen(R.layout.screen_search_photo_cards)
class SearchPhotoCardsScreen(private var specification: SearchPhotoCardsSpecification) :
        AbsScreen<HomeScreen.Component>(), TreeKey {

    override fun createScreenComponent(parentComponent: HomeScreen.Component?): Any {
        return DaggerSearchPhotoCardsScreen_Component.builder()
                .component(parentComponent)
                .module(Module(specification))
                .build()
    }

    override fun getParentKey() = HomeScreen(specification)

    @DaggerScope(SearchPhotoCardsScreen::class)
    @dagger.Component(dependencies = arrayOf(HomeScreen.Component::class),
            modules = arrayOf(Module::class))
    interface Component {
        fun inject(view: SearchPhotoCardsViewImpl)

        fun inject(presenter: SearchPhotoCardsPresenterImpl)
    }

    @dagger.Module
    class Module(private var specification: SearchPhotoCardsSpecification) {

        @Provides
        @DaggerScope(SearchPhotoCardsScreen::class)
        fun provideModel(): SearchPhotoCardsModel {
            return SearchPhotoCardsModelImpl()
        }

        @Provides
        @DaggerScope(SearchPhotoCardsScreen::class)
        fun provideFiltersViewModel(): FiltersVM {
            return FiltersVM()
        }

        @Provides
        @DaggerScope(SearchPhotoCardsScreen::class)
        fun provideSearchViewModel(): SearchPhotoCardsVM {
            return SearchPhotoCardsVM()
        }

        @Provides
        @DaggerScope(SearchPhotoCardsScreen::class)
        fun providePresenter(model: SearchPhotoCardsModel, searchVM: SearchPhotoCardsVM,
                             filtersVM: FiltersVM): SearchPhotoCardsPresenter {
            return SearchPhotoCardsPresenterImpl(specification, model, searchVM, filtersVM)
        }
    }
}