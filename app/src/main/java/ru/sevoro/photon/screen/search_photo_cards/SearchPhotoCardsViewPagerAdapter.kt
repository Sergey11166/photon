package ru.sevoro.photon.screen.search_photo_cards

import android.content.Context
import android.databinding.DataBindingUtil
import android.support.v4.view.PagerAdapter
import android.support.v4.view.ViewPager
import android.text.Editable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import ru.sevoro.photon.R
import ru.sevoro.photon.databinding.PageSearchPhotoCardsBinding
import ru.sevoro.photon.databinding.PageSearchPhotoCardsFiltersBinding
import ru.sevoro.photon.screen.base.FiltersVM
import ru.sevoro.photon.util.SimpleTextWatcher

/**
 * @author Sergey Vorobyev
 */
class SearchPhotoCardsViewPagerAdapter(
        private val context: Context,
        private val searchChangeListener: () -> Unit,
        private val clickListener: (view: View) -> Unit
) : PagerAdapter() {

    private lateinit var searchBinding: PageSearchPhotoCardsBinding
    private lateinit var filterBinding: PageSearchPhotoCardsFiltersBinding

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val view = when (position) {
            0 -> searchBinding.root
            1 -> filterBinding.root
            else -> null
        }
        searchBinding.searchInput.setSelection(searchBinding.searchInput.text.length)
        container.addView(view)
        return view!!
    }

    override fun getPageTitle(position: Int): CharSequence {
        return when (position) {
            0 -> context.getString(R.string.search_tab)
            1 -> context.getString(R.string.search_tab_filters)
            else -> ""
        }
    }

    fun prepareSearchPage(viewPager: ViewPager, searchVM: SearchPhotoCardsVM) {
        val searchView = LayoutInflater.from(context).inflate(R.layout.page_search_photo_cards, viewPager, false)
        searchBinding = DataBindingUtil.bind<PageSearchPhotoCardsBinding>(searchView)
        searchBinding.searchModel = searchVM
        searchBinding.searchCheckedButton.setOnClickListener { v -> clickListener(v) }
        searchBinding.searchButton.setOnClickListener { v -> clickListener(v) }
        searchBinding.searchInput.addTextChangedListener(object : SimpleTextWatcher() {
            override fun afterTextChanged(text: Editable) { searchChangeListener() }
        })
    }

    fun prepareFilterPage(viewPager: ViewPager, filterVM: FiltersVM) {
        val filterView = LayoutInflater.from(context).inflate(R.layout.page_search_photo_cards_filters, viewPager, false)
        filterBinding = DataBindingUtil.bind<PageSearchPhotoCardsFiltersBinding>(filterView)
        filterBinding.filtersModel = filterVM
        filterBinding.filterSearchButton.setOnClickListener { v -> clickListener(v) }
    }

    override fun isViewFromObject(view: View, `object`: Any) = view == `object`

    override fun getCount() = 2

    fun getSearchPageBinding() = searchBinding
}