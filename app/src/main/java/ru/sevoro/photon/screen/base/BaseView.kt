package ru.sevoro.photon.screen.base

/**
 * @author Sergey Vorobyev
 */
interface BaseView {

    fun bindViewModel(viewModel: BaseViewModel)

    fun onViewBackPressed(): Boolean
}
