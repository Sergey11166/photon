package ru.sevoro.photon.screen.home

import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import ru.sevoro.photon.R

/**
 * @author Sergey Vorobyev
 */
class PhotoCardViewHolder(itemView: View, clickListener: (Int) -> Unit) : RecyclerView.ViewHolder(itemView) {
    val image: ImageView = itemView.findViewById(R.id.photo)
    val favorites: TextView = itemView.findViewById(R.id.favorites)
    val views: TextView = itemView.findViewById(R.id.views)

    init {
        itemView.setOnClickListener { clickListener(adapterPosition) }
    }
}