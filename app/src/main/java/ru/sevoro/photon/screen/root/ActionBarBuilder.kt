package ru.sevoro.photon.screen.root

import android.support.annotation.DrawableRes
import android.support.annotation.StringRes
import android.support.v4.view.ViewPager

/**
 * @author Sergey Vorobyev
 */
const val DEFAULT_MODE = 0
const val TAB_MODE = 1

class ActionBarBuilder(private val rootView: RootView?) {

    private var toolbarMode = DEFAULT_MODE
    private var isGoBack = false
    private var isVisible = true
    private var viewPager: ViewPager? = null
    private var title: CharSequence = ""
    private var titleResId = 0
    private var overFlowIcon = 0
    private var isShowIconsInPopupMenu = false
    private val menuActions = mutableListOf<MenuItemHolder>()

    fun setBackArrow(enable: Boolean): ActionBarBuilder {
        this.isGoBack = enable
        return this
    }

    fun setTitle(title: CharSequence): ActionBarBuilder {
        this.title = title
        return this
    }

    fun setTitle(@StringRes resId: Int): ActionBarBuilder {
        this.titleResId = resId
        return this
    }

    fun setVisibility(visible: Boolean): ActionBarBuilder {
        this.isVisible = visible
        return this
    }

    fun addAction(action: MenuItemHolder): ActionBarBuilder {
        this.menuActions.add(action)
        return this
    }

    fun setTabs(viewPager: ViewPager): ActionBarBuilder {
        this.toolbarMode = TAB_MODE
        this.viewPager = viewPager
        return this
    }

    fun setOverflowIcon(@DrawableRes icon: Int): ActionBarBuilder {
        this.overFlowIcon = icon
        return this
    }

    fun setShowIconsInPopupMenu(isShow: Boolean): ActionBarBuilder {
        this.isShowIconsInPopupMenu = isShow
        return this
    }

    fun build() {
        if (rootView != null) {
            if (rootView is RootActivity) {
                val activity = rootView
                activity.setActionBarVisibility(isVisible)
                if (titleResId != 0) {
                    activity.setActionBarTitle(titleResId)
                } else {
                    activity.setActionBarTitle(title)
                }
                activity.setBackArrow(isGoBack)
                activity.setOverflowIcon(overFlowIcon)
                activity.setShowIconsInPopupMenu(isShowIconsInPopupMenu)
                activity.setMenuItems(menuActions)
                if (toolbarMode == TAB_MODE) {
                    activity.setTabLayout(viewPager!!)
                } else {
                    activity.removeTabLayout()
                }
            } else {
                throw IllegalStateException("RootView must be instance of RootActivity")
            }
        }
    }
}