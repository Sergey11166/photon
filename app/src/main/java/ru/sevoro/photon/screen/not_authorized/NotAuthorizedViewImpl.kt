package ru.sevoro.photon.screen.not_authorized

import android.content.Context
import android.util.AttributeSet
import android.view.View
import ru.sevoro.photon.R
import ru.sevoro.photon.dialog.SignInVM
import ru.sevoro.photon.dialog.SignUpVM
import ru.sevoro.photon.dialog.getInstanceSignInDialog
import ru.sevoro.photon.dialog.getInstanceSignUpDialog
import ru.sevoro.photon.screen.base.AbsView
import ru.sevoro.photon.util.getComponent

class NotAuthorizedViewImpl(context: Context, attrs: AttributeSet?) :
        AbsView<NotAuthorizedPresenter>(context, attrs), NotAuthorizedView {

    private val signUpDialog = getInstanceSignUpDialog { view ->
        when (view.id) {
            R.id.ok_button -> presenter.onOkButtonClickSignUpDialog()
        }
    }

    private val signInDialog = getInstanceSignInDialog { view ->
        when (view.id) {
            R.id.ok_button -> presenter.onOkButtonClickSignInDialog()
        }
    }

    override fun onFinishInflate() {
        super.onFinishInflate()
        if (!isInEditMode) {
            findViewById<View>(R.id.sign_in_button).setOnClickListener { presenter.onSignInButtonClick() }
            findViewById<View>(R.id.sign_up_button).setOnClickListener { presenter.onSignUpButtonClick() }
        }
    }

    override fun showSignInDialog(viewModel: SignInVM) {
        signInDialog.setViewModel(viewModel)
        showFragmentDialog(signInDialog)
    }

    override fun closeSignInDialog() {
        signInDialog.dismissAllowingStateLoss()
    }

    override fun showSignUpDialog(viewModel: SignUpVM) {
        signUpDialog.setViewModel(viewModel)
        showFragmentDialog(signUpDialog)
    }

    override fun closeSignUpDialog() {
        signUpDialog.dismissAllowingStateLoss()
    }

    override fun initDagger(context: Context) {
        context.getComponent<NotAuthorizedScreen.Component>().inject(this)
    }
}