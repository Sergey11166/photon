package ru.sevoro.photon.screen.photo_card

import com.birbit.android.jobqueue.Job
import com.birbit.android.jobqueue.Params
import com.birbit.android.jobqueue.RetryConstraint
import ru.sevoro.photon.App
import ru.sevoro.photon.INITIAL_BACK_OFF_IN_MS
import ru.sevoro.photon.data.JobPriority
import ru.sevoro.photon.util.L

/**
 * @author Sergey Vorobyev
 */
class AddPhotoCardViewJobJob(private val photoCardId: String) :
        Job(Params(JobPriority.HIGH).requireNetwork().persist()) {

    override fun onRun() {
        L.d("onRun")

        val restService = App.appComponent.restService()
        restService.addPhotoCardView(photoCardId).subscribe { L.d("photo card view added on server") }
    }

    override fun shouldReRunOnThrowable(throwable: Throwable, runCount: Int, maxRunCount: Int): RetryConstraint {
        L.d("shouldReRunOnThrowable")
        return RetryConstraint.createExponentialBackoff(runCount, INITIAL_BACK_OFF_IN_MS)
    }

    override fun onAdded() {
        L.d("onAdded")
    }

    override fun onCancel(cancelReason: Int, throwable: Throwable?) {
        L.d("onCancel")
    }
}