package ru.sevoro.photon.screen.not_authorized

import ru.sevoro.photon.screen.base.BasePresenter

/**
 * @author Sergey Vorobyev
 */
interface NotAuthorizedPresenter : BasePresenter<NotAuthorizedViewImpl> {

    fun onSignInButtonClick()

    fun onSignUpButtonClick()

    fun onOkButtonClickSignInDialog()

    fun onOkButtonClickSignUpDialog()
}