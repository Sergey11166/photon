package ru.sevoro.photon.screen.photo_card

import ru.sevoro.photon.screen.base.BasePresenter

/**
 * @author Sergey Vorobyev
 */
interface PhotoCardPresenter : BasePresenter<PhotoCardViewImpl> {

    fun onAvatarClick()
}