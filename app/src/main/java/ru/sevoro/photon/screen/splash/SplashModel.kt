package ru.sevoro.photon.screen.splash

import rx.Observable

/**
 * @author Sergey Vorobyev
 */
interface SplashModel {
    fun updateLocalData(): Observable<*>

    fun startUpdatePhotoCardsWithTimer()
}
