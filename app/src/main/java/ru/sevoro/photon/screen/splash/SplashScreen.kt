package ru.sevoro.photon.screen.splash

import dagger.Provides
import ru.sevoro.photon.R
import ru.sevoro.photon.dagger.DaggerScope
import ru.sevoro.photon.flow_mortar.Screen
import ru.sevoro.photon.screen.base.AbsScreen
import ru.sevoro.photon.screen.root.RootActivity

/**
 * @author Sergey Vorobyev
 */
@Screen(R.layout.screen_splash)
class SplashScreen : AbsScreen<RootActivity.RootComponent>() {

    override fun createScreenComponent(parentComponent: RootActivity.RootComponent): Any {
        return DaggerSplashScreen_Component.builder()
                .rootComponent(parentComponent)
                .module(Module())
                .build()
    }

    @DaggerScope(SplashScreen::class)
    @dagger.Component(dependencies = arrayOf(RootActivity.RootComponent::class),
            modules = arrayOf(Module::class))
    interface Component {
        fun inject(view: SplashViewImpl)

        fun inject(presenter: SplashPresenterImpl)
    }

    @dagger.Module
    class Module {
        @Provides
        @DaggerScope(SplashScreen::class)
        fun provideModel(): SplashModel {
            return SplashModelImpl()
        }

        @Provides
        @DaggerScope(SplashScreen::class)
        fun providePresenter(model: SplashModel): SplashPresenter {
            return SplashPresenterImpl(model)
        }
    }
}
