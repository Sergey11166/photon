package ru.sevoro.photon.screen.splash

import ru.sevoro.photon.screen.base.BasePresenter

/**
 * @author Sergey Vorobyev
 */
interface SplashPresenter : BasePresenter<SplashViewImpl>