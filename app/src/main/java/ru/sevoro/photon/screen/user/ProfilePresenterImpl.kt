package ru.sevoro.photon.screen.user

import android.Manifest.permission.WRITE_EXTERNAL_STORAGE
import android.app.Activity.RESULT_OK
import android.content.pm.PackageManager.PERMISSION_GRANTED
import android.net.Uri
import android.view.MenuItem
import flow.Direction.REPLACE
import flow.Flow
import mortar.MortarScope
import ru.sevoro.photon.R
import ru.sevoro.photon.data.dto.ActivityResultDto
import ru.sevoro.photon.data.dto.PermissionsResultDto
import ru.sevoro.photon.data.dto.realm.AlbumRealm
import ru.sevoro.photon.dialog.EditAlbumVM
import ru.sevoro.photon.dialog.EditProfileVM
import ru.sevoro.photon.screen.not_authorized.NotAuthorizedScreen
import ru.sevoro.photon.screen.root.ActionBarBuilder
import ru.sevoro.photon.screen.root.MenuItemHolder
import ru.sevoro.photon.util.SERVICE_NAME
import ru.sevoro.photon.util.filePathFromUri
import ru.sevoro.photon.util.showToast
import rx.Subscriber
import rx.subjects.PublishSubject
import java.io.File
import java.io.IOException

/**
 * @author Sergey Vorobyev
 */

// onActivityResult request codes
const val REQUEST_CODE_CAMERA = 0
const val REQUEST_CODE_GALLERY = 1
const val REQUEST_CODE_SETTINGS = 2

// Permissions requests
const val CAMERA_PERMISSION_REQUEST_CODE = 0
const val GALLERY_PERMISSION_REQUEST_CODE = 1

class ProfilePresenterImpl(
        private val userId: String,
        private val activityResultSubject: PublishSubject<ActivityResultDto>,
        private val permissionsResultSubject: PublishSubject<PermissionsResultDto>,
        private val model: UserModel,
        private val viewModel: UserVM
) : UserPresenterImpl<ProfileViewImpl>(userId, true, model, viewModel), ProfilePresenter {

    private var editProfileVM: EditProfileVM? = null
    private val newAlbumVM = EditAlbumVM()

    private var photoFile: File? = null // file path for taking a photo
    private var selectedAvatar: Uri? = null

    override fun onEnterScope(scope: MortarScope) {
        super.onEnterScope(scope)
        subscribeOnActivityResult()
        subscribeOnRequestPermissionsResult()
    }

    override fun onChoiceFromGallery() {
        checkAndRequestGalleryPermissions()
    }

    override fun onChoiceTakePhoto() {
        checkAndRequestCameraPermissions()
    }

    override fun onOkButtonClickEditProfileDialog() {
        if (editProfileVM != null) {
            if (!editProfileVM!!.isLoginValid || !editProfileVM!!.isUsernameValid) {
                view?.context?.showToast(R.string.message_tape_valid_data)
            } else {
                view?.closeEditProfileDialog()
                model.updateProfile(editProfileVM!!.username, editProfileVM!!.login)
                viewModel.username = editProfileVM!!.username
                viewModel.login = editProfileVM!!.login
            }
        }
    }

    override fun onOkButtonClickNewAlbumDialog() {
        if (!newAlbumVM.isNameValid || !newAlbumVM.isDescriptionValid) {
            view?.context?.showToast(R.string.message_tape_valid_data)
        } else {
            view?.closeNewAlbumDialog()
            rootView?.showProgress()
            model.createNewAlbum(newAlbumVM.name, newAlbumVM.description)
                    .subscribe(object : Subscriber<AlbumRealm>() {
                        override fun onCompleted() {}
                        override fun onNext(album: AlbumRealm) {
                            addAlbum(album)
                            rootView?.hideProgress()
                        }

                        override fun onError(e: Throwable) {
                            e.printStackTrace()
                            rootView?.hideProgress()
                            rootView?.showSnackBar(R.string.profile_error_add_new_album)
                        }
                    })

        }
    }

    private fun subscribeOnActivityResult() {
        subscriptions.add(activityResultSubject
                .filter { (requestCode, resultCode) ->
                    requestCode == REQUEST_CODE_CAMERA &&
                            resultCode == RESULT_OK &&
                            photoFile != null
                }
                .subscribe {
                    selectedAvatar = Uri.fromFile(photoFile)
                    onAvatarChanged()
                })

        subscriptions.add(activityResultSubject
                .filter { (requestCode, resultCode, intent) ->
                    requestCode == REQUEST_CODE_GALLERY &&
                            resultCode == RESULT_OK &&
                            intent != null &&
                            intent.data != null
                }
                .subscribe {
                    selectedAvatar = Uri.fromFile(File(view?.context?.filePathFromUri(it?.intent?.data!!)))
                    onAvatarChanged()
                })
    }

    private fun subscribeOnRequestPermissionsResult() {
        subscriptions.add(permissionsResultSubject
                .filter { (requestCode) -> requestCode == CAMERA_PERMISSION_REQUEST_CODE }
                .subscribe { handleCameraRequestPermissionsResult(it) })

        subscriptions.add(permissionsResultSubject
                .filter { (requestCode) -> requestCode == GALLERY_PERMISSION_REQUEST_CODE }
                .subscribe { handleGalleryRequestPermissionsResult(it) })
    }

    private fun handleCameraRequestPermissionsResult(result: PermissionsResultDto) {
        if (result.grantResults.isNotEmpty() && result.grantResults[0] == PERMISSION_GRANTED) {
            handleCameraPermissionsGranted()
        } else {
            if (view != null) {
                rootView?.showSnackBar(
                        R.string.message_allow_write_file,
                        R.string.snack_bar_action_settings,
                        { rootPresenter.startSettingForResult(REQUEST_CODE_SETTINGS) })
            }
        }
    }

    private fun handleGalleryRequestPermissionsResult(result: PermissionsResultDto) {
        if (result.grantResults.isNotEmpty() && result.grantResults[0] == PERMISSION_GRANTED) {
            handleGalleryPermissionsGranted()
        } else {
            if (view != null) {
                rootView?.showSnackBar(
                        R.string.message_allow_write_file,
                        R.string.snack_bar_action_settings,
                        { rootPresenter.startSettingForResult(REQUEST_CODE_SETTINGS) })
            }
        }
    }

    private fun checkAndRequestCameraPermissions() {
        val permissions = arrayOf(WRITE_EXTERNAL_STORAGE)
        if (rootPresenter.checkAndRequestPermission(permissions, CAMERA_PERMISSION_REQUEST_CODE)) {
            handleCameraPermissionsGranted()
        }
    }

    private fun checkAndRequestGalleryPermissions() {
        val permissions = arrayOf(WRITE_EXTERNAL_STORAGE)
        if (rootPresenter.checkAndRequestPermission(permissions, GALLERY_PERMISSION_REQUEST_CODE)) {
            handleGalleryPermissionsGranted()
        }
    }

    private fun handleCameraPermissionsGranted() {
        photoFile = null
        try {
            photoFile = model.createTempFileForCamera(userId)
        } catch (e: IOException) {
            e.printStackTrace()
            rootView?.showSnackBar(R.string.message_error_create_file)
        }
        if (photoFile != null) {
            rootPresenter.startCameraForResult(photoFile!!, REQUEST_CODE_CAMERA)
        }
    }

    private fun handleGalleryPermissionsGranted() {
        rootPresenter.startGalleryForResult(REQUEST_CODE_GALLERY)
    }

    private fun onAvatarChanged() {
        viewModel.avatar = selectedAvatar!!.toString()
        model.updateAvatar(selectedAvatar!!.path)
    }

    override fun initActionBar() {
        val actionBarBuilder: ActionBarBuilder? = rootPresenter.newActionBarBuilder()
        @Suppress("IfThenToSafeAccess")
        if (actionBarBuilder != null) {
            actionBarBuilder
                    .setTitle(R.string.profile_title)
                    .setOverflowIcon(R.drawable.ic_custom_menu_black_24dp)
                    .setShowIconsInPopupMenu(true)
                    .addAction(MenuItemHolder(R.drawable.ic_custom_add_to_album_black_24dp,
                            MenuItem.SHOW_AS_ACTION_NEVER, R.string.profile_menu_new_album,
                            { view?.showNewAlbumDialog(newAlbumVM) }))
                    .addAction(MenuItemHolder(R.drawable.ic_custom_edit_black_24dp,
                            MenuItem.SHOW_AS_ACTION_NEVER, R.string.profile_menu_edit_profile,
                            {
                                if (editProfileVM == null) {
                                    editProfileVM = EditProfileVM()
                                    editProfileVM!!.login = viewModel.login
                                    editProfileVM!!.username = if (viewModel.username.length > 2) {
                                        viewModel.username.substring(2)
                                    } else viewModel.username
                                }
                                view?.showEditProfileDialog(editProfileVM!!)
                            }))
                    .addAction(MenuItemHolder(R.drawable.ic_custom_change_avatar_black_24dp,
                            MenuItem.SHOW_AS_ACTION_NEVER, R.string.profile_menu_change_avatar,
                            { view?.showChangeAvatarDialog() }))
                    .addAction(MenuItemHolder(R.drawable.ic_custom_circular_power_on_black_24dp,
                            MenuItem.SHOW_AS_ACTION_NEVER, R.string.profile_menu_logout, {
                        if (view != null) {
                            model.logout()
                            Flow.get(view).replaceHistory(NotAuthorizedScreen().activateOnlyFade(), REPLACE)
                        }
                    }))
                    .build()
        }
    }

    override fun initDagger(scope: MortarScope) {
        scope.getService<ProfileScreen.Component>(SERVICE_NAME).inject(this)
    }
}
