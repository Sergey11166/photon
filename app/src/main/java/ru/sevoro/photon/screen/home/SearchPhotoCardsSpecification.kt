package ru.sevoro.photon.screen.home

/**
 * @author Sergey Vorobyev
 */
data class SearchPhotoCardsSpecification(
        var title: String = "",
        var dish: String = "",
        var nuances: ArrayList<String> = arrayListOf(),
        var decor: String = "",
        var temperature: String = "",
        var light: String = "",
        var lightDirection: String = "",
        var lightSource: String = "",
        var tags: ArrayList<String> = arrayListOf()) {

    fun clear() {
        title = ""
        dish = ""
        nuances.clear()
        decor = ""
        temperature = ""
        light = ""
        lightDirection = ""
        lightSource = ""
        tags.clear()
    }
}