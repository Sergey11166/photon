package ru.sevoro.photon.screen.album

import dagger.Provides
import ru.sevoro.photon.R
import ru.sevoro.photon.dagger.DaggerScope
import ru.sevoro.photon.data.dto.realm.AlbumRealm
import ru.sevoro.photon.dialog.EditAlbumVM
import ru.sevoro.photon.flow_mortar.Screen
import ru.sevoro.photon.screen.base.AbsScreen
import ru.sevoro.photon.screen.root.RootActivity
import java.util.*

/**
 * @author Sergey Vorobyev
 */
@Screen(R.layout.screen_album)
class AlbumScreen(private val album: AlbumRealm, private val isFromProfile: Boolean) :
        AbsScreen<RootActivity.RootComponent>() {

    override fun createScreenComponent(parentComponent: RootActivity.RootComponent?): Any {
        return DaggerAlbumScreen_Component.builder()
                .rootComponent(parentComponent)
                .module(Module(album, isFromProfile))
                .build()
    }

    override fun hashCode() = Objects.hash(album.id, isFromProfile)

    override fun equals(other: Any?) = other === this


    @DaggerScope(AlbumScreen::class)
    @dagger.Component(dependencies = arrayOf(RootActivity.RootComponent::class),
            modules = arrayOf(Module::class))
    interface Component {
        fun inject(view: AlbumViewImpl)

        fun inject(presenter: AlbumPresenterImpl)
    }

    @dagger.Module
    class Module(private val album: AlbumRealm, private val isFromProfile: Boolean) {

        @Provides
        @DaggerScope(AlbumScreen::class)
        fun provideModel(): AlbumModel {
            return AlbumModelImpl()
        }

        @Provides
        @DaggerScope(AlbumScreen::class)
        fun provideViewModel(): AlbumVM {
            return AlbumVM()
        }

        @Provides
        @DaggerScope(AlbumScreen::class)
        fun provideEditAlbumViewModel(): EditAlbumVM {
            return EditAlbumVM()
        }

        @Provides
        @DaggerScope(AlbumScreen::class)
        fun providePresenter(model: AlbumModel, viewModel: AlbumVM, editAlbumVM: EditAlbumVM): AlbumPresenter {
            return AlbumPresenterImpl(album, isFromProfile, model, viewModel, editAlbumVM)
        }
    }
}