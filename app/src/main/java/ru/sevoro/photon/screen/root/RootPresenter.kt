package ru.sevoro.photon.screen.root

import android.content.Intent
import ru.sevoro.photon.screen.base.AbsScreen

import java.io.File

/**
 * @author Sergey Vorobyev.
 */
interface RootPresenter {

    val view: RootView?

    fun takeView(rootView: RootView)

    fun dropView()

    fun onBottomMenuItemClick(itemId: Int): AbsScreen<*>?

    fun startCameraForResult(file: File, requestCode: Int)

    fun startGalleryForResult(requestCode: Int)

    fun startSettingForResult(requestCode: Int)

    fun checkAndRequestPermission(permissions: Array<String>, requestCode: Int): Boolean

    fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?)

    fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray)

    fun newActionBarBuilder(): ActionBarBuilder?
}
