package ru.sevoro.photon.screen.new_photo_card

import ru.sevoro.photon.data.dto.realm.AlbumRealm
import ru.sevoro.photon.dialog.EditAlbumVM
import ru.sevoro.photon.screen.base.BaseView
import ru.sevoro.photon.screen.base.FiltersVM

/**
 * @author Sergey Vorobyev
 */
interface NewPhotoCardView : BaseView {

    fun bindFiltersViewModel(viewModel: FiltersVM)

    fun showAlbums(albums: List<AlbumRealm>)

    fun selectAlbum(position: Int)

    fun showSelectedTags(tags: List<String>)

    fun showFilteredTags(tags: List<String>)

    fun showNewAlbumDialog(viewModel: EditAlbumVM)

    fun closeNewAlbumDialog()
}