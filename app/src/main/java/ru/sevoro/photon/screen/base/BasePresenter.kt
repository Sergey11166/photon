package ru.sevoro.photon.screen.base

import android.view.View

import ru.sevoro.photon.screen.root.RootView

/**
 * @author Sergey Vorobyev.
 */
interface BasePresenter<in V : View> {

    fun takeView(view: V)

    fun dropView(view: V?)

    val rootView: RootView?
}
