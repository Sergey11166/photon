package ru.sevoro.photon.screen.search_photo_cards

import android.os.Bundle
import flow.Flow
import mortar.MortarScope
import ru.sevoro.photon.data.dto.*
import ru.sevoro.photon.screen.base.AbsPresenter
import ru.sevoro.photon.screen.base.FiltersVM
import ru.sevoro.photon.screen.home.SearchPhotoCardsSpecification
import ru.sevoro.photon.util.SERVICE_NAME
import rx.Subscriber

class SearchPhotoCardsPresenterImpl(
        private var specification: SearchPhotoCardsSpecification,
        private val model: SearchPhotoCardsModel,
        private val searchVM: SearchPhotoCardsVM,
        private val filtersVM: FiltersVM
) : AbsPresenter<SearchPhotoCardsViewImpl>(), SearchPhotoCardsPresenter {

    private var searchPromts = ArrayList<String>()
    private var selectedTags = mutableMapOf<Int, String>() // <position, tag>
    private var allTags = ArrayList<String>()

    override fun onSearchChanged() {
        val promts = if (searchVM.search.isEmpty()) emptyList<String>() else searchPromts
                .filter { promt ->
                    promt.split(" ")
                            .filter { word ->
                                word.isNotEmpty() && word.startsWith(searchVM.search)
                            }
                            .isNotEmpty()
                }
                .take(5)
        view?.showSearchHistory(promts)
    }

    override fun onTagClicked(tag: String) {
        val position = allTags.indexOf(tag)
        if (!selectedTags.containsValue(tag)) {
            selectedTags.put(position, tag)
            view?.selectTag(position)
        } else {
            selectedTags.remove(allTags.indexOf(tag))
            view?.unSelectTag(position)
        }
    }

    override fun onSearchCheckedButtonClick() {
        model.saveQuery(searchVM.search)
        if (view != null) {
            prepareSearchSpec()
            Flow.get(view).goBack()
        }
    }

    override fun onFilterSearchButtonClick() {
        if (view != null) {
            prepareFilterSpec()
            Flow.get(view).goBack()
        }
    }

    override fun onEnterScope(scope: MortarScope) {
        super.onEnterScope(scope)

        rootView?.showProgress()
        model.getPopularTags().subscribe(object : Subscriber<List<String>>() {
            override fun onCompleted() {}

            override fun onNext(tags: List<String>) {
                rootView?.hideProgress()
                allTags.addAll(tags)
                view?.showTags(allTags)

                specification.tags.forEach { tag ->
                    val pos = allTags.indexOf(tag)
                    if (pos != -1) {
                        selectedTags.put(allTags.indexOf(tag), tag)
                    } else {
                        specification.tags.remove(tag)
                    }
                }

                if (selectedTags.isNotEmpty()) view?.selectTags(selectedTags.keys.toList())
            }

            override fun onError(e: Throwable) {
                rootView?.hideProgress()
                e.printStackTrace()
            }
        })

        model.getAvailablePromts().forEach { promt -> searchPromts.add(promt.toLowerCase()) }

        initSearchVM()
        initFiltersVM()
    }

    override fun onLoad(savedInstanceState: Bundle?) {
        super.onLoad(savedInstanceState)
        view?.bindSearchViewModel(searchVM)
        view?.bindFiltersViewModel(filtersVM)
        if (allTags.isNotEmpty()) {
            view?.showTags(allTags)
            if (selectedTags.isNotEmpty()) view?.selectTags(selectedTags.keys.toList())
        }
        if (searchVM.search.isNotEmpty()) onSearchChanged()
    }

    private fun initSearchVM() {
        searchVM.search = specification.title
    }

    private fun initFiltersVM() {
        when (specification.dish) {
            MEAT -> filtersVM.dishMeat = true
            FISH -> filtersVM.dishFish = true
            VEGETABLES -> filtersVM.dishVegetables = true
            FRUIT -> filtersVM.dishFruits = true
            CHEESE -> filtersVM.dishCheese = true
            DESSERTS -> filtersVM.dishDesserts = true
            DRINKS -> filtersVM.dishDrinks = true
        }

        if (specification.nuances.contains(RED)) filtersVM.tintRed = true
        if (specification.nuances.contains(ORANGE)) filtersVM.tintOrange = true
        if (specification.nuances.contains(YELLOW)) filtersVM.tintYellow = true
        if (specification.nuances.contains(GREEN)) filtersVM.tintGreen = true
        if (specification.nuances.contains(LIGHT_BLUE)) filtersVM.tintBlue = true
        if (specification.nuances.contains(BLUE)) filtersVM.tintDarkBlue = true
        if (specification.nuances.contains(VIOLET)) filtersVM.tintViolet = true
        if (specification.nuances.contains(BROWN)) filtersVM.tintBrown = true
        if (specification.nuances.contains(BLACK)) filtersVM.tintBlack = true
        if (specification.nuances.contains(WHITE)) filtersVM.tintWhite = true

        when (specification.decor) {
            SIMPLE -> filtersVM.decorRegular = true
            HOLIDAY -> filtersVM.decorHoliday = true
        }

        when (specification.temperature) {
            HOT -> filtersVM.tempHot = true
            MIDDLE -> filtersVM.tempComfort = true
            COLD -> filtersVM.tempCold = true
        }

        when (specification.light) {
            NATURAL -> filtersVM.lightNatural = true
            SYNTHETIC -> filtersVM.lightSynthetic = true
            MIXED -> filtersVM.lightMixed = true
        }

        when (specification.lightDirection) {
            DIRECT -> filtersVM.lightDirFront = true
            BACK_LIGHT -> filtersVM.lightDirBack = true
            SIDE_LIGHT -> filtersVM.lightDirSide = true
            MIXED -> filtersVM.lightDirMixed = true
        }

        when (specification.lightSource) {
            ONE -> filtersVM.lightCount1 = true
            TWO -> filtersVM.lightCount2 = true
            THREE -> filtersVM.lightCount3 = true
        }
    }

    private fun prepareSearchSpec() {
        specification.clear()
        specification.title = searchVM.search
        specification.tags.addAll(selectedTags.values)
    }

    private fun prepareFilterSpec() {
        if (filtersVM.tintRed) specification.nuances.add(RED)
        if (filtersVM.tintOrange) specification.nuances.add(ORANGE)
        if (filtersVM.tintYellow) specification.nuances.add(YELLOW)
        if (filtersVM.tintGreen) specification.nuances.add(GREEN)
        if (filtersVM.tintBlue) specification.nuances.add(LIGHT_BLUE)
        if (filtersVM.tintDarkBlue) specification.nuances.add(BLUE)
        if (filtersVM.tintViolet) specification.nuances.add(VIOLET)
        if (filtersVM.tintBrown) specification.nuances.add(BROWN)
        if (filtersVM.tintBlack) specification.nuances.add(BLACK)
        if (filtersVM.tintWhite) specification.nuances.add(WHITE)

        when {
            filtersVM.dishMeat -> specification.dish = MEAT
            filtersVM.dishFish -> specification.dish = FISH
            filtersVM.dishVegetables -> specification.dish = VEGETABLES
            filtersVM.dishFruits -> specification.dish = FRUIT
            filtersVM.dishCheese -> specification.dish = CHEESE
            filtersVM.dishDesserts -> specification.dish = DESSERTS
            filtersVM.dishDrinks -> specification.dish = DRINKS
        }

        when {
            filtersVM.decorRegular -> specification.decor = SIMPLE
            filtersVM.decorHoliday -> specification.decor = HOLIDAY
        }

        when {
            filtersVM.tempHot -> specification.temperature = HOT
            filtersVM.tempComfort -> specification.temperature = MIDDLE
            filtersVM.tempCold -> specification.temperature = COLD
        }

        when {
            filtersVM.lightNatural -> specification.light = NATURAL
            filtersVM.lightSynthetic -> specification.light = SYNTHETIC
            filtersVM.lightMixed -> specification.light = MIXED
        }

        when {
            filtersVM.lightDirFront -> specification.lightDirection = DIRECT
            filtersVM.lightDirBack -> specification.lightDirection = BACK_LIGHT
            filtersVM.lightDirSide -> specification.lightDirection = SIDE_LIGHT
            filtersVM.lightDirMixed -> specification.lightDirection = MIXED
        }

        when {
            filtersVM.lightCount1 -> specification.lightSource = ONE
            filtersVM.lightCount2 -> specification.lightSource = TWO
            filtersVM.lightCount3 -> specification.lightSource = THREE
        }
    }

    override fun initActionBar() {
        rootPresenter.newActionBarBuilder()?.setVisibility(false)?.setTabs(view.getViewPager())?.build()
    }

    override fun initDagger(scope: MortarScope) {
        scope.getService<SearchPhotoCardsScreen.Component>(SERVICE_NAME).inject(this)
    }
}