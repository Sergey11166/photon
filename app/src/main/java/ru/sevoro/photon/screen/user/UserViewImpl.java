package ru.sevoro.photon.screen.user;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.GridLayout;
import android.widget.ImageView;
import android.widget.TextView;

import org.jetbrains.annotations.NotNull;

import io.realm.RealmList;
import ru.sevoro.photon.R;
import ru.sevoro.photon.data.dto.realm.AlbumRealm;
import ru.sevoro.photon.data.dto.realm.PhotoCardRealm;
import ru.sevoro.photon.screen.base.AbsView;

import static android.content.Context.WINDOW_SERVICE;
import static ru.sevoro.photon.util.BindingUtilsKt.loadImage;

/**
 * @author Sergey Vorobyev
 */

public abstract class UserViewImpl<P extends UserPresenter> extends AbsView<P> implements UserView {

    public UserViewImpl(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public void showAlbums(@NotNull RealmList<AlbumRealm> albums) {
        getAlbumsContainer().removeAllViews();

        for (AlbumRealm album : albums) {
            String preview = "";
            int totalFavorites = 0;
            int totalViews = 0;
            if (album.getPhotoCards().size() > 0) {
                preview = album.getPhotoCards().get(0).getPhoto();
                for (PhotoCardRealm card : album.getPhotoCards()) {
                    totalFavorites += card.getFavorites();
                    totalViews += card.getViews();
                }
            }
            View view = LayoutInflater.from(getContext())
                    .inflate(R.layout.item_album, getAlbumsContainer(), false);

            loadImage((ImageView) view.findViewById(R.id.photo), preview, null,
                    ContextCompat.getColor(getContext(), R.color.colorGrayLight), "centerCrop");
            ((TextView) view.findViewById(R.id.title)).setText(album.getTitle());
            ((TextView) view.findViewById(R.id.photo_count)).setText(String.valueOf(album.getPhotoCards().size()));
            ((TextView) view.findViewById(R.id.favorites)).setText(String.valueOf(totalFavorites));
            ((TextView) view.findViewById(R.id.views)).setText(String.valueOf(totalViews));
            getAlbumsContainer().addView(view);

            DisplayMetrics dm = new DisplayMetrics();
            WindowManager wm = (WindowManager) getContext().getSystemService(WINDOW_SERVICE);
            if (wm != null) {
                wm.getDefaultDisplay().getMetrics(dm);
            }

            GridLayout.LayoutParams lp = new GridLayout.LayoutParams();
            lp.width = (int) (dm.widthPixels * 0.5);
            view.setLayoutParams(lp);

            view.setOnClickListener(v -> presenter.onAlbumClick(album));
        }
    }

    abstract GridLayout getAlbumsContainer();
}

