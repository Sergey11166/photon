package ru.sevoro.photon.screen.user

import ru.sevoro.photon.data.dto.realm.UserRealm
import ru.sevoro.photon.screen.base.AbsModel
import rx.Observable
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import java.io.IOException

class UserModelImpl : AbsModel(), UserModel {

    override fun deleteAlbum(albumId: String) {

    }

    override fun getUser(userId: String): Observable<UserRealm?> {
        return updateUser(userId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map { realmManager.getEntityById(UserRealm::class.java, userId) }
    }

    override fun getCurrentUserId() = prefsManager.getCurrentUserId()

    override fun updateAvatar(imagePath: String) {
        jobQueueManager.addJobInBackground(UploadAvatarJob(
                imagePath,
                prefsManager.getCurrentUserId(),
                prefsManager.getToken()))

        realmManager.updateAvatar(prefsManager.getCurrentUserId(), imagePath)
    }

    override fun updateProfile(username: String, login: String) {
        realmManager.updateUserInfo(prefsManager.getCurrentUserId(), username, login)
        jobQueueManager.addJobInBackground(UpdateUserInfoJob(
                prefsManager.getCurrentUserId(),
                prefsManager.getToken()))
    }

    override fun logout() {
        prefsManager.deleteToken()
    }

    @Throws(IOException::class)
    override fun createTempFileForCamera(userId: String) = fileManager.createFileInCache(fileName = "$userId.jpg")
}