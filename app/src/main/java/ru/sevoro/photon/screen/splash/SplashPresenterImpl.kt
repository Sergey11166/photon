package ru.sevoro.photon.screen.splash

import flow.Direction.REPLACE
import flow.Flow
import mortar.MortarScope
import ru.sevoro.photon.screen.base.AbsPresenter
import ru.sevoro.photon.screen.home.HomeScreen
import ru.sevoro.photon.screen.home.SearchPhotoCardsSpecification
import ru.sevoro.photon.screen.root.ActionBarBuilder
import ru.sevoro.photon.util.L
import ru.sevoro.photon.util.SERVICE_NAME
import rx.Subscriber

/**
 * @author Sergey Vorobyev
 */
class SplashPresenterImpl(private val model: SplashModel) :
        AbsPresenter<SplashViewImpl>(), SplashPresenter {

    override fun onEnterScope(scope: MortarScope) {
        super.onEnterScope(scope)
        rootView?.showProgress()
        model.updateLocalData().subscribe(object : Subscriber<Any>() {
            override fun onNext(t: Any?) {
                L.d("updateLocalData onNext: $t")
            }

            override fun onCompleted() {
                L.d("updateLocalData onCompleted")
                goToMainScreen()
            }

            override fun onError(e: Throwable) {
                L.d("updateLocalData onError")
                e.printStackTrace()
                goToMainScreen()
            }
        })
    }

    private fun goToMainScreen() {
        rootView?.hideProgress()
        model.startUpdatePhotoCardsWithTimer()
        if (view != null) {
            rootView?.setBackground(android.R.color.white)
            rootView?.showBottomNavigation()
            Flow.get(view).replaceTop(HomeScreen(SearchPhotoCardsSpecification()).activateOnlyFade(), REPLACE)
        }
    }

    override fun initActionBar() {
        val actionBarBuilder: ActionBarBuilder? = rootPresenter.newActionBarBuilder()
        @Suppress("IfThenToSafeAccess")
        if (actionBarBuilder != null) {
            actionBarBuilder
                    .setVisibility(false)
                    .build()
        }
    }

    override fun initDagger(scope: MortarScope) =
            (scope.getService<Any>(SERVICE_NAME) as SplashScreen.Component).inject(this)
}