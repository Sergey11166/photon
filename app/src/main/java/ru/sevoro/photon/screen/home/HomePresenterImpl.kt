package ru.sevoro.photon.screen.home

import android.os.Bundle
import android.view.MenuItem.SHOW_AS_ACTION_ALWAYS
import android.view.MenuItem.SHOW_AS_ACTION_NEVER
import flow.Flow
import mortar.MortarScope
import ru.sevoro.photon.R
import ru.sevoro.photon.data.dto.realm.PhotoCardRealm
import ru.sevoro.photon.data.error.ApiError
import ru.sevoro.photon.dialog.SignInVM
import ru.sevoro.photon.dialog.SignUpVM
import ru.sevoro.photon.screen.base.AbsPresenter
import ru.sevoro.photon.screen.photo_card.PhotoCardScreen
import ru.sevoro.photon.screen.root.ActionBarBuilder
import ru.sevoro.photon.screen.root.MenuItemHolder
import ru.sevoro.photon.screen.search_photo_cards.SearchPhotoCardsScreen
import ru.sevoro.photon.util.L
import ru.sevoro.photon.util.SERVICE_NAME
import ru.sevoro.photon.util.showToast
import rx.Subscriber
import rx.Subscription

class HomePresenterImpl(
        private val model: HomeModel,
        private var specification: SearchPhotoCardsSpecification
) : AbsPresenter<HomeViewImpl>(), HomePresenter {

    private val signUpVM = SignUpVM()
    private val signInVM = SignInVM()

    private var photoCardSubscription: Subscription? = null

    override fun onPhotoCardClick(photoCard: PhotoCardRealm) {
        Flow.get(view).set(PhotoCardScreen(photoCard, isFromAlbum = false))
    }

    override fun onLoad(savedInstanceState: Bundle?) {
        super.onLoad(savedInstanceState)
        if (!isFilterEmpty()) {
            rootView?.showSnackBar(
                    R.string.message_filters_applied,
                    R.string.snack_bar_action_reset_filters,
                    { resetFilters() })
        }
        updatePhotoCards()
    }

    override fun dropView(view: HomeViewImpl?) {
        photoCardSubscription?.unsubscribe()
        super.dropView(view)
    }

    private fun updatePhotoCards() {
        photoCardSubscription = model.getPhotoCards(specification)
                .subscribe(object : Subscriber<PhotoCardRealm>() {
                    override fun onError(e: Throwable) {
                        L.d("getPhotoCards() onError")
                        e.printStackTrace()
                    }

                    override fun onCompleted() {
                        L.d("getPhotoCards() onCompleted")
                    }

                    override fun onNext(photoCard: PhotoCardRealm) {
                        L.d("getPhotoCards() onNext $photoCard")
                        view?.addPhotoCard(photoCard)
                    }
                })
    }

    override fun onOkButtonClickSignUpDialog() {
        if (!signUpVM.isLoginValid ||
                !signUpVM.isEmailValid ||
                !signUpVM.isUsernameValid ||
                !signUpVM.isPasswordValid) {
            view?.context?.showToast(R.string.message_tape_valid_data)
        } else {
            rootView?.showProgress()
            view?.closeSignUpDialog()
            model.signUp(signUpVM.login, signUpVM.email, signUpVM.username, signUpVM.password)
                    .subscribe(object : Subscriber<String>() {

                        override fun onNext(userId: String) {
                            rootView?.hideProgress()
                            rootView?.showSnackBar(R.string.auth_message_logged_in)
                            initActionBar()
                        }

                        override fun onError(e: Throwable) {
                            rootView?.hideProgress()
                            view?.context?.showToast(e.message!!)
                            view?.showSignInDialog(signUpVM)
                        }

                        override fun onCompleted() {}
                    })
        }
    }

    override fun onOkButtonClickSignInDialog() {
        if (!signInVM.isEmailValid || !signInVM.isPasswordValid) {
            view?.context?.showToast(R.string.message_tape_valid_data)
        } else {
            rootView?.showProgress()
            view?.closeSignInDialog()
            model.signIn(signInVM.email, signInVM.password).subscribe(object : Subscriber<String>() {

                override fun onNext(userId: String) {
                    rootView?.hideProgress()
                    rootView?.showSnackBar(R.string.auth_message_logged_in)
                    initActionBar()
                }

                override fun onError(e: Throwable) {
                    rootView?.hideProgress()
                    if (e is ApiError && e.code == 404) {
                        view?.context?.showToast(R.string.auth_message_user_not_found)
                    } else {
                        view?.context?.showToast(e.message!!)
                    }
                    view?.showSignInDialog(signInVM)
                }

                override fun onCompleted() {}
            })
        }
    }

    override fun initActionBar() {
        val actionBarBuilder: ActionBarBuilder? = rootPresenter.newActionBarBuilder()
        if (actionBarBuilder != null) {
            actionBarBuilder
                    .setVisibility(true)
                    .setTitle(R.string.app_name)
                    .setOverflowIcon(R.drawable.ic_custom_gear_black_24dp)
                    .addAction(MenuItemHolder(
                            if (isFilterEmpty()) R.drawable.ic_custom_search_black_24dp else R.drawable.ic_style_accent_24dp,
                            SHOW_AS_ACTION_ALWAYS, 0,
                            {
                                if (view != null) {
                                    Flow.get(view).set(SearchPhotoCardsScreen(specification))
                                }
                            }))
            if (model.isAuthorized()) {
                actionBarBuilder
                        .addAction(MenuItemHolder(0, SHOW_AS_ACTION_NEVER, R.string.main_menu_logout, {
                            model.logout()
                            initActionBar()
                            rootView?.showSnackBar(R.string.auth_message_logged_out)
                        }))
            } else {
                actionBarBuilder
                        .addAction(MenuItemHolder(0, SHOW_AS_ACTION_NEVER, R.string.main_menu_sign_in,
                                { view?.showSignInDialog(signInVM) }))
                        .addAction(MenuItemHolder(0, SHOW_AS_ACTION_NEVER, R.string.main_menu_sign_up,
                                { view?.showSignUpDialog(signUpVM) }))
            }
            actionBarBuilder.build()
        }
    }

    private fun isFilterEmpty() =
            specification.title.isEmpty() &&
                    specification.dish.isEmpty() &&
                    specification.nuances.isEmpty() &&
                    specification.decor.isEmpty() &&
                    specification.light.isEmpty() &&
                    specification.lightDirection.isEmpty() &&
                    specification.lightSource.isEmpty() &&
                    specification.tags.isEmpty()

    private fun resetFilters() {
        specification.title = ""
        specification.dish = ""
        specification.decor = ""
        specification.light = ""
        specification.lightDirection = ""
        specification.lightSource = ""
        specification.tags.clear()
        specification.nuances.clear()

        updatePhotoCards()
        if (rootView?.currentScreen is HomeView) initActionBar()
    }

    override fun initDagger(scope: MortarScope) {
        scope.getService<HomeScreen.Component>(SERVICE_NAME).inject(this)
    }
}