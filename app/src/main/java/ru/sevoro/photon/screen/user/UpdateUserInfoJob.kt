package ru.sevoro.photon.screen.user

import com.birbit.android.jobqueue.Job
import com.birbit.android.jobqueue.Params
import com.birbit.android.jobqueue.RetryConstraint
import ru.sevoro.photon.App
import ru.sevoro.photon.INITIAL_BACK_OFF_IN_MS
import ru.sevoro.photon.data.JobPriority
import ru.sevoro.photon.data.dto.realm.UserRealm
import ru.sevoro.photon.data.dto.request.EditProfileRequest
import ru.sevoro.photon.util.L

/**
 * @author Sergey Vorobyev
 */
class UpdateUserInfoJob(
        private val userId: String,
        private val token: String
) : Job(Params(JobPriority.HIGH).requireNetwork().persist()) {

    override fun onRun() {
        L.d("onRun")

        val realmManager = App.appComponent.realmManager()
        val restService = App.appComponent.restService()
        val localUser = realmManager.getEntityById(UserRealm::class.java, userId)

        if (localUser == null || localUser.isSynced) return

        restService.editUser(token, userId, EditProfileRequest(localUser.username, localUser.login))
                .subscribe { response ->
                    L.d("user info updated, code ${response.code()}, body ${response.body()}")
                    val newUser = UserRealm(response.body()!!)
                    newUser.isSynced = true
                    realmManager.saveEntity(newUser)
                }
    }

    override fun shouldReRunOnThrowable(throwable: Throwable, runCount: Int, maxRunCount: Int): RetryConstraint {
        L.d("shouldReRunOnThrowable")
        return RetryConstraint.createExponentialBackoff(runCount, INITIAL_BACK_OFF_IN_MS)
    }

    override fun onAdded() {
        L.d("onAdded")
    }

    override fun onCancel(cancelReason: Int, throwable: Throwable?) {
        L.d("onCancel")
    }
}