package ru.sevoro.photon.screen.search_photo_cards

import android.content.Context
import android.support.v4.content.ContextCompat
import android.support.v4.view.ViewPager
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.TextView
import ru.sevoro.photon.R
import ru.sevoro.photon.screen.base.AbsView
import ru.sevoro.photon.screen.base.FiltersVM
import ru.sevoro.photon.util.getComponent

class SearchPhotoCardsViewImpl(context: Context, attrs: AttributeSet?) :
        AbsView<SearchPhotoCardsPresenter>(context, attrs), SearchPhotoCardsView {

    private lateinit var viewPager: ViewPager

    private val adapter = SearchPhotoCardsViewPagerAdapter(context, { presenter.onSearchChanged() },
            { view ->
                when (view.id) {
                    R.id.filter_search_button -> presenter.onFilterSearchButtonClick()
                    R.id.search_button -> presenter.onSearchCheckedButtonClick()
                    R.id.search_checked_button -> presenter.onSearchCheckedButtonClick()
                }
            }
    )

    override fun onFinishInflate() {
        super.onFinishInflate()
        if (!isInEditMode) {
            viewPager = findViewById(R.id.view_pager)
            viewPager.adapter = adapter
        }
    }

    override fun bindSearchViewModel(viewModel: SearchPhotoCardsVM) {
        adapter.prepareSearchPage(viewPager, viewModel)
    }

    override fun bindFiltersViewModel(viewModel: FiltersVM) {
        adapter.prepareFilterPage(viewPager, viewModel)
    }

    override fun showSearchHistory(history: List<String>) {
        val binding = adapter.getSearchPageBinding()
        binding.searchPromts.removeAllViews()
        binding.searchPromtsDividerBottom.visibility = if (history.isEmpty()) GONE else VISIBLE
        history.forEach { item ->
            val view = LayoutInflater.from(context).inflate(R.layout.item_promts_list, binding.searchPromts, false)
            view.findViewById<TextView>(R.id.value).text = item
            binding.searchPromts.addView(view)
            view.setOnClickListener {
                binding.searchInput.setText(item)
                binding.searchInput.setSelection(binding.searchInput.text.length)
            }
        }
    }

    override fun showTags(tags: List<String>) {
        val binding = adapter.getSearchPageBinding()
        tags.forEach { item ->
            val tag = "#$item"
            val view = LayoutInflater.from(context).inflate(R.layout.item_tag, binding.tagsContainer, false)
            view.findViewById<TextView>(R.id.tag).text = tag
            view.setOnClickListener { presenter.onTagClicked(item) }
            binding.tagsContainer.addView(view)
        }
    }

    override fun selectTags(tagsPositions: List<Int>) {
        val binding = adapter.getSearchPageBinding()
        tagsPositions.forEach { pos ->
            val view = binding.tagsContainer.getChildAt(pos)
            if (view != null) {
                setSelectedTagView(view, isSelected = true)
            }
        }
    }

    override fun selectTag(tagPosition: Int) {
        val binding = adapter.getSearchPageBinding()
        val view = binding.tagsContainer.getChildAt(tagPosition)
        if (view != null) {
            setSelectedTagView(view, isSelected = true)
        }
    }

    override fun unSelectTag(tagPosition: Int) {
        val binding = adapter.getSearchPageBinding()
        val view = binding.tagsContainer.getChildAt(tagPosition)
        if (view != null) {
            setSelectedTagView(view, isSelected = false)
        }
    }

    private fun setSelectedTagView(view: View, isSelected: Boolean) {
        view.setBackgroundResource(if (isSelected) R.drawable.border_tag_accent else R.drawable.border_tag)
        view.findViewById<TextView>(R.id.tag).setTextColor(ContextCompat
                .getColor(context, if (isSelected) R.color.colorAccent else android.R.color.black))
    }

    fun getViewPager() = viewPager

    override fun initDagger(context: Context) {
        context.getComponent<SearchPhotoCardsScreen.Component>().inject(this)
    }
}