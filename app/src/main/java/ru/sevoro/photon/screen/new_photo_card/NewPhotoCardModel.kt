package ru.sevoro.photon.screen.new_photo_card

import ru.sevoro.photon.data.dto.realm.AlbumRealm
import ru.sevoro.photon.data.dto.realm.TagRealm
import rx.Observable

/**
 * @author Sergey Vorobyev
 */
interface NewPhotoCardModel {

    fun getAlbumsOfCurrentUser(): List<AlbumRealm>

    fun getAvailableTags(): List<TagRealm>

    fun saveTag(tag: String)

    fun createNewAlbum(name: String, description: String): Observable<AlbumRealm>

    fun uploadNewPhotoCard(
            photoName: String,
            albumId: String,
            photoLocalPath: String,
            tags: List<String>,
            dish: String,
            tints: List<String>,
            decor: String,
            temperature: String,
            light: String,
            lightDir: String,
            lightCount: String): Observable<String>
}