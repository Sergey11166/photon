package ru.sevoro.photon.screen.user

import ru.sevoro.photon.data.dto.realm.AlbumRealm
import ru.sevoro.photon.data.dto.realm.UserRealm
import rx.Observable
import java.io.File
import java.io.IOException

/**
 * @author Sergey Vorobyev
 */
interface UserModel {

    fun deleteAlbum(albumId: String)

    fun getUser(userId: String): Observable<UserRealm?>

    fun getCurrentUserId(): String

    fun updateAvatar(imagePath: String)

    fun updateProfile(username: String, login: String)

    fun createNewAlbum(name: String, description: String): Observable<AlbumRealm>

    fun logout()

    @Throws(IOException::class)
    fun createTempFileForCamera(userId: String): File
}