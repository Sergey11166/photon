package ru.sevoro.photon.screen.album

import ru.sevoro.photon.screen.base.AbsModel

class AlbumModelImpl : AbsModel(), AlbumModel {

    override fun updateAlbumInfo(albumId: String, title: String, description: String) {
        realmManager.updateAlbumInfo(albumId, title, description)
        jobQueueManager.addJobInBackground(UpdateAlbumInfoJob(
                prefsManager.getCurrentUserId(),
                albumId,
                prefsManager.getToken()))
    }

    override fun deleteAlbum(albumId: String) {
        realmManager.deleteAlbum(albumId)
        jobQueueManager.addJobInBackground(DeleteAlbumJob(
                prefsManager.getCurrentUserId(),
                albumId,
                prefsManager.getToken()))
    }
}