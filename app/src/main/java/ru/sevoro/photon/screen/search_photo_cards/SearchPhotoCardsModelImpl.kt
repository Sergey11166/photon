package ru.sevoro.photon.screen.search_photo_cards

import ru.sevoro.photon.data.dto.realm.PromtRealm
import ru.sevoro.photon.data.transformers.RestCallTransformer
import ru.sevoro.photon.screen.base.AbsModel
import rx.Observable
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers

class SearchPhotoCardsModelImpl : AbsModel(), SearchPhotoCardsModel {

    override fun saveQuery(search: String) {
        realmManager.saveEntity(PromtRealm(search))
    }

    override fun getPopularTags(): Observable<List<String>> {
        @Suppress("UNCHECKED_CAST")
        return restService.getTags()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .compose(restCallTransformer as RestCallTransformer<List<String>>)
                .onErrorReturn { realmManager.getAllTags().map { it.value } }
    }

    override fun getAvailablePromts() = realmManager.getAllPromts().map { it.value }
}