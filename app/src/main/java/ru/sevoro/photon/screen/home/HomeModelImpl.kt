package ru.sevoro.photon.screen.home

import ru.sevoro.photon.data.dto.realm.PhotoCardRealm
import ru.sevoro.photon.screen.not_authorized.AuthModelImpl
import rx.Observable

class HomeModelImpl : AuthModelImpl(), HomeModel {

    override fun getPhotoCards(specification: SearchPhotoCardsSpecification): Observable<PhotoCardRealm> {
        return realmManager.getAllPhotoCardsHot(specification)
                .map { realmManager.getCopyFromRealm(it) }
                .distinct { it.id }
    }

    override fun isAuthorized() = prefsManager.getToken().isNotEmpty()

    override fun logout() {
        prefsManager.deleteToken()
    }
}