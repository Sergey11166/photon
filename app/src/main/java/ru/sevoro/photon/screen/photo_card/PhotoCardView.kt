package ru.sevoro.photon.screen.photo_card

import android.graphics.Bitmap
import io.realm.RealmList
import ru.sevoro.photon.data.dto.realm.TagRealm
import ru.sevoro.photon.screen.base.BaseView

/**
 * @author Sergey Vorobyev
 */
interface PhotoCardView : BaseView {

    fun showTags(tags: RealmList<TagRealm>)

    fun getPhotoBitmap(): Bitmap
}