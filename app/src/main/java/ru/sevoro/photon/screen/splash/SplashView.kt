package ru.sevoro.photon.screen.splash

import ru.sevoro.photon.screen.base.BaseView

/**
 * @author Sergey Vorobyev
 */
interface SplashView : BaseView
