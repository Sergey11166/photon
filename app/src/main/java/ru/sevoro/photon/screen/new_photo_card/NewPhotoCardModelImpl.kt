package ru.sevoro.photon.screen.new_photo_card

import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import ru.sevoro.photon.data.dto.Filter
import ru.sevoro.photon.data.dto.realm.*
import ru.sevoro.photon.data.dto.request.CreatePhotoCardRequest
import ru.sevoro.photon.data.dto.response.CreatedIdResponse
import ru.sevoro.photon.data.dto.response.UploadImageResponse
import ru.sevoro.photon.data.transformers.RestCallTransformer
import ru.sevoro.photon.screen.base.AbsModel
import ru.sevoro.photon.util.toRealmList
import rx.Observable
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import java.io.File

class NewPhotoCardModelImpl : AbsModel(), NewPhotoCardModel {

    override fun getAlbumsOfCurrentUser() =
            realmManager.getEntityById(UserRealm::class.java, prefsManager.getCurrentUserId())!!.albums!!

    override fun getAvailableTags() = realmManager.getAllTags()

    override fun uploadNewPhotoCard(
            photoName: String,
            albumId: String,
            photoLocalPath: String,
            tags: List<String>,
            dish: String,
            tints: List<String>,
            decor: String,
            temperature: String,
            light: String,
            lightDir: String,
            lightCount: String): Observable<String> {

        val imageFile = File(photoLocalPath)
        val imageRequestBody = RequestBody.create(MediaType.parse("multipart/form-data"), imageFile)
        val imagePart = MultipartBody.Part.createFormData("image", imageFile.name, imageRequestBody)
        var photoUrl: String = ""

        @Suppress("UNCHECKED_CAST")
        return restService.uploadImage(prefsManager.getToken(), prefsManager.getCurrentUserId(), imagePart)
                .compose(restCallTransformer as RestCallTransformer<UploadImageResponse>)
                .flatMap {
                    photoUrl = it.image
                    val filters = Filter(dish, tints, decor, temperature, light, lightDir, lightCount)
                    val photoCardRequestBody = CreatePhotoCardRequest(photoName, albumId, it.image, tags, filters)
                    restService
                            .createPhotoCard(prefsManager.getToken(), prefsManager.getCurrentUserId(), photoCardRequestBody)
                            .compose(restCallTransformer as RestCallTransformer<CreatedIdResponse>)
                }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext {
                    val photoCard = PhotoCardRealm()
                    photoCard.id = it.id
                    photoCard.owner = prefsManager.getCurrentUserId()
                    photoCard.title = photoName
                    photoCard.photo = photoUrl
                    photoCard.tags = tags.toRealmList { TagRealm(it) }
                    photoCard.dish = dish
                    photoCard.nuances = tints.toRealmList { RealmString(it) }
                    photoCard.decor = decor
                    photoCard.temperature = temperature
                    photoCard.light = light
                    photoCard.lightDirection = lightDir
                    photoCard.lightSource = lightCount

                    realmManager.saveEntity(photoCard)
                    realmManager.addToAlbum(it.id, albumId)
                }
                .flatMap { Observable.just(it.id) }
    }

    override fun saveTag(tag: String) {
        realmManager.saveEntity(TagRealm(tag))
    }
}