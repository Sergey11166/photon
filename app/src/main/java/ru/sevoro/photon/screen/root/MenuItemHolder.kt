package ru.sevoro.photon.screen.root

/**
 * @author Sergey Vorobyev
 */
data class MenuItemHolder(
        val iconResId: Int,
        val showFlag: Int,
        val title: Int,
        val listener: () -> Unit)

