package ru.sevoro.photon.screen.album

import com.birbit.android.jobqueue.Job
import com.birbit.android.jobqueue.Params
import com.birbit.android.jobqueue.RetryConstraint
import ru.sevoro.photon.App
import ru.sevoro.photon.INITIAL_BACK_OFF_IN_MS
import ru.sevoro.photon.data.JobPriority
import ru.sevoro.photon.data.dto.realm.AlbumRealm
import ru.sevoro.photon.data.dto.request.EditAlbumRequest
import ru.sevoro.photon.util.L

/**
 * @author Sergey Vorobyev
 */
class UpdateAlbumInfoJob(
        private val userId: String,
        private val albumId: String,
        private val token: String
) : Job(Params(JobPriority.HIGH).requireNetwork().persist()) {

    override fun onRun() {
        L.d("onRun")

        val realmManager = App.appComponent.realmManager()
        val restService = App.appComponent.restService()
        val localAlbum = realmManager.getEntityById(AlbumRealm::class.java, albumId)

        if (localAlbum == null || localAlbum.isSynced) return

        restService.editAlbum(token, userId, albumId, EditAlbumRequest(localAlbum.title, localAlbum.description))
                .subscribe { response ->
                    L.d("album info updated, code ${response.code()}, body ${response.body()}")
                    val newAlbum = AlbumRealm(response.body()!!)
                    newAlbum.isSynced = true
                    realmManager.saveEntity(newAlbum)
                }

    }

    override fun shouldReRunOnThrowable(throwable: Throwable, runCount: Int, maxRunCount: Int): RetryConstraint {
        L.d("shouldReRunOnThrowable")
        return RetryConstraint.createExponentialBackoff(runCount, INITIAL_BACK_OFF_IN_MS)
    }

    override fun onAdded() {
        L.d("onAdded")
    }

    override fun onCancel(cancelReason: Int, throwable: Throwable?) {
        L.d("onCancel")
    }
}