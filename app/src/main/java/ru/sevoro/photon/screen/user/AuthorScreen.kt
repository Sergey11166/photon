package ru.sevoro.photon.screen.user

import dagger.Provides
import ru.sevoro.photon.R
import ru.sevoro.photon.dagger.DaggerScope
import ru.sevoro.photon.flow_mortar.Screen
import ru.sevoro.photon.screen.base.AbsScreen
import ru.sevoro.photon.screen.root.RootActivity
import java.util.*

/**
 * @author Sergey Vorobyev
 */
@Screen(R.layout.screen_author)
class AuthorScreen(private val userId: String) : AbsScreen<RootActivity.RootComponent>() {

    override fun createScreenComponent(parentComponent: RootActivity.RootComponent?): Any {
        return DaggerAuthorScreen_Component.builder()
                .rootComponent(parentComponent)
                .module(Module(userId))
                .build()
    }

    override fun hashCode() = Objects.hash(userId)

    override fun equals(other: Any?) = other === this

    @DaggerScope(AuthorScreen::class)
    @dagger.Component(dependencies = arrayOf(RootActivity.RootComponent::class),
            modules = arrayOf(Module::class))
    interface Component {
        fun inject(view: AuthorViewImpl)

        fun inject(presenter: AuthorPresenterImpl)
    }

    @dagger.Module
    class Module(private val userId: String) {

        @Provides
        @DaggerScope(AuthorScreen::class)
        fun provideModel(): UserModel {
            return UserModelImpl()
        }

        @Provides
        @DaggerScope(AuthorScreen::class)
        fun providePresenter(model: UserModel, viewModel: UserVM): AuthorPresenter {
            return AuthorPresenterImpl(userId, model, viewModel)
        }

        @Provides
        @DaggerScope(AuthorScreen::class)
        fun provideViewModel(): UserVM {
            return UserVM()
        }
    }
}