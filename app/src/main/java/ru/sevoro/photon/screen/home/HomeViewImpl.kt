package ru.sevoro.photon.screen.home

import android.content.Context
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.AttributeSet
import ru.sevoro.photon.R
import ru.sevoro.photon.data.dto.realm.PhotoCardRealm
import ru.sevoro.photon.dialog.SignInVM
import ru.sevoro.photon.dialog.SignUpVM
import ru.sevoro.photon.dialog.getInstanceSignInDialog
import ru.sevoro.photon.dialog.getInstanceSignUpDialog
import ru.sevoro.photon.screen.base.AbsView
import ru.sevoro.photon.util.getComponent

class HomeViewImpl(context: Context, attrs: AttributeSet?) : AbsView<HomePresenter>(context, attrs), HomeView {

    private lateinit var recyclerView: RecyclerView
    private lateinit var adapter: HomePhotoCardAdapter

    private val signUpDialog = getInstanceSignUpDialog { view ->
        when (view.id) {
            R.id.ok_button -> presenter.onOkButtonClickSignUpDialog()
        }
    }

    private val signInDialog = getInstanceSignInDialog { view ->
        when (view.id) {
            R.id.ok_button -> presenter.onOkButtonClickSignInDialog()
        }
    }

    override fun onFinishInflate() {
        super.onFinishInflate()
        if (!isInEditMode) {
            adapter = HomePhotoCardAdapter({ position -> presenter.onPhotoCardClick(adapter.items[position]) })
            recyclerView = findViewById(R.id.recycler)
            recyclerView.layoutManager = GridLayoutManager(context, 2)
            recyclerView.adapter = adapter
        }
    }

    override fun addPhotoCard(photoCard: PhotoCardRealm) {
        adapter.addPhotoCard(photoCard)
    }

    override fun showSignInDialog(viewModel: SignInVM) {
        signInDialog.setViewModel(viewModel)
        showFragmentDialog(signInDialog)
    }

    override fun closeSignInDialog() {
        signInDialog.dismissAllowingStateLoss()
    }

    override fun showSignUpDialog(viewModel: SignUpVM) {
        signUpDialog.setViewModel(viewModel)
        showFragmentDialog(signUpDialog)
    }

    override fun closeSignUpDialog() {
        signUpDialog.dismissAllowingStateLoss()
    }

    override fun initDagger(context: Context) {
        context.getComponent<HomeScreen.Component>().inject(this)
    }
}