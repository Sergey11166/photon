package ru.sevoro.photon.screen.user

import com.birbit.android.jobqueue.Job
import com.birbit.android.jobqueue.Params
import com.birbit.android.jobqueue.RetryConstraint
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import ru.sevoro.photon.App
import ru.sevoro.photon.INITIAL_BACK_OFF_IN_MS
import ru.sevoro.photon.data.JobPriority
import ru.sevoro.photon.data.dto.request.EditProfileRequest
import ru.sevoro.photon.util.L
import java.io.File

/**
 * @author Sergey Vorobyev
 */
class UploadAvatarJob(
        private val imagePath: String,
        private val userId: String,
        private val token: String
) : Job(Params(JobPriority.HIGH).requireNetwork().persist()) {

    override fun onRun() {
        L.d("onRun")

        val file = File(imagePath)
        val requestBody = RequestBody.create(MediaType.parse("multipart/form-data"), file)
        val part = MultipartBody.Part.createFormData("image", file.name, requestBody)

        val realmManager = App.appComponent.realmManager()
        val restService = App.appComponent.restService()

        if (part == null) return

        restService.uploadImage(token, userId, part)
                .doOnNext { response ->
                    L.d("image uploaded")
                    realmManager.updateAvatar(userId, response.body()!!.image)
                }
                .flatMap { response ->
                    val request = EditProfileRequest(avatar = response.body()!!.image)
                    restService.editUser(token, userId, request)
                }
                .subscribe { response ->
                    L.d("user info updated, code ${response.code()}, body ${response.body()}")
                }
    }

    override fun shouldReRunOnThrowable(throwable: Throwable, runCount: Int, maxRunCount: Int): RetryConstraint {
        L.d("shouldReRunOnThrowable")
        return RetryConstraint.createExponentialBackoff(runCount, INITIAL_BACK_OFF_IN_MS)
    }

    override fun onAdded() {
        L.d("onAdded")
    }

    override fun onCancel(cancelReason: Int, throwable: Throwable?) {
        L.d("onCancel")
    }
}