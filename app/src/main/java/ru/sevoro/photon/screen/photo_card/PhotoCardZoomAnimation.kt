package ru.sevoro.photon.screen.photo_card

import android.support.animation.DynamicAnimation
import android.support.animation.SpringAnimation
import android.support.animation.SpringForce
import android.view.MotionEvent
import android.view.View
import ru.sevoro.photon.R
import ru.sevoro.photon.util.L


/**
 * @author Sergey Vorobyev
 */
class PhotoCardZoomAnimation(
        private val animatedView: View,
        private val relatedView: View
) {

    private val photoXScaleAnimation = createSpringAnimation(
            animatedView, SpringAnimation.SCALE_X,
            animatedView.scaleX,
            SpringForce.STIFFNESS_MEDIUM,
            SpringForce.DAMPING_RATIO_NO_BOUNCY)

    private val photoYScaleAnimation = createSpringAnimation(
            animatedView, SpringAnimation.SCALE_Y,
            animatedView.scaleY,
            SpringForce.STIFFNESS_MEDIUM,
            SpringForce.DAMPING_RATIO_NO_BOUNCY)

    private val photoYTranslationAnimation = createSpringAnimation(
            animatedView, SpringAnimation.TRANSLATION_Y,
            animatedView.translationY,
            SpringForce.STIFFNESS_MEDIUM,
            SpringForce.DAMPING_RATIO_NO_BOUNCY)

    private val descriptionYTranslationAnimation = createSpringAnimation(
            relatedView, SpringAnimation.TRANSLATION_Y,
            relatedView.translationY,
            SpringForce.STIFFNESS_MEDIUM,
            SpringForce.DAMPING_RATIO_NO_BOUNCY
    )

    private var titleView: View = animatedView.findViewById(R.id.title)

    private var lastTouchY = 0f

    init {

        descriptionYTranslationAnimation
                .addEndListener { _, _, _, _ -> titleView.animate().alpha(1f).start() }

        animatedView.setOnTouchListener { v, event ->
            when (event.actionMasked) {

                MotionEvent.ACTION_DOWN -> titleView.animate().alpha(0f).start()

                MotionEvent.ACTION_MOVE -> {
                    val eventY = event.rawY
                    val viewPositionY = if (lastTouchY < eventY) v.y * 2 - v.height else v.y * 2
                    val deltaY = eventY - viewPositionY
                    val currentScaleY = v.scaleY

                    L.d("viewPositionY $viewPositionY" +
                            "\n deltaY $deltaY" +
                            "\n lastTouchY $lastTouchY" +
                            "\n eventY $eventY" +
                            "\n currentScaleY $currentScaleY")

                    if (lastTouchY < eventY) {
                        val newScale = currentScaleY + deltaY * 0.00005f
                        if (newScale > 1 && newScale < 1.6) {
                            animatedView.animate()
                                    .scaleX(newScale)
                                    .scaleY(newScale)
                                    .translationY(v.height * 0.5f * (newScale - 1))
                                    .setDuration(0)
                                    .start()

                            relatedView.animate()
                                    .translationY(v.height * (newScale - 1))
                                    .setDuration(0)
                                    .start()
                        }
                    }

                    lastTouchY = eventY
                }

                MotionEvent.ACTION_UP -> {
                    photoXScaleAnimation.start()
                    photoYScaleAnimation.start()
                    photoYTranslationAnimation.start()
                    descriptionYTranslationAnimation.start()
                }
            }

            return@setOnTouchListener true
        }
    }
}

fun createSpringAnimation(view: View,
                          property: DynamicAnimation.ViewProperty,
                          finalPosition: Float,
                          stiffness: Float,
                          dampingRatio: Float): SpringAnimation {

    val animation = SpringAnimation(view, property)
    val springForce = SpringForce(finalPosition)
    springForce.stiffness = stiffness
    springForce.dampingRatio = dampingRatio
    animation.spring = springForce
    return animation
}