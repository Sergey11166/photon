package ru.sevoro.photon.screen.root

import android.content.Intent
import ru.sevoro.photon.App
import ru.sevoro.photon.R
import ru.sevoro.photon.data.dto.ActivityResultDto
import ru.sevoro.photon.data.dto.PermissionsResultDto
import ru.sevoro.photon.screen.base.AbsScreen
import ru.sevoro.photon.screen.choice_photo.ChoicePhotoScreen
import ru.sevoro.photon.screen.home.HomeScreen
import ru.sevoro.photon.screen.home.SearchPhotoCardsSpecification
import ru.sevoro.photon.screen.not_authorized.NotAuthorizedScreen
import ru.sevoro.photon.screen.user.ProfileScreen
import rx.subjects.PublishSubject
import java.io.File

/**
 * @author Sergey Vorobyev.
 */
class RootPresenterImpl(
        private val activityResultSubject: PublishSubject<ActivityResultDto>,
        private val permissionsResultSubject: PublishSubject<PermissionsResultDto>)
    : RootPresenter {

    private val pm = App.appComponent.preferencesManager()

    override fun onBottomMenuItemClick(itemId: Int): AbsScreen<*>? {
        return when (itemId) {
            R.id.nav_home -> HomeScreen(SearchPhotoCardsSpecification()).activateOnlyFade()
            R.id.nav_profile ->
                if (pm.getToken().isNotEmpty())
                    ProfileScreen(pm.getCurrentUserId()).activateOnlyFade()
                else NotAuthorizedScreen().activateOnlyFade()
            R.id.nav_upload -> {
                if (pm.getToken().isNotEmpty()) {
                    ChoicePhotoScreen().activateOnlyFade()
                } else {
                    view?.showSnackBar(R.string.message_need_authorize)
                    null
                }
            }
            else -> null
        }
    }

    override var view: RootView? = null
        private set

    override fun takeView(rootView: RootView) {
        view = rootView
    }

    override fun dropView() {
        view = null
    }

    override fun startCameraForResult(file: File, requestCode: Int) {
        view?.startCameraForResult(file, requestCode)
    }

    override fun startGalleryForResult(requestCode: Int) {
        view?.startGalleryForResult(requestCode)
    }

    override fun startSettingForResult(requestCode: Int) {
        view?.startSettingForResult(requestCode)
    }

    override fun checkAndRequestPermission(permissions: Array<String>, requestCode: Int): Boolean {
        var isAllGranted: Boolean = false
        if (view != null) {
            isAllGranted = view!!.isAllPermissionsGranted(permissions)
            if (!isAllGranted) {
                view!!.requestPermissions(permissions, requestCode)
            }
        }
        return isAllGranted
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        activityResultSubject.onNext(ActivityResultDto(requestCode, resultCode, data))
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>,
                                            grantResults: IntArray) {
        permissionsResultSubject.onNext(PermissionsResultDto(requestCode, permissions, grantResults))
    }

    override fun newActionBarBuilder(): ActionBarBuilder? {
        if (view != null) return ActionBarBuilder(view)
        else return null
    }
}
