package ru.sevoro.photon.screen.search_photo_cards

import android.databinding.Bindable
import ru.sevoro.photon.BR
import ru.sevoro.photon.screen.base.BaseViewModel

/**
 * @author Sergey Vorobyev
 */
class SearchPhotoCardsVM : BaseViewModel() {

    @get:Bindable
    var search: String = ""
        set(value) {
            field = value
            notifyPropertyChanged(BR.search)
        }

    fun clearSearch() {
        search = ""
    }
}