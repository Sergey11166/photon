package ru.sevoro.photon.screen.new_photo_card

import ru.sevoro.photon.screen.base.BasePresenter

/**
 * @author Sergey Vorobyev
 */
interface NewPhotoCardPresenter : BasePresenter<NewPhotoCardViewImpl> {

    fun onTagNameChanged(tagName: String)

    fun onTagSelected(tag: String)

    fun onAddAlbumButtonClick()

    fun onAlbumClick(position: Int)

    fun onSaveButtonClick()

    fun onOkButtonClickNewAlbumDialog()
}