package ru.sevoro.photon.screen.choice_photo

import android.Manifest.permission.WRITE_EXTERNAL_STORAGE
import android.app.Activity.RESULT_OK
import android.content.pm.PackageManager.PERMISSION_GRANTED
import android.net.Uri
import flow.Flow
import mortar.MortarScope
import ru.sevoro.photon.R
import ru.sevoro.photon.data.dto.ActivityResultDto
import ru.sevoro.photon.data.dto.PermissionsResultDto
import ru.sevoro.photon.screen.base.AbsPresenter
import ru.sevoro.photon.screen.new_photo_card.NewPhotoCardScreen
import ru.sevoro.photon.util.SERVICE_NAME
import ru.sevoro.photon.util.filePathFromUri
import rx.subjects.PublishSubject
import java.io.File

/**
 * @author Sergey Vorobyev
 */

// onActivityResult request codes
const val REQUEST_CODE_GALLERY = 0
const val REQUEST_CODE_SETTINGS = 1

// Permissions requests
const val GALLERY_PERMISSION_REQUEST_CODE = 1

class ChoicePhotoPresenterImpl(
        private val activityResultSubject: PublishSubject<ActivityResultDto>,
        private val permissionsResultSubject: PublishSubject<PermissionsResultDto>
) : AbsPresenter<ChoicePhotoViewImpl>(), ChoicePhotoPresenter {

    override fun onEnterScope(scope: MortarScope) {
        super.onEnterScope(scope)
        subscribeOnActivityResult()
        subscribeOnRequestPermissionsResult()
    }

    override fun onChoiceButtonClick() {
        checkAndRequestGalleryPermissions()
    }

    private fun subscribeOnActivityResult() {
        subscriptions.add(activityResultSubject
                .filter { (requestCode, resultCode, intent) ->
                    requestCode == REQUEST_CODE_GALLERY &&
                            resultCode == RESULT_OK &&
                            intent != null &&
                            intent.data != null
                }
                .subscribe {
                    val selectedPhoto = Uri.fromFile(File(view?.context?.filePathFromUri(it?.intent?.data!!)))
                    if (view != null) {
                        Flow.get(view).set(NewPhotoCardScreen(selectedPhoto.path))
                    }
                })
    }

    private fun subscribeOnRequestPermissionsResult() {
        subscriptions.add(permissionsResultSubject
                .filter { (requestCode) -> requestCode == GALLERY_PERMISSION_REQUEST_CODE }
                .subscribe { handleGalleryRequestPermissionsResult(it) })
    }

    private fun handleGalleryRequestPermissionsResult(result: PermissionsResultDto) {
        if (result.grantResults.isNotEmpty() && result.grantResults[0] == PERMISSION_GRANTED) {
            handleGalleryPermissionsGranted()
        } else {
            if (view != null) {
                rootView?.showSnackBar(
                        R.string.message_allow_write_file,
                        R.string.snack_bar_action_settings,
                        { rootPresenter.startSettingForResult(REQUEST_CODE_SETTINGS) })
            }
        }
    }

    private fun checkAndRequestGalleryPermissions() {
        val permissions = arrayOf(WRITE_EXTERNAL_STORAGE)
        if (rootPresenter.checkAndRequestPermission(permissions, GALLERY_PERMISSION_REQUEST_CODE)) {
            handleGalleryPermissionsGranted()
        }
    }

    private fun handleGalleryPermissionsGranted() {
        rootPresenter.startGalleryForResult(REQUEST_CODE_GALLERY)
    }

    override fun initActionBar() {
        rootPresenter.newActionBarBuilder()?.setVisibility(false)?.build()
    }

    override fun initDagger(scope: MortarScope) {
        scope.getService<ChoicePhotoScreen.Component>(SERVICE_NAME).inject(this)
    }
}