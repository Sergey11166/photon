package ru.sevoro.photon.screen.user

import android.databinding.Bindable
import ru.sevoro.photon.BR
import ru.sevoro.photon.screen.base.BaseViewModel

/**
 * @author Sergey Vorobyev
 */
class UserVM : BaseViewModel() {

    @get:Bindable
    var avatar: String = ""
        set(value) {
            field = value
            notifyPropertyChanged(BR.avatar)
        }

    @get:Bindable
    var login: String = ""
        set(value) {
            field = value
            notifyPropertyChanged(BR.login)
        }

    @get:Bindable
    var username: String = ""
        set(value) {
            field = "/ $value"
            notifyPropertyChanged(BR.username)
        }

    @get:Bindable
    var albumCount: String = ""
        set(value) {
            field = value
            notifyPropertyChanged(BR.albumCount)
        }

    @get:Bindable
    var photoCardCount = ""
        set(value) {
            field = value
            notifyPropertyChanged(BR.photoCardCount)
        }
}