package ru.sevoro.photon.screen.album

/**
 * @author Sergey Vorobyev
 */
interface AlbumModel {

    fun updateAlbumInfo(albumId: String, title: String, description: String)

    fun deleteAlbum(albumId: String)
}