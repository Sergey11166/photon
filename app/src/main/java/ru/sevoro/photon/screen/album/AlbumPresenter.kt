package ru.sevoro.photon.screen.album

import ru.sevoro.photon.data.dto.realm.PhotoCardRealm
import ru.sevoro.photon.screen.base.BasePresenter

/**
 * @author Sergey Vorobyev
 */
interface AlbumPresenter : BasePresenter<AlbumViewImpl> {

    fun onPhotoCardClick(photoCard: PhotoCardRealm)

    fun onOkButtonClickEditAlbumDialog()
}