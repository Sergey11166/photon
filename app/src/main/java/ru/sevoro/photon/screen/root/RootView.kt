package ru.sevoro.photon.screen.root

import android.support.annotation.DrawableRes
import android.support.annotation.IdRes
import android.support.annotation.StringRes
import ru.sevoro.photon.screen.base.BaseView
import java.io.File

/**
 * @author Sergey Vorobyev
 */
interface RootView {

    val currentScreen: BaseView?

    fun showSnackBar(message: String)

    fun showSnackBar(message: String, actionText: String, clickListener: () -> Unit)

    fun showSnackBar(@StringRes messageId: Int)

    fun showSnackBar(@StringRes messageId: Int, @StringRes actionTextId: Int, clickListener: () -> Unit)

    fun showProgress()

    fun hideProgress()

    fun isAllPermissionsGranted(permissions: Array<String>): Boolean

    fun setCheckedNavigationItem(@IdRes itemId: Int)

    fun requestPermissions(permissions: Array<String>, requestCode: Int)

    fun startCameraForResult(file: File, requestCode: Int)

    fun startGalleryForResult(requestCode: Int)

    fun startSettingForResult(requestCode: Int)

    fun setBackground(@DrawableRes resId: Int)

    fun showBottomNavigation()
}
