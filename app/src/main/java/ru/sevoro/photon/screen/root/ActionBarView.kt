package ru.sevoro.photon.screen.root

import android.support.annotation.DrawableRes
import android.support.annotation.StringRes
import android.support.v4.view.ViewPager

/**
 * @author Sergey Vorobyev
 */
interface ActionBarView {

    fun setActionBarTitle(title: CharSequence)

    fun setActionBarTitle(@StringRes resId: Int)

    fun setActionBarVisibility(isVisible: Boolean)

    fun setBackArrow(enable: Boolean)

    fun setMenuItems(items: List<MenuItemHolder>)

    fun setOverflowIcon(@DrawableRes icon: Int)

    fun setShowIconsInPopupMenu(isShow: Boolean)

    fun setTabLayout(viewPager: ViewPager)

    fun removeTabLayout()
}
