package ru.sevoro.photon.screen.user

import mortar.MortarScope
import ru.sevoro.photon.R
import ru.sevoro.photon.screen.root.ActionBarBuilder
import ru.sevoro.photon.util.SERVICE_NAME

/**
 * @author Sergey Vorobyev
 */
class AuthorPresenterImpl(userId: String, model: UserModel, viewModel: UserVM) :
        UserPresenterImpl<AuthorViewImpl>(userId, false, model, viewModel), AuthorPresenter {

    override fun initActionBar() {
        val actionBarBuilder: ActionBarBuilder? = rootPresenter.newActionBarBuilder()
        @Suppress("IfThenToSafeAccess")
        if (actionBarBuilder != null) {
            actionBarBuilder
                    .setBackArrow(true)
                    .setTitle(R.string.author_title)
                    .build()
        }
    }

    override fun initDagger(scope: MortarScope) {
        scope.getService<AuthorScreen.Component>(SERVICE_NAME).inject(this)
    }
}