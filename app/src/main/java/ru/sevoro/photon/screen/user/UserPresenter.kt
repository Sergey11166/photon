package ru.sevoro.photon.screen.user

import ru.sevoro.photon.data.dto.realm.AlbumRealm
import ru.sevoro.photon.screen.base.BasePresenter

@Suppress("FINITE_BOUNDS_VIOLATION_IN_JAVA")
/**
 * @author Sergey Vorobyev
 */
interface UserPresenter<in V : UserViewImpl<*>> : BasePresenter<V> {

    fun onAlbumClick(album: AlbumRealm)
}