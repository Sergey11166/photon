package ru.sevoro.photon.screen.album

import android.os.Bundle
import android.view.MenuItem
import flow.Direction
import flow.Flow
import mortar.MortarScope
import ru.sevoro.photon.R
import ru.sevoro.photon.data.dto.realm.AlbumRealm
import ru.sevoro.photon.data.dto.realm.PhotoCardRealm
import ru.sevoro.photon.dialog.EditAlbumVM
import ru.sevoro.photon.screen.base.AbsPresenter
import ru.sevoro.photon.screen.choice_photo.ChoicePhotoScreen
import ru.sevoro.photon.screen.photo_card.PhotoCardScreen
import ru.sevoro.photon.screen.root.ActionBarBuilder
import ru.sevoro.photon.screen.root.MenuItemHolder
import ru.sevoro.photon.util.SERVICE_NAME
import ru.sevoro.photon.util.showToast

/**
 * @author Sergey Vorobyev
 */
class AlbumPresenterImpl(
        private val album: AlbumRealm,
        private val isFromProfile: Boolean,
        private val model: AlbumModel,
        private val viewModel: AlbumVM,
        private val editAlbumVM: EditAlbumVM) :
        AbsPresenter<AlbumViewImpl>(), AlbumPresenter {

    override fun onPhotoCardClick(photoCard: PhotoCardRealm) {
        if (view != null) {
            Flow.get(view).set(PhotoCardScreen(photoCard, true))
        }
    }

    override fun onOkButtonClickEditAlbumDialog() {
        if (!editAlbumVM.isNameValid || !editAlbumVM.isDescriptionValid) {
            view?.context?.showToast(R.string.message_tape_valid_data)
        } else {
            model.updateAlbumInfo(album.id, editAlbumVM.name, editAlbumVM.description)
            viewModel.title = editAlbumVM.name
            viewModel.description = editAlbumVM.description
            view?.closeEditAlbumDialog()
        }
    }

    override fun onEnterScope(scope: MortarScope) {
        super.onEnterScope(scope)
        initViewModel()
    }

    override fun onLoad(savedInstanceState: Bundle?) {
        super.onLoad(savedInstanceState)
        view?.bindViewModel(viewModel)
        view?.showPhotoCards(album.photoCards)
    }

    override fun initActionBar() {
        val actionBarBuilder: ActionBarBuilder? = rootPresenter.newActionBarBuilder()
        @Suppress("IfThenToSafeAccess")
        if (actionBarBuilder != null) {
            actionBarBuilder
                    .setBackArrow(true)
                    .setTitle(R.string.album_title)
            if (isFromProfile) {
                actionBarBuilder
                        .setOverflowIcon(R.drawable.ic_custom_menu_black_24dp)
                        .addAction(MenuItemHolder(0, MenuItem.SHOW_AS_ACTION_NEVER, R.string.album_menu_edit_album,
                                {
                                    editAlbumVM.name = viewModel.title
                                    editAlbumVM.description = viewModel.description
                                    view?.showEditAlbumDialog(editAlbumVM)
                                })
                        )
                        .addAction(MenuItemHolder(0, MenuItem.SHOW_AS_ACTION_NEVER, R.string.album_menu_remove_album,
                                {
                                    if (!album.isFavorite) {
                                        model.deleteAlbum(album.id)
                                        if (view != null) {
                                            Flow.get(view).goBack()
                                        }
                                    } else {
                                        rootView?.showSnackBar(R.string.album_message_not_allowed_delete_favorite)
                                    }
                                }))
                        .addAction(MenuItemHolder(0, MenuItem.SHOW_AS_ACTION_NEVER, R.string.album_menu_add_photo, {
                            if (view != null) {
                                Flow.get(view).replaceHistory(ChoicePhotoScreen().activateOnlyFade(), Direction.REPLACE)
                            }
                        }))
            }
            actionBarBuilder.build()
        }
    }

    private fun initViewModel() {
        viewModel.title = album.title
        viewModel.photoCardCount = album.photoCards.size.toString()
        viewModel.description = album.description
    }

    override fun initDagger(scope: MortarScope) {
        scope.getService<AlbumScreen.Component>(SERVICE_NAME).inject(this)
    }
}