package ru.sevoro.photon.screen.album

import io.realm.RealmList
import ru.sevoro.photon.data.dto.realm.PhotoCardRealm
import ru.sevoro.photon.dialog.EditAlbumVM
import ru.sevoro.photon.screen.base.BaseView

/**
 * @author Sergey Vorobyev
 */
interface AlbumView : BaseView {

    fun showPhotoCards(cards: RealmList<PhotoCardRealm>)

    fun showEditAlbumDialog(viewModel: EditAlbumVM)

    fun closeEditAlbumDialog()
}