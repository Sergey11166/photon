package ru.sevoro.photon.screen.base

import android.os.Bundle
import mortar.MortarScope
import mortar.ViewPresenter
import ru.sevoro.photon.screen.root.RootPresenter
import ru.sevoro.photon.screen.root.RootView
import rx.subscriptions.CompositeSubscription
import javax.inject.Inject

/**
 * @author Sergey Vorobyev
 */
abstract class AbsPresenter<V : AbsView<*>> : ViewPresenter<V>() {

    @Inject
    lateinit protected var rootPresenter: RootPresenter

    lateinit protected var subscriptions: CompositeSubscription

    override fun onEnterScope(scope: MortarScope) {
        super.onEnterScope(scope)
        subscriptions = CompositeSubscription()
        initDagger(scope)
    }

    override fun onLoad(savedInstanceState: Bundle?) {
        super.onLoad(savedInstanceState)
        initActionBar()
    }

    override fun onExitScope() {
        subscriptions.unsubscribe()
        super.onExitScope()
    }

    open protected fun initActionBar() {}

    val rootView: RootView?
        get() = rootPresenter.view

    protected abstract fun initDagger(scope: MortarScope)
}
