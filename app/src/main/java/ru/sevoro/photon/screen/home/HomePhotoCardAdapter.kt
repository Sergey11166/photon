package ru.sevoro.photon.screen.home

import android.content.Context
import android.support.v4.content.ContextCompat
import android.support.v7.util.SortedList
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.RecyclerView.Adapter
import android.view.LayoutInflater
import android.view.ViewGroup
import ru.sevoro.photon.R
import ru.sevoro.photon.data.dto.realm.PhotoCardRealm
import ru.sevoro.photon.util.SimpleSortedListCallback
import ru.sevoro.photon.util.loadImage

/**
 * @author Sergey Vorobyev
 */
class HomePhotoCardAdapter(private val itemClickListener: (Int) -> Unit) : Adapter<PhotoCardViewHolder>() {

    private lateinit var context: Context

    val items = SortedList<PhotoCardRealm>(
            PhotoCardRealm::class.java, object : SimpleSortedListCallback<PhotoCardRealm>() {
        override fun compare(o1: PhotoCardRealm, o2: PhotoCardRealm) = o1.views.compareTo(o2.views) * -1
    })

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)
        context = recyclerView.context
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int) =
            PhotoCardViewHolder(LayoutInflater.from(context)
                    .inflate(R.layout.item_photo_card_home, parent, false), itemClickListener)

    override fun onBindViewHolder(viewHolder: PhotoCardViewHolder, position: Int) {
        val photoCard = items[position]
        loadImage(viewHolder.image, photoCard.photo, null,
                ContextCompat.getColor(context, R.color.colorGrayLight), "centerCrop")
        viewHolder.favorites.text = "${photoCard.favorites}"
        viewHolder.views.text = "${photoCard.views}"
    }

    override fun getItemCount() = items.size()

    fun addPhotoCard(item: PhotoCardRealm) {
        val index = items.indexOf(item)
        if (index != -1) {
            items.updateItemAt(index, item)
            notifyItemChanged(index)
        } else {
            items.add(item)
            notifyItemInserted(items.indexOf(item))
        }

    }
}