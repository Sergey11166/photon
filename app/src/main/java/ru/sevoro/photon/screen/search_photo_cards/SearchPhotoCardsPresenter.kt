package ru.sevoro.photon.screen.search_photo_cards

import ru.sevoro.photon.screen.base.BasePresenter

/**
 * @author Sergey Vorobyev
 */
interface SearchPhotoCardsPresenter : BasePresenter<SearchPhotoCardsViewImpl> {

    fun onSearchChanged()

    fun onTagClicked(tag: String)

    fun onSearchCheckedButtonClick()

    fun onFilterSearchButtonClick()
}