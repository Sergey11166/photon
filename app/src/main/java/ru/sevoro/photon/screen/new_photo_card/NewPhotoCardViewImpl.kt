package ru.sevoro.photon.screen.new_photo_card

import android.content.Context
import android.content.Context.WINDOW_SERVICE
import android.databinding.DataBindingUtil
import android.support.v4.content.ContextCompat
import android.text.Editable
import android.util.AttributeSet
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.View
import android.view.WindowManager
import android.widget.GridLayout
import android.widget.TextView
import flow.Flow
import ru.sevoro.photon.R
import ru.sevoro.photon.data.dto.realm.AlbumRealm
import ru.sevoro.photon.databinding.ScreenNewPhotoCardBinding
import ru.sevoro.photon.dialog.EditAlbumVM
import ru.sevoro.photon.dialog.getInstanceNewAlbumDialog
import ru.sevoro.photon.screen.base.AbsView
import ru.sevoro.photon.screen.base.BaseViewModel
import ru.sevoro.photon.screen.base.FiltersVM
import ru.sevoro.photon.util.SimpleTextWatcher
import ru.sevoro.photon.util.getComponent
import ru.sevoro.photon.util.loadImage

/**
 * @author Sergey Vorobyev
 */
class NewPhotoCardViewImpl(context: Context, attrs: AttributeSet?) :
        AbsView<NewPhotoCardPresenter>(context, attrs), NewPhotoCardView {

    private lateinit var binding: ScreenNewPhotoCardBinding

    private val newAlbumDialog = getInstanceNewAlbumDialog(
            context.getString(R.string.new_album_title),
            { view ->
                when (view.id) {
                    R.id.ok_button -> presenter.onOkButtonClickNewAlbumDialog()
                }
            }
    )

    override fun onFinishInflate() {
        super.onFinishInflate()
        if (!isInEditMode) {
            binding = DataBindingUtil.bind(this)
            binding.addAlbumButton.setOnClickListener { presenter.onAddAlbumButtonClick() }
            binding.saveButton.setOnClickListener { presenter.onSaveButtonClick() }
            binding.inputTagCheckedButton.setOnClickListener {
                val text = binding.tagInput.text.toString().toLowerCase()
                if (text.isNotEmpty()) presenter.onTagSelected(text)
            }
            binding.cancelButton.setOnClickListener { Flow.get(this).goBack() }
            binding.tagInput.addTextChangedListener(object : SimpleTextWatcher() {
                override fun afterTextChanged(text: Editable) {
                    presenter.onTagNameChanged(text.toString())
                }
            })
            binding.tagInput.setOnFocusChangeListener { _, hasFocus ->
                if (hasFocus) binding.scrollView.smoothScrollTo(0, binding.tagInputDividerTop.y.toInt())
            }
        }
    }

    override fun showSelectedTags(tags: List<String>) {
        binding.tagsContainer.removeAllViews()
        tags.forEach {
            val tag = "#$it"
            val view = LayoutInflater.from(context).inflate(R.layout.item_tag, binding.tagsContainer, false)
            view.findViewById<TextView>(R.id.tag).text = tag
            binding.tagsContainer.addView(view)
        }
    }

    override fun showFilteredTags(tags: List<String>) {
        binding.filteredTags.removeAllViews()
        if (tags.isEmpty()) {
            binding.filteredTagsDividerBottom.visibility = View.GONE
            return
        }
        binding.filteredTagsDividerBottom.visibility = View.VISIBLE
        tags.forEach { tag ->
            val view = LayoutInflater.from(context).inflate(R.layout.item_promts_list, binding.filteredTags, false)
            view.findViewById<TextView>(R.id.value).text = tag
            binding.filteredTags.addView(view)
            view.setOnClickListener { presenter.onTagSelected(tag.toLowerCase()) }
        }
    }

    override fun bindViewModel(viewModel: BaseViewModel) {
        binding.newPhotoCardModel = viewModel as NewPhotoCardVM
    }

    override fun bindFiltersViewModel(viewModel: FiltersVM) {
        binding.filtersModel = viewModel
    }

    override fun showAlbums(albums: List<AlbumRealm>) {
        binding.albumsContainer.removeAllViews()

        albums.forEach { album ->
            val preview: String = if (album.photoCards.size > 0) album.photoCards[0].photo else ""

            val view = LayoutInflater.from(context).inflate(R.layout.item_album, binding.albumsContainer, false)

            view.findViewById<View>(R.id.ic_favorite).visibility = View.GONE
            view.findViewById<View>(R.id.ic_views).visibility = View.GONE
            view.findViewById<View>(R.id.favorites).visibility = View.GONE
            view.findViewById<View>(R.id.views).visibility = View.GONE

            loadImage(view.findViewById(R.id.photo), preview, null,
                    ContextCompat.getColor(context, R.color.colorGrayLight), "centerCrop")

            view.findViewById<TextView>(R.id.title).text = album.title
            view.findViewById<TextView>(R.id.photo_count).text = album.photoCards.size.toString()

            binding.albumsContainer.addView(view)

            val displayMetrics = DisplayMetrics()
            (context.getSystemService(WINDOW_SERVICE) as WindowManager)
                    .defaultDisplay.getMetrics(displayMetrics)

            val lp = GridLayout.LayoutParams()
            lp.width = (displayMetrics.widthPixels * 0.5).toInt()
            view.layoutParams = lp

            view.setOnClickListener { v: View ->
                for (i in 0..binding.albumsContainer.childCount) {
                    binding.albumsContainer.getChildAt(i)?.alpha = 1f
                }
                v.alpha = 0.7f
                presenter.onAlbumClick(albums.indexOf(album))
            }
        }
    }

    override fun selectAlbum(position: Int) {
        binding.albumsContainer.getChildAt(position).alpha = 0.7f
    }

    override fun showNewAlbumDialog(viewModel: EditAlbumVM) {
        newAlbumDialog.setViewModel(viewModel)
        showFragmentDialog(newAlbumDialog)
    }

    override fun closeNewAlbumDialog() {
        newAlbumDialog.dismiss()
    }

    override fun initDagger(context: Context) {
        context.getComponent<NewPhotoCardScreen.Component>().inject(this)
    }
}