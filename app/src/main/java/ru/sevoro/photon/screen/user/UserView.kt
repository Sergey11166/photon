package ru.sevoro.photon.screen.user

import io.realm.RealmList
import ru.sevoro.photon.data.dto.realm.AlbumRealm
import ru.sevoro.photon.screen.base.BaseView

/**
 * @author Sergey Vorobyev
 */
interface UserView : BaseView {

    fun showAlbums(albums: RealmList<AlbumRealm>)
}