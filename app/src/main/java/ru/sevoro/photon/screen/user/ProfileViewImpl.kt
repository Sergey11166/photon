package ru.sevoro.photon.screen.user

import android.content.Context
import android.databinding.DataBindingUtil
import android.util.AttributeSet
import android.widget.GridLayout
import ru.sevoro.photon.R
import ru.sevoro.photon.databinding.ScreenProfileBinding
import ru.sevoro.photon.dialog.*
import ru.sevoro.photon.screen.base.BaseViewModel
import ru.sevoro.photon.util.getComponent

/**
 * @author Sergey Vorobyev
 */
class ProfileViewImpl(context: Context, attrs: AttributeSet?) :
        UserViewImpl<ProfilePresenter>(context, attrs), ProfileView {

    private lateinit var binding: ScreenProfileBinding

    private val changeImageDialog = getInstanceChangeImageDialog { item ->
        when (item) {
            0 -> presenter.onChoiceTakePhoto()
            1 -> presenter.onChoiceFromGallery()
        }
    }

    private val newAlbumDialog = getInstanceNewAlbumDialog(
            context.getString(R.string.new_album_title),
            { view ->
                when (view.id) {
                    R.id.ok_button -> presenter.onOkButtonClickNewAlbumDialog()
                }
            }
    )

    private val editProfileDialog = getInstanceEditProfileDialog { view ->
        when (view.id) {
            R.id.ok_button -> presenter.onOkButtonClickEditProfileDialog()
        }
    }

    override fun onFinishInflate() {
        super.onFinishInflate()
        if (!isInEditMode) binding = DataBindingUtil.bind(this)
    }

    override fun showChangeAvatarDialog() {
        showFragmentDialog(changeImageDialog)
    }

    override fun showEditProfileDialog(viewModel: EditProfileVM) {
        editProfileDialog.setViewModel(viewModel)
        showFragmentDialog(editProfileDialog)
    }

    override fun closeEditProfileDialog() {
        editProfileDialog.dismiss()
    }

    override fun showNewAlbumDialog(viewModel: EditAlbumVM) {
        newAlbumDialog.setViewModel(viewModel)
        showFragmentDialog(newAlbumDialog)
    }

    override fun closeNewAlbumDialog() {
        newAlbumDialog.dismiss()
    }

    override fun getAlbumsContainer(): GridLayout {
        return binding.layout!!.albumsContainer
    }

    override fun bindViewModel(viewModel: BaseViewModel) {
        binding.userModel = viewModel as UserVM
    }

    override fun initDagger(context: Context) {
        context.getComponent<ProfileScreen.Component>().inject(this)
    }
}