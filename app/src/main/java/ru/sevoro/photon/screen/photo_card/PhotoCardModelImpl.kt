package ru.sevoro.photon.screen.photo_card

import android.graphics.Bitmap
import android.net.Uri
import ru.sevoro.photon.data.dto.realm.PhotoCardRealm
import ru.sevoro.photon.data.dto.realm.UserRealm
import ru.sevoro.photon.data.dto.response.IsSuccessResponse
import ru.sevoro.photon.data.transformers.RestCallTransformer
import ru.sevoro.photon.screen.base.AbsModel
import rx.Observable
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers

class PhotoCardModelImpl : AbsModel(), PhotoCardModel {

    override fun addPhotoCardView(photoCard: PhotoCardRealm) {
        jobQueueManager.addJobInBackground(AddPhotoCardViewJobJob(photoCard.id))
    }

    override fun isFavorite(photoCardId: String) =
            realmManager.isFavorite(photoCardId, prefsManager.getCurrentUserId()) ?: false

    override fun addToFavorites(photoCardId: String): Observable<Boolean> {
        val currentUserId = prefsManager.getCurrentUserId()

        @Suppress("UNCHECKED_CAST")
        return restService.addToFavorites(prefsManager.getToken(), currentUserId, photoCardId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .compose(restCallTransformer as RestCallTransformer<IsSuccessResponse>)
                .map { it.isSuccess }
                .doOnNext { if (it) realmManager.addToFavorite(photoCardId, currentUserId) }
    }

    override fun getUser(userId: String): Observable<UserRealm?> {
        return updateUser(userId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map { realmManager.getEntityById(UserRealm::class.java, userId) }
    }

    override fun isAuthorized() = prefsManager.getToken().isNotEmpty()

    override fun saveBitmapToCache(bitmap: Bitmap, photoCardId: String): Observable<Uri> {
        return fileManager.saveBitmapToCache(bitmap, photoCardId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }

    override fun saveBitmapGallery(bitmap: Bitmap, photoCardId: String): Observable<Uri> {
        return fileManager.saveBitmapToGallery(bitmap, photoCardId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }
}