package ru.sevoro.photon.screen.search_photo_cards

import rx.Observable

/**
 * @author Sergey Vorobyev
 */
interface SearchPhotoCardsModel {

    fun saveQuery(search: String)

    fun getPopularTags(): Observable<List<String>>

    fun getAvailablePromts(): List<String>
}