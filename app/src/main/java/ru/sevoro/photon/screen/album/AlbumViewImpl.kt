package ru.sevoro.photon.screen.album

import android.content.Context
import android.databinding.DataBindingUtil
import android.support.v4.content.ContextCompat
import android.util.AttributeSet
import android.util.DisplayMetrics
import android.view.ViewGroup.LayoutParams.WRAP_CONTENT
import android.view.WindowManager
import android.widget.GridLayout
import android.widget.ImageView
import io.realm.RealmList
import ru.sevoro.photon.R
import ru.sevoro.photon.custom_view.AspectRatioImageView
import ru.sevoro.photon.custom_view.AspectRatioImageView.ASPECT_RATIO_1_1
import ru.sevoro.photon.data.dto.realm.PhotoCardRealm
import ru.sevoro.photon.databinding.ScreenAlbumBinding
import ru.sevoro.photon.dialog.EditAlbumVM
import ru.sevoro.photon.dialog.getInstanceNewAlbumDialog
import ru.sevoro.photon.screen.base.AbsView
import ru.sevoro.photon.screen.base.BaseViewModel
import ru.sevoro.photon.util.getComponent
import ru.sevoro.photon.util.loadImage

/**
 * @author Sergey Vorobyev
 */
class AlbumViewImpl(context: Context, attrs: AttributeSet?) :
        AbsView<AlbumPresenter>(context, attrs), AlbumView {

    private lateinit var binding: ScreenAlbumBinding

    private val editAlbumDialog = getInstanceNewAlbumDialog(
            context.getString(R.string.edit_album_title),
            { view ->
                when (view.id) {
                    R.id.ok_button -> presenter.onOkButtonClickEditAlbumDialog()
                }
            }
    )

    override fun showPhotoCards(cards: RealmList<PhotoCardRealm>) {
        for (card in cards) {
            val photoImageView = AspectRatioImageView(context, ASPECT_RATIO_1_1)
            photoImageView.scaleType = ImageView.ScaleType.CENTER_CROP
            loadImage(photoImageView, card.photo, null,
                    ContextCompat.getColor(context, R.color.colorGrayLight), "centerCrop")
            binding.cardsContainer.addView(photoImageView)

            val displayMetrics = DisplayMetrics()
            (context.getSystemService(Context.WINDOW_SERVICE) as WindowManager)
                    .defaultDisplay.getMetrics(displayMetrics)

            val lp = GridLayout.LayoutParams()
            lp.width = displayMetrics.widthPixels / 3
            lp.height = WRAP_CONTENT
            photoImageView.layoutParams = lp

            photoImageView.setOnClickListener { presenter.onPhotoCardClick(card) }
        }
    }

    override fun onFinishInflate() {
        super.onFinishInflate()
        if (!isInEditMode) binding = DataBindingUtil.bind(this)
    }

    override fun bindViewModel(viewModel: BaseViewModel) {
        binding.albumModel = viewModel as AlbumVM
    }

    override fun showEditAlbumDialog(viewModel: EditAlbumVM) {
        editAlbumDialog.setViewModel(viewModel)
        showFragmentDialog(editAlbumDialog)
    }

    override fun closeEditAlbumDialog() {
        editAlbumDialog.dismiss()
    }

    override fun initDagger(context: Context) {
        context.getComponent<AlbumScreen.Component>().inject(this)
    }
}