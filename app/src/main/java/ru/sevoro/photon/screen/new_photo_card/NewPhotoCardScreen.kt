package ru.sevoro.photon.screen.new_photo_card

import dagger.Provides
import ru.sevoro.photon.R
import ru.sevoro.photon.dagger.DaggerScope
import ru.sevoro.photon.dialog.EditAlbumVM
import ru.sevoro.photon.flow_mortar.Screen
import ru.sevoro.photon.screen.base.AbsScreen
import ru.sevoro.photon.screen.base.FiltersVM
import ru.sevoro.photon.screen.root.RootActivity

/**
 * @author Sergey Vorobyev
 */
@Screen(R.layout.screen_new_photo_card)
class NewPhotoCardScreen(private val photoLocalPath: String) : AbsScreen<RootActivity.RootComponent>() {

    override fun createScreenComponent(parentComponent: RootActivity.RootComponent?): Any {
        return DaggerNewPhotoCardScreen_Component.builder()
                .rootComponent(parentComponent)
                .module(Module(photoLocalPath))
                .build()
    }

    @DaggerScope(NewPhotoCardScreen::class)
    @dagger.Component(dependencies = arrayOf(RootActivity.RootComponent::class),
            modules = arrayOf(Module::class))
    interface Component {
        fun inject(view: NewPhotoCardViewImpl)

        fun inject(presenter: NewPhotoCardPresenterImpl)
    }

    @dagger.Module
    class Module(private val photoLocalPath: String) {

        @Provides
        @DaggerScope(NewPhotoCardScreen::class)
        fun provideModel(): NewPhotoCardModel {
            return NewPhotoCardModelImpl()
        }

        @Provides
        @DaggerScope(NewPhotoCardScreen::class)
        fun provideViewModel(): NewPhotoCardVM {
            return NewPhotoCardVM()
        }

        @Provides
        @DaggerScope(NewPhotoCardScreen::class)
        fun provideFilterVM(): FiltersVM {
            return FiltersVM()
        }

        @Provides
        @DaggerScope(NewPhotoCardScreen::class)
        fun provideNewAlbumViewModel(): EditAlbumVM {
            return EditAlbumVM()
        }

        @Provides
        @DaggerScope(NewPhotoCardScreen::class)
        fun providePresenter(model: NewPhotoCardModel,
                             viewModel: NewPhotoCardVM,
                             filtersVM: FiltersVM,
                             newAlbumVM: EditAlbumVM
        ): NewPhotoCardPresenter {
            return NewPhotoCardPresenterImpl(photoLocalPath, model, viewModel, filtersVM, newAlbumVM)
        }
    }
}