package ru.sevoro.photon.screen.photo_card

import android.Manifest.permission.WRITE_EXTERNAL_STORAGE
import android.content.pm.PackageManager.PERMISSION_GRANTED
import android.net.Uri
import android.os.Bundle
import android.view.MenuItem
import flow.Flow
import mortar.MortarScope
import ru.sevoro.photon.R
import ru.sevoro.photon.data.dto.PermissionsResultDto
import ru.sevoro.photon.data.dto.realm.PhotoCardRealm
import ru.sevoro.photon.data.dto.realm.UserRealm
import ru.sevoro.photon.screen.base.AbsPresenter
import ru.sevoro.photon.screen.root.ActionBarBuilder
import ru.sevoro.photon.screen.root.MenuItemHolder
import ru.sevoro.photon.screen.user.AuthorScreen
import ru.sevoro.photon.util.L
import ru.sevoro.photon.util.SERVICE_NAME
import ru.sevoro.photon.util.shareImage
import rx.Subscriber
import rx.subjects.PublishSubject


/**
 * @author Sergey Vorobyev
 */

const val REQUEST_PERMISSIONS_SAVE = 0
const val REQUEST_CODE_SETTING_SAVE = 1

class PhotoCardPresenterImpl(
        private val photoCard: PhotoCardRealm,
        private val permissionsResultSubject: PublishSubject<PermissionsResultDto>,
        private val model: PhotoCardModel,
        private val viewModel: PhotoCardVM
) : AbsPresenter<PhotoCardViewImpl>(), PhotoCardPresenter {

    private var isUserLoaded = false

    override fun onAvatarClick() {
        if (!isUserLoaded) {
            Flow.get(view).set(AuthorScreen(photoCard.owner))
        } else {
            rootView?.showProgress()
            model.getUser(photoCard.owner).subscribe(object : Subscriber<UserRealm>() {
                override fun onCompleted() {}
                override fun onNext(user: UserRealm) {
                    rootView?.hideProgress()
                    L.d("onAvatarClick getUser onNext: $user")
                    Flow.get(view).set(AuthorScreen(photoCard.owner))
                }

                override fun onError(e: Throwable) {
                    e.printStackTrace()
                    rootView?.hideProgress()
                    rootView?.showSnackBar(e.message!!)
                    e.printStackTrace()
                }
            })
        }
    }

    override fun onEnterScope(scope: MortarScope) {
        super.onEnterScope(scope)
        initViewModel()
        model.addPhotoCardView(photoCard)
        model.getUser(photoCard.owner).subscribe(object : Subscriber<UserRealm?>() {
            override fun onCompleted() {}
            override fun onNext(user: UserRealm?) {
                L.d("onEnterScope getUser onNext: $user")
                if (user != null) {
                    viewModel.username = user.username
                    viewModel.avatar = user.avatar
                    viewModel.albumCount = user.albums.filter { !it.isDeleted }.size.toString()

                    var photoCardCount = 0
                    user.albums.filter { !it.isDeleted }.forEach { album -> photoCardCount += album.photoCards.size }
                    viewModel.photoCardCount = photoCardCount.toString()
                }
            }

            override fun onError(e: Throwable) {
                e.printStackTrace()
            }
        })
        subscriptions.add(permissionsResultSubject
                .filter({ (requestCode) -> requestCode == REQUEST_PERMISSIONS_SAVE })
                .subscribe {
                    if (it.grantResults.isNotEmpty() && it.grantResults[0] == PERMISSION_GRANTED) {
                        savePhotoToGallery()
                    } else {
                        rootView?.showSnackBar(
                                R.string.message_allow_write_file,
                                R.string.snack_bar_action_settings,
                                { rootPresenter.startSettingForResult(REQUEST_CODE_SETTING_SAVE) })
                    }
                })
    }

    override fun onLoad(savedInstanceState: Bundle?) {
        super.onLoad(savedInstanceState)
        view?.bindViewModel(viewModel)
        view?.showTags(photoCard.tags)
    }

    private fun initViewModel() {
        val user = UserRealm()
        viewModel.title = photoCard.title
        viewModel.photo = photoCard.photo
        viewModel.username = user.username
        viewModel.avatar = user.avatar
        viewModel.albumCount = user.albumCount.toString()
        viewModel.photoCardCount = user.photoCardCount.toString()
    }

    override fun initActionBar() {
        val actionBarBuilder: ActionBarBuilder? = rootPresenter.newActionBarBuilder()
        @Suppress("IfThenToSafeAccess")
        if (actionBarBuilder != null) {
            actionBarBuilder
                    .setBackArrow(true)
                    .setTitle(R.string.photocard_title)
                    .setOverflowIcon(R.drawable.ic_custom_menu_black_24dp)

            actionBarBuilder.addAction(MenuItemHolder(0, MenuItem.SHOW_AS_ACTION_NEVER, R.string.photocard_menu_favorite, {
                when {
                    model.isAuthorized() && !model.isFavorite(photoCard.id) -> {
                        model.addToFavorites(photoCard.id).subscribe(object : Subscriber<Boolean>() {
                            override fun onCompleted() {}
                            override fun onError(e: Throwable) {
                                e.printStackTrace()
                                rootView?.showSnackBar(e.message!!)
                            }

                            override fun onNext(t: Boolean?) {
                                rootView?.showSnackBar(R.string.photocard_message_add_favorite_success)
                            }
                        })
                    }

                    !model.isAuthorized() -> rootView?.showSnackBar(R.string.message_need_authorize)

                    model.isAuthorized() && model.isFavorite(photoCard.id) ->
                        rootView?.showSnackBar(R.string.photocard_message_already_is_favorite)
                }
            }))

            actionBarBuilder
                    .addAction(MenuItemHolder(0, MenuItem.SHOW_AS_ACTION_NEVER, R.string.photocard_menu_share, {
                        val bitmap = view?.getPhotoBitmap()
                        if (bitmap != null) {
                            rootView?.showProgress()
                            model.saveBitmapToCache(bitmap, photoCard.id)
                                    .subscribe(object : Subscriber<Uri>() {
                                        override fun onCompleted() {}
                                        override fun onNext(imageUri: Uri) {
                                            L.d("saveBitmapToCache onNext: $imageUri")
                                            rootView?.hideProgress()
                                            view?.context?.shareImage(imageUri)
                                        }

                                        override fun onError(e: Throwable) {
                                            rootView?.hideProgress()
                                            e.printStackTrace()
                                        }
                                    })
                        }
                    }))
                    .addAction(MenuItemHolder(0, MenuItem.SHOW_AS_ACTION_NEVER, R.string.photocard_menu_save_to_gallery, {
                        checkAndRequestSaveToGalleryPermissions()
                    }))
                    .build()
        }
    }

    private fun checkAndRequestSaveToGalleryPermissions() {
        if (rootPresenter.checkAndRequestPermission(arrayOf(WRITE_EXTERNAL_STORAGE), REQUEST_PERMISSIONS_SAVE)) {
            savePhotoToGallery()
        }
    }

    private fun savePhotoToGallery() {
        val bitmap = view?.getPhotoBitmap()
        if (bitmap != null) {
            rootView?.showProgress()
            model.saveBitmapGallery(bitmap, photoCard.id)
                    .subscribe(object : Subscriber<Uri>() {
                        override fun onCompleted() {}
                        override fun onNext(imageUri: Uri) {
                            L.d("saveBitmapGallery onNext: $imageUri")
                            rootView?.hideProgress()
                            rootView?.showSnackBar(R.string.photocard_message_photo_saved)
                        }

                        override fun onError(e: Throwable) {
                            rootView?.hideProgress()
                            e.printStackTrace()
                        }
                    })
        }
    }

    override fun initDagger(scope: MortarScope) {
        scope.getService<PhotoCardScreen.Component>(SERVICE_NAME).inject(this)
    }
}