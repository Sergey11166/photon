package ru.sevoro.photon.screen.new_photo_card

import android.os.Bundle
import flow.Flow
import mortar.MortarScope
import ru.sevoro.photon.R
import ru.sevoro.photon.data.dto.*
import ru.sevoro.photon.data.dto.realm.AlbumRealm
import ru.sevoro.photon.dialog.EditAlbumVM
import ru.sevoro.photon.screen.base.AbsPresenter
import ru.sevoro.photon.screen.base.FiltersVM
import ru.sevoro.photon.util.L
import ru.sevoro.photon.util.SERVICE_NAME
import ru.sevoro.photon.util.showToast
import rx.Subscriber

/**
 * @author Sergey Vorobyev
 */
class NewPhotoCardPresenterImpl(
        private val photoLocalPath: String,
        private val model: NewPhotoCardModel,
        private val viewModel: NewPhotoCardVM,
        private val filtersVM: FiltersVM,
        private val newAlbumVM: EditAlbumVM
) : AbsPresenter<NewPhotoCardViewImpl>(), NewPhotoCardPresenter {

    private val albums = ArrayList<AlbumRealm>()
    private val selectedTags = ArrayList<String>()
    private var tagSource = ArrayList<String>()
    private var selectedAlbumPosition = -1

    override fun onAddAlbumButtonClick() {
        view?.showNewAlbumDialog(newAlbumVM)
    }

    override fun onTagNameChanged(tagName: String) {
        view?.showFilteredTags(tagSource
                .filter { tag ->
                    tag.split(" ")
                            .filter { word ->
                                word.isNotEmpty() && word.startsWith(tagName)
                            }
                            .isNotEmpty()
                }
                .take(3))
    }

    override fun onTagSelected(tag: String) {
        if (!tagSource.contains(tag)) {
            tagSource.add(tag)
        }

        if (!selectedTags.contains(tag)) {
            selectedTags.add(tag)
            view?.showSelectedTags(selectedTags)
        }
    }

    override fun onAlbumClick(position: Int) {
        selectedAlbumPosition = position
    }

    override fun onSaveButtonClick() {
        if (viewModel.photoName.isEmpty()) {
            rootView?.showSnackBar(R.string.add_photo_info_message_specif_photo_name)
            return
        }

        if (selectedAlbumPosition == -1) {
            rootView?.showSnackBar(R.string.add_photo_info_message_select_album)
            return
        }

        rootView?.showProgress()

        val tints = ArrayList<String>()
        if (filtersVM.tintRed) tints.add(RED)
        if (filtersVM.tintOrange) tints.add(ORANGE)
        if (filtersVM.tintYellow) tints.add(YELLOW)
        if (filtersVM.tintGreen) tints.add(GREEN)
        if (filtersVM.tintBlue) tints.add(LIGHT_BLUE)
        if (filtersVM.tintDarkBlue) tints.add(BLUE)
        if (filtersVM.tintViolet) tints.add(VIOLET)
        if (filtersVM.tintBrown) tints.add(BROWN)
        if (filtersVM.tintBlack) tints.add(BLACK)
        if (filtersVM.tintWhite) tints.add(WHITE)

        subscriptions.add(model.uploadNewPhotoCard(
                photoName = viewModel.photoName,
                albumId = albums[selectedAlbumPosition].id,
                photoLocalPath = photoLocalPath,
                tags = selectedTags,
                dish = when {
                    filtersVM.dishMeat -> MEAT
                    filtersVM.dishFish -> FISH
                    filtersVM.dishVegetables -> VEGETABLES
                    filtersVM.dishFruits -> FRUIT
                    filtersVM.dishCheese -> CHEESE
                    filtersVM.dishDesserts -> DESSERTS
                    filtersVM.dishDrinks -> DRINKS
                    else -> ""
                },
                tints = tints,
                decor = when {
                    filtersVM.decorRegular -> SIMPLE
                    filtersVM.decorHoliday -> HOLIDAY
                    else -> ""
                },
                temperature = when {
                    filtersVM.tempHot -> HOT
                    filtersVM.tempComfort -> MIDDLE
                    filtersVM.tempCold -> COLD
                    else -> ""
                },
                light = when {
                    filtersVM.lightNatural -> NATURAL
                    filtersVM.lightSynthetic -> SYNTHETIC
                    filtersVM.lightMixed -> MIXED
                    else -> ""
                },
                lightDir = when {
                    filtersVM.lightDirFront -> DIRECT
                    filtersVM.lightDirBack -> BACK_LIGHT
                    filtersVM.lightDirSide -> SIDE_LIGHT
                    filtersVM.lightDirMixed -> MIXED
                    else -> ""
                },
                lightCount = when {
                    filtersVM.lightCount1 -> ONE
                    filtersVM.lightCount2 -> TWO
                    filtersVM.lightCount3 -> THREE
                    else -> ""
                }
        ).subscribe(object : Subscriber<String>() {
            override fun onCompleted() {}
            override fun onNext(photoCardId: String) {
                L.d("photoCard created with id: $photoCardId")
                rootView?.hideProgress()
                rootView?.showSnackBar(R.string.add_photo_info_message_photo_card_created)
                if (view != null) Flow.get(view).goBack()
            }

            override fun onError(e: Throwable) {
                e.printStackTrace()
                rootView?.hideProgress()
                rootView?.showSnackBar(e.message!!)
            }
        }))
    }

    override fun onOkButtonClickNewAlbumDialog() {
        if (!newAlbumVM.isNameValid || !newAlbumVM.isDescriptionValid) {
            view?.context?.showToast(R.string.message_tape_valid_data)
        } else {
            view?.closeNewAlbumDialog()
            rootView?.showProgress()
            model.createNewAlbum(newAlbumVM.name, newAlbumVM.description)
                    .subscribe(object : Subscriber<AlbumRealm>() {
                        override fun onCompleted() {}
                        override fun onNext(album: AlbumRealm) {
                            albums.add(album)
                            viewModel.isAlbumsExists = true
                            view?.showAlbums(albums)
                            rootView?.hideProgress()
                        }

                        override fun onError(e: Throwable) {
                            e.printStackTrace()
                            rootView?.hideProgress()
                            rootView?.showSnackBar(R.string.profile_error_add_new_album)
                        }
                    })

        }
    }

    override fun onEnterScope(scope: MortarScope) {
        super.onEnterScope(scope)
        model.getAvailableTags().forEach { tag ->
            val sourceTag = tag.value.toLowerCase()
            tagSource.add(if (sourceTag.startsWith("#")) sourceTag.substring(1) else sourceTag)
        }
        tagSource.sort(); onTagNameChanged("")

        val al = model.getAlbumsOfCurrentUser()
        if (al.isNotEmpty()) {
            viewModel.isAlbumsExists = true
            albums.addAll(al)
        }
    }

    override fun onLoad(savedInstanceState: Bundle?) {
        super.onLoad(savedInstanceState)
        view?.bindViewModel(viewModel)
        view?.bindFiltersViewModel(filtersVM)
        if (selectedTags.isNotEmpty()) view?.showSelectedTags(selectedTags)
        if (albums.isNotEmpty()) view?.showAlbums(albums)
        if (selectedAlbumPosition != -1) view?.selectAlbum(selectedAlbumPosition)
        onTagNameChanged("")
    }

    override fun initActionBar() {
        rootPresenter.newActionBarBuilder()?.setVisibility(true)?.setTitle(R.string.app_name)?.build()
    }

    override fun initDagger(scope: MortarScope) {
        scope.getService<NewPhotoCardScreen.Component>(SERVICE_NAME).inject(this)
    }
}