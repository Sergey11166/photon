package ru.sevoro.photon.screen.splash

import android.content.Context
import android.util.AttributeSet
import ru.sevoro.photon.screen.base.AbsView
import ru.sevoro.photon.util.getComponent

/**
 * @author Sergey Vorobyev
 */
class SplashViewImpl(context: Context, attrs: AttributeSet?) :
        AbsView<SplashPresenter>(context, attrs), SplashView {

    override fun initDagger(context: Context) = context.getComponent<SplashScreen.Component>().inject(this)
}
