package ru.sevoro.photon.screen.home

import ru.sevoro.photon.data.dto.realm.PhotoCardRealm
import ru.sevoro.photon.screen.base.BasePresenter

/**
 * @author Sergey Vorobyev
 */
interface HomePresenter : BasePresenter<HomeViewImpl> {

    fun onPhotoCardClick(photoCard: PhotoCardRealm)

    fun onOkButtonClickSignInDialog()

    fun onOkButtonClickSignUpDialog()
}