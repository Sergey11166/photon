package ru.sevoro.photon.screen.photo_card

import dagger.Provides
import ru.sevoro.photon.R
import ru.sevoro.photon.dagger.DaggerScope
import ru.sevoro.photon.data.dto.PermissionsResultDto
import ru.sevoro.photon.data.dto.realm.PhotoCardRealm
import ru.sevoro.photon.flow_mortar.Screen
import ru.sevoro.photon.screen.base.AbsScreen
import ru.sevoro.photon.screen.root.RootActivity
import rx.subjects.PublishSubject
import java.util.*

/**
 * @author Sergey Vorobyev
 */
@Screen(R.layout.screen_photo_card)
class PhotoCardScreen(private val photoCard: PhotoCardRealm, private val isFromAlbum: Boolean) :
        AbsScreen<RootActivity.RootComponent>() {

    override fun createScreenComponent(parentComponent: RootActivity.RootComponent?): Any {
        return DaggerPhotoCardScreen_Component.builder()
                .rootComponent(parentComponent)
                .module(Module(photoCard))
                .build()
    }

    override fun hashCode() = Objects.hash(photoCard.id, isFromAlbum)

    override fun equals(other: Any?) = other === this

    @DaggerScope(PhotoCardScreen::class)
    @dagger.Component(dependencies = arrayOf(RootActivity.RootComponent::class),
            modules = arrayOf(Module::class))
    interface Component {
        fun inject(view: PhotoCardViewImpl)

        fun inject(presenter: PhotoCardPresenterImpl)
    }

    @dagger.Module
    class Module(private val photoCard: PhotoCardRealm) {

        @Provides
        @DaggerScope(PhotoCardScreen::class)
        fun provideModel(): PhotoCardModel {
            return PhotoCardModelImpl()
        }

        @Provides
        @DaggerScope(PhotoCardScreen::class)
        fun providePresenter(onPermissionsResultSubject: PublishSubject<PermissionsResultDto>,
                             model: PhotoCardModel, viewModel: PhotoCardVM): PhotoCardPresenter {
            return PhotoCardPresenterImpl(photoCard, onPermissionsResultSubject, model, viewModel)
        }

        @Provides
        @DaggerScope(PhotoCardScreen::class)
        fun provideViewModel(): PhotoCardVM {
            return PhotoCardVM()
        }
    }
}