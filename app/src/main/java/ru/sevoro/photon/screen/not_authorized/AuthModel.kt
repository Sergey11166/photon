package ru.sevoro.photon.screen.not_authorized

import rx.Observable

/**
 * @author Sergey Vorobyev
 */
interface AuthModel {

    fun signUp(login: String, email: String, username: String, password: String): Observable<String>

    fun signIn(email: String, password: String): Observable<String>
}