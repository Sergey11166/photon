package ru.sevoro.photon.screen.splash

import ru.sevoro.photon.MIN_SEC_DELAY_SPLASH_SCREEN
import ru.sevoro.photon.screen.base.AbsModel
import rx.Observable
import rx.Observable.mergeDelayError
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import java.util.concurrent.TimeUnit

/**
 * @author Sergey Vorobyev
 */
class SplashModelImpl : AbsModel(), SplashModel {

    override fun updateLocalData(): Observable<Any> = mergeDelayError(
            Observable.just(1).delay(MIN_SEC_DELAY_SPLASH_SCREEN, TimeUnit.SECONDS),
            dataManager.updateAllPhotoCards(), updateCurrentUser())
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())

    override fun startUpdatePhotoCardsWithTimer() = dataManager.startUpdatePhotoCardsWithTimer()

    private fun updateCurrentUser(): Observable<*> {
        val currentUserId = prefsManager.getCurrentUserId()
        if (!currentUserId.isEmpty()) {
            return updateUser(currentUserId)
        } else {
            return Observable.empty<Any>()
        }
    }
}
