package ru.sevoro.photon.screen.not_authorized

import ru.sevoro.photon.dialog.SignInVM
import ru.sevoro.photon.dialog.SignUpVM

/**
 * @author Sergey Vorobyev
 */
interface NotAuthorizedView {

    fun showSignInDialog(viewModel: SignInVM)

    fun closeSignInDialog()

    fun showSignUpDialog(viewModel: SignUpVM)

    fun closeSignUpDialog()
}