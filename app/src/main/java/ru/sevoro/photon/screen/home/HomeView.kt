package ru.sevoro.photon.screen.home

import ru.sevoro.photon.data.dto.realm.PhotoCardRealm
import ru.sevoro.photon.dialog.SignInVM
import ru.sevoro.photon.dialog.SignUpVM
import ru.sevoro.photon.screen.base.BaseView

/**
 * @author Sergey Vorobyev
 */
interface HomeView : BaseView {

    fun addPhotoCard(photoCard: PhotoCardRealm)

    fun showSignInDialog(viewModel: SignInVM)

    fun closeSignInDialog()

    fun showSignUpDialog(viewModel: SignUpVM)

    fun closeSignUpDialog()
}