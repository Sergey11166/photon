package ru.sevoro.photon.screen.home

import dagger.Provides
import ru.sevoro.photon.R
import ru.sevoro.photon.dagger.DaggerScope
import ru.sevoro.photon.flow_mortar.Screen
import ru.sevoro.photon.screen.base.AbsScreen
import ru.sevoro.photon.screen.root.RootActivity
import ru.sevoro.photon.screen.root.RootPresenter

/**
 * @author Sergey Vorobyev
 */
@Screen(R.layout.screen_home)
class HomeScreen(private var specification: SearchPhotoCardsSpecification) : AbsScreen<RootActivity.RootComponent>() {

    override fun createScreenComponent(parentComponent: RootActivity.RootComponent?): Any {
        return DaggerHomeScreen_Component.builder()
                .rootComponent(parentComponent)
                .module(Module(specification))
                .build()
    }

    @DaggerScope(HomeScreen::class)
    @dagger.Component(dependencies = arrayOf(RootActivity.RootComponent::class),
            modules = arrayOf(Module::class))
    interface Component {
        fun inject(view: HomeViewImpl)

        fun inject(presenter: HomePresenterImpl)

        fun rootPresenter(): RootPresenter
    }

    @dagger.Module
    class Module(private var specification: SearchPhotoCardsSpecification) {

        @Provides
        @DaggerScope(HomeScreen::class)
        fun provideModel(): HomeModel {
            return HomeModelImpl()
        }

        @Provides
        @DaggerScope(HomeScreen::class)
        fun providePresenter(model: HomeModel): HomePresenter {
            return HomePresenterImpl(model, specification)
        }
    }
}