package ru.sevoro.photon.screen.photo_card

import android.databinding.Bindable
import ru.sevoro.photon.BR
import ru.sevoro.photon.screen.base.BaseViewModel

/**
 * @author Sergey Vorobyev
 */
class PhotoCardVM : BaseViewModel() {

    @get:Bindable
    var title: String = ""
        set(value) {
            field = value
            notifyPropertyChanged(BR.title)
        }

    @get:Bindable
    var photo = ""
        set(value) {
            field = value
            notifyPropertyChanged(BR.photo)
        }

    @get:Bindable
    var username = ""
        set(value) {
            field = value
            notifyPropertyChanged(BR.username)
        }

    @get:Bindable
    var avatar = ""
        set(value) {
            field = value
            notifyPropertyChanged(BR.avatar)
        }

    @get:Bindable
    var albumCount = ""
        set(value) {
            field = value
            notifyPropertyChanged(BR.albumCount)
        }

    @get:Bindable
    var photoCardCount = ""
        set(value) {
            field = value
            notifyPropertyChanged(BR.photoCardCount)
        }
}