package ru.sevoro.photon.screen.base

import android.os.Bundle
import android.support.design.widget.CoordinatorLayout
import android.support.design.widget.Snackbar
import android.support.v4.view.LayoutInflaterCompat
import android.support.v7.app.AppCompatActivity
import android.text.Html
import android.text.Spanned
import android.text.TextUtils
import android.view.View
import android.view.ViewGroup
import ru.sevoro.photon.util.InflaterFactory2

/**
 * @author Sergey Vorobyev
 */

abstract class BaseActivity : AppCompatActivity() {

    protected val rootView: View
        get() {
            val rootGroup: ViewGroup = findViewById(android.R.id.content)
            return rootGroup.getChildAt(0)
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        LayoutInflaterCompat.setFactory2(layoutInflater, InflaterFactory2(delegate))
        super.onCreate(savedInstanceState)
    }

    protected fun showSnackBar(marginBottom: Int, message: String, actionText: String, clickListener: () -> Unit) {
        if (TextUtils.isEmpty(message)) return
        val html = fromHtmlSupport("<font color=\"#ffffff\">$message</font>")
        val snack: Snackbar = Snackbar.make(rootView, html, Snackbar.LENGTH_LONG)
        val view = snack.view
        val params = view.layoutParams as CoordinatorLayout.LayoutParams
        params.setMargins(params.marginStart, params.topMargin, params.marginEnd, marginBottom)
        view.layoutParams = params
        if (!TextUtils.isEmpty(actionText)) {
            snack.setAction(actionText, { clickListener.invoke() })
        }
        snack.show()
    }

    private fun fromHtmlSupport(html: String): Spanned {
        val result: Spanned
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            result = Html.fromHtml(html, Html.FROM_HTML_MODE_LEGACY)
        } else {
            @Suppress("DEPRECATION")
            result = Html.fromHtml(html)
        }
        return result
    }
}
