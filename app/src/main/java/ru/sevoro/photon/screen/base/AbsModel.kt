package ru.sevoro.photon.screen.base

import com.birbit.android.jobqueue.JobManager
import ru.sevoro.photon.App
import ru.sevoro.photon.data.*
import ru.sevoro.photon.data.dto.realm.AlbumRealm
import ru.sevoro.photon.data.dto.realm.PhotoCardRealm
import ru.sevoro.photon.data.dto.realm.UserRealm
import ru.sevoro.photon.data.dto.request.CreateAlbumRequest
import ru.sevoro.photon.data.dto.response.CreatedIdResponse
import ru.sevoro.photon.data.dto.response.UserResponse
import ru.sevoro.photon.data.transformers.RestCallTransformer
import ru.sevoro.photon.data.transformers.UserTransformer
import ru.sevoro.photon.util.L
import rx.Observable
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import javax.inject.Inject

/**
 * @author Sergey Vorobyev
 */
abstract class AbsModel {

    @Inject
    lateinit protected var restService: RestService

    @Inject
    lateinit protected var dataManager: DataManager

    @Inject
    lateinit protected var fileManager: FileManager

    @Inject
    lateinit protected var realmManager: RealmManager

    @Inject
    lateinit protected var jobQueueManager: JobManager

    @Inject
    lateinit protected var prefsManager: PreferencesManager

    @Inject
    lateinit protected var userTransformer: UserTransformer

    @Inject
    lateinit protected var restCallTransformer: RestCallTransformer<*>

    init {
        @Suppress("LeakingThis")
        App.appComponent.inject(this)
    }

    protected fun updateUser(userId: String): Observable<Any> {
        val start = Observable.just(1)

        val fromServer = restService.getUser(/*user?.lastUpdate ?:*/ DEFAULT_LAST_MODIFIED, userId)
                .compose(userTransformer)
                .doOnNext { updateUserInRealm(it) }
                .flatMap { Observable.just(1) }
                .doOnError { it.printStackTrace() }
                .onErrorReturn { 1 }

        return Observable.merge(start, fromServer)
    }

    protected fun updateUserInRealm(userResponse: UserResponse) {
        L.d("updateUserInRealm thread: ${Thread.currentThread().name}")
        val newUser = UserRealm(userResponse)

        // if user in realm not synced by UpdateUserInfoJob then don't update user info
        val localUser = realmManager.getEntityById(UserRealm::class.java, userResponse.id)
        if (localUser != null && !localUser.isSynced) {
            newUser.username = localUser.username
            newUser.login = localUser.login
            newUser.isSynced = false
        }

        val oldAlbums = localUser?.albums
        if (oldAlbums != null && oldAlbums.isNotEmpty()) {
            loop@ for (oldAlbum in oldAlbums) {

                // if album in realm marked as deleted, then mark new album as deleted
                if (oldAlbum.isDeleted) {
                    val newAlbumList = newUser.albums.filter { it.id == oldAlbum.id }
                    if (newAlbumList.isNotEmpty()) {
                        newAlbumList.first().isDeleted = true
                        continue@loop
                    }
                }

                // if album in realm not synced by UpdateAlbumInfoJob then don't update album info
                if (!oldAlbum.isSynced) {
                    val newAlbumList = newUser.albums.filter { it.id == oldAlbum.id }
                    if (newAlbumList.isNotEmpty()) {
                        newAlbumList.first().title = oldAlbum.title
                        newAlbumList.first().description = oldAlbum.description
                    }
                }
            }
        }

        realmManager.saveEntity(newUser)
        userResponse.albums.forEach { album ->
            album.photoCards.forEach { (id, _, title, _, isActive) ->
                if (!isActive) {
                    L.d("photoCard deleted: $title")
                    realmManager.deleteFromAlbum(id, album.id)
                    realmManager.deleteEntityById(PhotoCardRealm::class.java, id)
                }
            }
            if (!album.isActive) {
                L.d("album deleted: ${album.title}")
                realmManager.deleteEntityById(AlbumRealm::class.java, album.id)
            }
        }
    }

    fun createNewAlbum(name: String, description: String): Observable<AlbumRealm> {
        @Suppress("UNCHECKED_CAST")
        return restService.createAlbum(
                prefsManager.getToken(),
                prefsManager.getCurrentUserId(),
                CreateAlbumRequest(name, description))
                .compose(restCallTransformer as RestCallTransformer<CreatedIdResponse>)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext {
                    val album = AlbumRealm()
                    album.id = it.id
                    album.owner = prefsManager.getCurrentUserId()
                    album.title = name
                    album.description = description
                    realmManager.saveEntity(album)
                    realmManager.addAlbumToUser(prefsManager.getCurrentUserId(), it.id)
                }
                .flatMap { Observable.just(realmManager.getEntityById(AlbumRealm::class.java, it.id)) }
    }
}