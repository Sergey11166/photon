package ru.sevoro.photon.screen.user

import android.content.Context
import android.databinding.DataBindingUtil
import android.util.AttributeSet
import android.widget.GridLayout
import ru.sevoro.photon.databinding.ScreenAuthorBinding
import ru.sevoro.photon.screen.base.BaseViewModel
import ru.sevoro.photon.util.getComponent

/**
 * @author Sergey Vorobyev
 */
class AuthorViewImpl(context: Context, attrs: AttributeSet?) :
        UserViewImpl<AuthorPresenter>(context, attrs) {

    private lateinit var binding: ScreenAuthorBinding

    override fun onFinishInflate() {
        super.onFinishInflate()
        if (!isInEditMode) binding = DataBindingUtil.bind(this)
    }

    override fun getAlbumsContainer(): GridLayout {
        return binding.layout!!.albumsContainer
    }

    override fun bindViewModel(viewModel: BaseViewModel) {
        binding.userModel = viewModel as UserVM
    }

    override fun initDagger(context: Context) {
        context.getComponent<AuthorScreen.Component>().inject(this)
    }
}