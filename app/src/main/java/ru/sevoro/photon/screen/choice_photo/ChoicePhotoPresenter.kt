package ru.sevoro.photon.screen.choice_photo

import ru.sevoro.photon.screen.base.BasePresenter

/**
 * @author Sergey Vorobyev
 */
interface ChoicePhotoPresenter : BasePresenter<ChoicePhotoViewImpl> {

    fun onChoiceButtonClick()
}