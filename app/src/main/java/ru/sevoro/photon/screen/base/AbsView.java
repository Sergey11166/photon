package ru.sevoro.photon.screen.base;

import android.app.Activity;
import android.app.DialogFragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.RelativeLayout;

import javax.inject.Inject;

import ru.sevoro.photon.screen.root.RootView;

/**
 * @author Sergey Vorobyev
 */
public abstract class AbsView<P extends BasePresenter> extends RelativeLayout implements BaseView {

    @Inject
    protected P presenter;

    public AbsView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        if (!isInEditMode()) initDagger(context);
    }

    @Override
    public void bindViewModel(@NonNull BaseViewModel viewModel) {

    }

    public boolean onViewBackPressed() {
        return false;
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (!isInEditMode()) {
            //noinspection unchecked
            presenter.takeView(this);
            int navigationItem = getCheckedNavigationItem();
            if (navigationItem > 0 && getRootMvpView() != null) {
                getRootMvpView().setCheckedNavigationItem(navigationItem);
            }
        }
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (!isInEditMode()) {
            //noinspection unchecked
            presenter.dropView(this);
        }
    }

    @Nullable
    protected RootView getRootMvpView() {
        return presenter.getRootView();
    }

    protected void showFragmentDialog(@NonNull DialogFragment dialog) {
        Activity activity = (Activity) getRootMvpView();
        if (activity != null) {
            FragmentTransaction transaction = activity.getFragmentManager().beginTransaction();
            transaction.add(dialog, dialog.getClass().getName());
            transaction.commitAllowingStateLoss();
        }
    }

    @IdRes
    protected int getCheckedNavigationItem() {
        return 0;
    }

    protected abstract void initDagger(Context context);
}
