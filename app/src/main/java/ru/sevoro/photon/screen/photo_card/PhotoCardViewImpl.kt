package ru.sevoro.photon.screen.photo_card

import android.content.Context
import android.databinding.DataBindingUtil
import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.TextView
import io.realm.RealmList
import ru.sevoro.photon.R
import ru.sevoro.photon.data.dto.realm.TagRealm
import ru.sevoro.photon.databinding.ScreenPhotoCardBinding
import ru.sevoro.photon.screen.base.AbsView
import ru.sevoro.photon.screen.base.BaseViewModel
import ru.sevoro.photon.util.getComponent

class PhotoCardViewImpl(context: Context, attrs: AttributeSet?) :
        AbsView<PhotoCardPresenter>(context, attrs), PhotoCardView {

    private lateinit var binding: ScreenPhotoCardBinding

    override fun onFinishInflate() {
        super.onFinishInflate()
        if (!isInEditMode) {
            binding = DataBindingUtil.bind(this)
            binding.avatar.setOnClickListener { presenter.onAvatarClick() }
            PhotoCardZoomAnimation(binding.photoContainer, binding.scrollView)
        }
    }

    override fun showTags(tags: RealmList<TagRealm>) {
        tags.forEach { item ->
            val tag = "#${item.value.toLowerCase()}"
            val view = LayoutInflater.from(context).inflate(R.layout.item_tag, binding.tagsContainer, false)
            view.findViewById<TextView>(R.id.tag).text = tag
            binding.tagsContainer.addView(view)
        }
    }

    override fun getPhotoBitmap(): Bitmap {
        return (binding.photo.drawable as BitmapDrawable).bitmap
    }

    override fun bindViewModel(viewModel: BaseViewModel) {
        binding.photoCardModel = viewModel as PhotoCardVM
    }

    override fun initDagger(context: Context) {
        context.getComponent<PhotoCardScreen.Component>().inject(this)
    }
}