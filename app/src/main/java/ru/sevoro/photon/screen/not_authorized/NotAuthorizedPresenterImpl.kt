package ru.sevoro.photon.screen.not_authorized

import flow.Direction.REPLACE
import flow.Flow
import mortar.MortarScope
import ru.sevoro.photon.R
import ru.sevoro.photon.data.error.ApiError
import ru.sevoro.photon.dialog.SignInVM
import ru.sevoro.photon.dialog.SignUpVM
import ru.sevoro.photon.screen.base.AbsPresenter
import ru.sevoro.photon.screen.root.ActionBarBuilder
import ru.sevoro.photon.screen.user.ProfileScreen
import ru.sevoro.photon.util.SERVICE_NAME
import ru.sevoro.photon.util.showToast
import rx.Subscriber

class NotAuthorizedPresenterImpl(private val model: AuthModel) :
        AbsPresenter<NotAuthorizedViewImpl>(), NotAuthorizedPresenter {

    private val signUpVM = SignUpVM()
    private val signInVM = SignInVM()

    override fun onSignInButtonClick() {
        view?.showSignInDialog(signInVM)
    }

    override fun onSignUpButtonClick() {
        view?.showSignUpDialog(signUpVM)
    }

    override fun onOkButtonClickSignInDialog() {
        if (!signInVM.isEmailValid || !signInVM.isPasswordValid) {
            view?.context?.showToast(R.string.message_tape_valid_data)
        } else {
            rootView?.showProgress()
            view?.closeSignInDialog()
            model.signIn(signInVM.email, signInVM.password).subscribe(object : Subscriber<String>() {

                override fun onNext(userId: String) {
                    rootView?.hideProgress()
                    Flow.get(view).replaceHistory(ProfileScreen(userId), REPLACE)
                }

                override fun onError(e: Throwable) {
                    e.printStackTrace()
                    rootView?.hideProgress()
                    if (e is ApiError && e.code == 404) {
                        view?.context?.showToast(R.string.auth_message_user_not_found)
                    } else {
                        view?.context?.showToast(e.message!!)
                    }
                    view?.showSignInDialog(signInVM)
                }

                override fun onCompleted() {}
            })
        }
    }

    override fun onOkButtonClickSignUpDialog() {
        if (!signUpVM.isLoginValid ||
                !signUpVM.isEmailValid ||
                !signUpVM.isUsernameValid ||
                !signUpVM.isPasswordValid) {
            view?.context?.showToast(R.string.message_tape_valid_data)
        } else {
            rootView?.showProgress()
            view?.closeSignUpDialog()
            model.signUp(signUpVM.login, signUpVM.email, signUpVM.username, signUpVM.password)
                    .subscribe(object : Subscriber<String>() {

                        override fun onNext(userId: String) {
                            rootView?.hideProgress()
                            Flow.get(view).replaceHistory(ProfileScreen(userId), REPLACE)
                        }

                        override fun onError(e: Throwable) {
                            rootView?.hideProgress()
                            view?.context?.showToast(e.message!!)
                            view?.showSignInDialog(signUpVM)
                        }

                        override fun onCompleted() {}
                    })
        }
    }

    override fun initActionBar() {
        val actionBarBuilder: ActionBarBuilder? = rootPresenter.newActionBarBuilder()
        @Suppress("IfThenToSafeAccess")
        if (actionBarBuilder != null) {
            actionBarBuilder
                    .setTitle(R.string.profile_title)
                    .build()
        }
    }

    override fun initDagger(scope: MortarScope) {
        scope.getService<NotAuthorizedScreen.Component>(SERVICE_NAME).inject(this)
    }
}