package ru.sevoro.photon.screen.photo_card

import android.graphics.Bitmap
import android.net.Uri
import ru.sevoro.photon.data.dto.realm.PhotoCardRealm
import ru.sevoro.photon.data.dto.realm.UserRealm
import rx.Observable

/**
 * @author Sergey Vorobyev
 */
interface PhotoCardModel {

    fun addPhotoCardView(photoCard: PhotoCardRealm)

    fun getUser(userId: String): Observable<UserRealm?>

    fun addToFavorites(photoCardId: String): Observable<Boolean>

    fun isAuthorized(): Boolean

    fun isFavorite(photoCardId: String): Boolean

    fun saveBitmapToCache(bitmap: Bitmap, photoCardId: String): Observable<Uri>

    fun saveBitmapGallery(bitmap: Bitmap, photoCardId: String): Observable<Uri>
}