package ru.sevoro.photon.screen.root

import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.support.design.widget.AppBarLayout
import android.support.design.widget.BottomNavigationView
import android.support.design.widget.TabLayout
import android.support.v4.content.ContextCompat
import android.support.v4.view.ViewPager
import android.support.v7.app.ActionBar
import android.support.v7.app.AppCompatDelegate.FEATURE_SUPPORT_ACTION_BAR
import android.support.v7.widget.Toolbar
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.FrameLayout
import dagger.Component
import flow.Direction
import flow.Flow
import mortar.MortarScope.findChild
import mortar.bundler.BundleServiceRunner
import ru.sevoro.photon.BuildConfig
import ru.sevoro.photon.R
import ru.sevoro.photon.dagger.AppComponent
import ru.sevoro.photon.dagger.DaggerScope
import ru.sevoro.photon.dagger.RootModule
import ru.sevoro.photon.data.dto.ActivityResultDto
import ru.sevoro.photon.data.dto.PermissionsResultDto
import ru.sevoro.photon.flow_mortar.TreeKeyDispatcher
import ru.sevoro.photon.screen.base.AbsScreen
import ru.sevoro.photon.screen.base.BaseActivity
import ru.sevoro.photon.screen.base.BaseView
import ru.sevoro.photon.screen.splash.SplashScreen
import ru.sevoro.photon.util.*
import rx.subjects.PublishSubject
import java.io.File
import java.lang.Boolean.TYPE
import javax.inject.Inject

/**
 * @author Sergey Vorobyev.
 */

private val KEY_IS_PROGRESS_SHOWING = "KEY_IS_PROGRESS_SHOWING"

class RootActivity : BaseActivity(), RootView, ActionBarView {

    @Inject
    lateinit var presenter: RootPresenter

    private var isShowIconsInPopupMenu = false
    private var actionBar: ActionBar? = null
    private val menuItems: MutableList<MenuItemHolder> = mutableListOf()
    private lateinit var toolbar: Toolbar
    private lateinit var progress: View
    private lateinit var appBarLayout: AppBarLayout
    private lateinit var bottomNavigation: BottomNavigationView

    private var isProgressShowing: Boolean = false

    override val currentScreen: BaseView
        get() {
            return findViewById<FrameLayout>(R.id.container).getChildAt(0) as BaseView
        }

    override fun attachBaseContext(context: Context) {
        val newBase = Flow.configure(context, this)
                .defaultKey(SplashScreen())
                .dispatcher(TreeKeyDispatcher(this))
                .install()
        super.attachBaseContext(newBase)
    }

    override fun getSystemService(name: String): Any {
        val rootActivityScope = findChild(applicationContext, RootActivity::class.java.name)
        return if (rootActivityScope.hasService(name))
            rootActivityScope.getService(name)
        else
            super.getSystemService(name)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_root)
        BundleServiceRunner.getBundleServiceRunner(this).onCreate(savedInstanceState)
        initWidgets()
        initToolbar()
        if (savedInstanceState == null) {
            bottomNavigation.waiteForMeasure({ _, _, height -> bottomNavigation.translationY = height.toFloat() })
        } else {
            isProgressShowing = savedInstanceState.getBoolean(KEY_IS_PROGRESS_SHOWING)
            if (isProgressShowing) showProgress()
            setBackground(android.R.color.white)
        }
    }

    private fun initWidgets() {
        toolbar = findViewById(R.id.toolbar)
        appBarLayout = findViewById(R.id.appbar)
        progress = findViewById(R.id.progress)
        bottomNavigation = findViewById(R.id.bottom_navigation)
        bottomNavigation.setOnNavigationItemSelectedListener { item ->
            val key: AbsScreen<*>? = presenter.onBottomMenuItemClick(item.itemId)
            if (key != null) {
                Flow.get(this).replaceHistory(key, Direction.REPLACE)
            }
            return@setOnNavigationItemSelectedListener key != null
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)
        getComponent<RootComponent>().inject(this)
        presenter.takeView(this)
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        BundleServiceRunner.getBundleServiceRunner(this).onSaveInstanceState(outState)
        outState?.putBoolean(KEY_IS_PROGRESS_SHOWING, isProgressShowing)
    }

    override fun onDestroy() {
        presenter.dropView()
        super.onDestroy()
    }

    override fun onBackPressed() {
        if (!currentScreen.onViewBackPressed() && !Flow.get(this).goBack()) {
            super.onBackPressed()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        presenter.onActivityResult(requestCode, resultCode, data)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>,
                                            grantResults: IntArray) {
        presenter.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    override fun onPrepareOptionsMenu(menu: Menu): Boolean {
        menu.clear()
        if (!menuItems.isEmpty()) {
            for ((iconResId, showFlag, title, listener) in menuItems) {
                val menuItem = menu.add(if (title > 0) getString(title) else "")
                menuItem.setShowAsActionFlags(showFlag)
                        .setIcon(iconResId)
                        .setOnMenuItemClickListener {
                            listener()
                            true
                        }
            }
        }

        return super.onPrepareOptionsMenu(menu)
    }

    override fun onMenuOpened(featureId: Int, menu: Menu?): Boolean {
        if (isShowIconsInPopupMenu && featureId == FEATURE_SUPPORT_ACTION_BAR && menu != null) {
            if (menu.javaClass.simpleName == "MenuBuilder") {
                try {
                    val method = menu.javaClass.getDeclaredMethod("setOptionalIconsVisible", TYPE)
                    method.isAccessible = true
                    method.invoke(menu, true)
                } catch (e: Exception) {
                    throw RuntimeException(e)
                }
            }
        }
        return super.onMenuOpened(featureId, menu)
    }

    override fun isAllPermissionsGranted(permissions: Array<String>) =
            permissions
                    .map { ContextCompat.checkSelfPermission(this, it) }
                    .none { it != PackageManager.PERMISSION_GRANTED }


    override fun showSnackBar(message: String) {
        showSnackBar(bottomNavigation.height, message, "", {})
    }

    override fun showSnackBar(message: String, actionText: String, clickListener: () -> Unit) {
        showSnackBar(bottomNavigation.height, message, actionText, clickListener)
    }

    override fun showSnackBar(messageId: Int) {
        showSnackBar(bottomNavigation.height, getString(messageId), "", {})
    }

    override fun showSnackBar(messageId: Int, actionTextId: Int, clickListener: () -> Unit) {
        showSnackBar(bottomNavigation.height, getString(messageId), getString(actionTextId), clickListener)
    }

    override fun showProgress() {
        progress.visibility = View.VISIBLE
        isProgressShowing = true
    }

    override fun hideProgress() {
        progress.visibility = View.GONE
        isProgressShowing = false
    }

    override fun setCheckedNavigationItem(itemId: Int) {
        bottomNavigation.selectedItemId = itemId
    }

    override fun startCameraForResult(file: File, requestCode: Int) = goToCameraApp(file, requestCode)

    override fun startGalleryForResult(requestCode: Int) = goToGalleryApp(requestCode)

    override fun startSettingForResult(requestCode: Int) = goToSettingsApp(requestCode)

    override fun setActionBarTitle(title: CharSequence) {
        actionBar?.title = title
    }

    override fun setActionBarTitle(resId: Int) {
        actionBar?.setTitle(resId)
    }

    override fun setActionBarVisibility(isVisible: Boolean) {
        if (isVisible) {
            actionBar?.show()
            if (Build.VERSION.SDK_INT < 21) {
                findViewById<View>(R.id.toolbar_shadow).visibility = View.VISIBLE
            }
        } else {
            actionBar?.hide()
            if (Build.VERSION.SDK_INT < 21) {
                findViewById<View>(R.id.toolbar_shadow).visibility = View.GONE
            }
        }
    }

    override fun showBottomNavigation() {
        bottomNavigation.animate()
                .translationY(0f)
                .start()
    }

    override fun setBackground(resId: Int) {
        findViewById<View>(R.id.coordinator_layout).background = ContextCompat.getDrawable(this, resId)
    }

    override fun setBackArrow(enable: Boolean) {
        actionBar?.setDisplayHomeAsUpEnabled(enable)
    }

    override fun setMenuItems(items: List<MenuItemHolder>) {
        menuItems.clear()
        menuItems.addAll(items)
        invalidateOptionsMenu()
    }

    override fun setOverflowIcon(icon: Int) {
        if (icon > 0) toolbar.overflowIcon = ContextCompat.getDrawable(this, icon)
    }

    override fun setShowIconsInPopupMenu(isShow: Boolean) {
        isShowIconsInPopupMenu = isShow
    }

    override fun setTabLayout(viewPager: ViewPager) {
        (0..appBarLayout.childCount)
                .filter { appBarLayout.getChildAt(it) is TabLayout }
                .forEach { return }

        val tabLayout = TabLayout(this)
        tabLayout.setupWithViewPager(viewPager)
        appBarLayout.addView(tabLayout)
        viewPager.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(tabLayout))

        if (Build.VERSION.SDK_INT < 21) {
            val shadow = findViewById<View>(R.id.toolbar_shadow)
            shadow.visibility = View.VISIBLE
            tabLayout.waiteForMeasure({ _, _, _ ->
                shadow.translationY = (appBarLayout.height - actionBar!!.height).toFloat()
            })
        }
    }

    override fun removeTabLayout() {
        val tabLayout = appBarLayout.getChildAt(1)
        if (tabLayout != null && tabLayout is TabLayout) {
            appBarLayout.removeView(tabLayout)
        }

        if (BuildConfig.VERSION_CODE < 21) {
            val shadow = findViewById<View>(R.id.toolbar_shadow)
            shadow.translationY = 0f
        }
    }

    private fun initToolbar() {
        setSupportActionBar(toolbar)
        actionBar = supportActionBar
        actionBar?.setHomeAsUpIndicator(R.drawable.ic_custom_back_black_24dp)
    }

    @DaggerScope(RootActivity::class)
    @Component(dependencies = arrayOf(AppComponent::class), modules = arrayOf(RootModule::class))
    interface RootComponent {
        fun inject(activity: RootActivity)

        fun inject(presenter: RootPresenterImpl)

        val rootPresenter: RootPresenter

        val onActivityResultSubject: PublishSubject<ActivityResultDto>

        val onPermissionsResultSubject: PublishSubject<PermissionsResultDto>
    }
}
