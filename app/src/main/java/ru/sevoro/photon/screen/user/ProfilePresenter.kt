package ru.sevoro.photon.screen.user

/**
 * @author Sergey Vorobyev
 */
interface ProfilePresenter : UserPresenter<ProfileViewImpl> {

    fun onChoiceFromGallery()

    fun onChoiceTakePhoto()

    fun onOkButtonClickEditProfileDialog()

    fun onOkButtonClickNewAlbumDialog()
}