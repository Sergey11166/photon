package ru.sevoro.photon.screen.base

import android.databinding.Bindable
import ru.sevoro.photon.BR

/**
 * @author Sergey Vorobyev
 */
class FiltersVM : BaseViewModel() {

    @get:Bindable
    var dishMeat: Boolean = false
        set(value) {
            field = value
            notifyPropertyChanged(BR.dishMeat)
        }

    fun changeDishMeat() {
        dishMeat = !dishMeat
        dishFish = false
        dishVegetables = false
        dishCheese = false
        dishFruits = false
        dishDesserts = false
        dishDrinks = false
    }

    @get:Bindable
    var dishFish: Boolean = false
        set(value) {
            field = value
            notifyPropertyChanged(BR.dishFish)
        }

    fun changeDishFish() {
        dishFish = !dishFish
        dishMeat = false
        dishVegetables = false
        dishCheese = false
        dishFruits = false
        dishDesserts = false
        dishDrinks = false
    }

    @get:Bindable
    var dishVegetables: Boolean = false
        set(value) {
            field = value
            notifyPropertyChanged(BR.dishVegetables)
        }

    fun changeDishVegetables() {
        dishVegetables = !dishVegetables
        dishMeat = false
        dishFish = false
        dishCheese = false
        dishFruits = false
        dishDesserts = false
        dishDrinks = false
    }

    @get:Bindable
    var dishFruits: Boolean = false
        set(value) {
            field = value
            notifyPropertyChanged(BR.dishFruits)
        }

    fun changeDishFruits() {
        dishFruits = !dishFruits
        dishMeat = false
        dishFish = false
        dishVegetables = false
        dishCheese = false
        dishDesserts = false
        dishDrinks = false
    }

    @get:Bindable
    var dishCheese: Boolean = false
        set(value) {
            field = value
            notifyPropertyChanged(BR.dishCheese)
        }

    fun changeDishCheese() {
        dishCheese = !dishCheese
        dishMeat = false
        dishFish = false
        dishVegetables = false
        dishFruits = false
        dishDesserts = false
        dishDrinks = false
    }

    @get:Bindable
    var dishDesserts: Boolean = false
        set(value) {
            field = value
            notifyPropertyChanged(BR.dishDesserts)
        }

    fun changeDishDesserts() {
        dishDesserts = !dishDesserts
        dishMeat = false
        dishFish = false
        dishVegetables = false
        dishCheese = false
        dishFruits = false
        dishDrinks = false
    }

    @get:Bindable
    var dishDrinks: Boolean = false
        set(value) {
            field = value
            notifyPropertyChanged(BR.dishDrinks)
        }

    fun changeDishDrinks() {
        dishDrinks = !dishDrinks
        dishMeat = false
        dishFish = false
        dishVegetables = false
        dishCheese = false
        dishFruits = false
        dishDesserts = false
    }

    @get:Bindable
    var tintRed: Boolean = false
        set(value) {
            field = value
            notifyPropertyChanged(BR.tintRed)
        }

    fun changeTintRed() {
        tintRed = !tintRed
    }

    @get:Bindable
    var tintOrange: Boolean = false
        set(value) {
            field = value
            notifyPropertyChanged(BR.tintOrange)
        }

    fun changeTintOrange() {
        tintOrange = !tintOrange
    }

    @get:Bindable
    var tintYellow: Boolean = false
        set(value) {
            field = value
            notifyPropertyChanged(BR.tintYellow)
        }

    fun changeTintYellow() {
        tintYellow = !tintYellow
    }

    @get:Bindable
    var tintGreen: Boolean = false
        set(value) {
            field = value
            notifyPropertyChanged(BR.tintGreen)
        }

    fun changeTintGreen() {
        tintGreen = !tintGreen
    }

    @get:Bindable
    var tintBlue: Boolean = false
        set(value) {
            field = value
            notifyPropertyChanged(BR.tintBlue)
        }

    fun changeTintBlue() {
        tintBlue = !tintBlue
    }

    @get:Bindable
    var tintDarkBlue: Boolean = false
        set(value) {
            field = value
            notifyPropertyChanged(BR.tintDarkBlue)
        }

    fun changeTintDarkBlue() {
        tintDarkBlue = !tintDarkBlue
    }

    @get:Bindable
    var tintViolet: Boolean = false
        set(value) {
            field = value
            notifyPropertyChanged(BR.tintViolet)
        }

    fun changeTintViolet() {
        tintViolet = !tintViolet
    }

    @get:Bindable
    var tintBrown: Boolean = false
        set(value) {
            field = value
            notifyPropertyChanged(BR.tintBrown)
        }

    fun changeTintBrown() {
        tintBrown = !tintBrown
    }

    @get:Bindable
    var tintBlack: Boolean = false
        set(value) {
            field = value
            notifyPropertyChanged(BR.tintBlack)
        }

    fun changeTintBlack() {
        tintBlack = !tintBlack
    }

    @get:Bindable
    var tintWhite: Boolean = false
        set(value) {
            field = value
            notifyPropertyChanged(BR.tintWhite)
        }

    fun changeTintWhite() {
        tintWhite = !tintWhite
    }

    @get:Bindable
    var decorRegular: Boolean = false
        set(value) {
            field = value
            notifyPropertyChanged(BR.decorRegular)
        }

    fun changeDecorRegular() {
        decorRegular = !decorRegular
        decorHoliday = false
    }

    @get:Bindable
    var decorHoliday: Boolean = false
        set(value) {
            field = value
            notifyPropertyChanged(BR.decorHoliday)
        }

    fun changeDecorHoliday() {
        decorHoliday = !decorHoliday
        decorRegular = false
    }

    @get:Bindable
    var tempHot: Boolean = false
        set(value) {
            field = value
            notifyPropertyChanged(BR.tempHot)
        }

    fun changeTempHot() {
        tempHot = !tempHot
        tempComfort = false
        tempCold = false
    }

    @get:Bindable
    var tempComfort: Boolean = false
        set(value) {
            field = value
            notifyPropertyChanged(BR.tempComfort)
        }

    fun changeTempComfort() {
        tempComfort = !tempComfort
        tempHot = false
        tempCold = false
    }

    @get:Bindable
    var tempCold: Boolean = false
        set(value) {
            field = value
            notifyPropertyChanged(BR.tempCold)
        }

    fun changeTempCold() {
        tempCold = !tempCold
        tempHot = false
        tempComfort = false
    }

    @get:Bindable
    var lightNatural: Boolean = false
        set(value) {
            field = value
            notifyPropertyChanged(BR.lightNatural)
        }

    fun changeLightNatural() {
        lightNatural = !lightNatural
        lightSynthetic = false
        lightMixed = false
    }

    @get:Bindable
    var lightSynthetic: Boolean = false
        set(value) {
            field = value
            notifyPropertyChanged(BR.lightSynthetic)
        }

    fun changeLightSynthetic() {
        lightSynthetic = !lightSynthetic
        lightNatural = false
        lightMixed = false
    }

    @get:Bindable
    var lightMixed: Boolean = false
        set(value) {
            field = value
            notifyPropertyChanged(BR.lightMixed)
        }

    fun changeLightMixed() {
        lightMixed = !lightMixed
        lightNatural = false
        lightSynthetic = false
    }

    @get:Bindable
    var lightDirFront: Boolean = false
        set(value) {
            field = value
            notifyPropertyChanged(BR.lightDirFront)
        }

    fun changeLightDirFront() {
        lightDirFront = !lightDirFront
        lightDirBack = false
        lightDirSide = false
        lightDirMixed = false
    }

    @get:Bindable
    var lightDirBack: Boolean = false
        set(value) {
            field = value
            notifyPropertyChanged(BR.lightDirBack)
        }

    fun changeLightDirBack() {
        lightDirBack = !lightDirBack
        lightDirFront = false
        lightDirSide = false
        lightDirMixed = false
    }

    @get:Bindable
    var lightDirSide: Boolean = false
        set(value) {
            field = value
            notifyPropertyChanged(BR.lightDirSide)
        }

    fun changeLightDirSide() {
        lightDirSide = !lightDirSide
        lightDirFront = false
        lightDirBack = false
        lightDirMixed = false
    }

    @get:Bindable
    var lightDirMixed: Boolean = false
        set(value) {
            field = value
            notifyPropertyChanged(BR.lightDirMixed)
        }

    fun changeLightDirMixed() {
        lightDirMixed = !lightDirMixed
        lightDirFront = false
        lightDirBack = false
        lightDirSide = false
    }

    @get:Bindable
    var lightCount1: Boolean = false
        set(value) {
            field = value
            notifyPropertyChanged(BR.lightCount1)
        }

    fun changeLightCount1() {
        lightCount1 = !lightCount1
        lightCount2 = false
        lightCount3 = false
    }

    @get:Bindable
    var lightCount2: Boolean = false
        set(value) {
            field = value
            notifyPropertyChanged(BR.lightCount2)
        }

    fun changeLightCount2() {
        lightCount2 = !lightCount2
        lightCount1 = false
        lightCount3 = false
    }

    @get:Bindable
    var lightCount3: Boolean = false
        set(value) {
            field = value
            notifyPropertyChanged(BR.lightCount3)
        }

    fun changeLightCount3() {
        lightCount3 = !lightCount3
        lightCount1 = false
        lightCount2 = false
    }

    fun clear() {
        dishMeat = false
        dishFish = false
        dishVegetables = false
        dishFruits = false
        dishCheese = false
        dishDesserts = false
        dishDrinks = false

        tintRed = false
        tintOrange = false
        tintYellow = false
        tintGreen = false
        tintBlue = false
        tintDarkBlue = false
        tintViolet = false
        tintBrown = false
        tintBlack = false
        tintWhite = false

        decorRegular = false
        decorHoliday = false

        tempHot = false
        tempComfort = false
        tempCold = false

        lightNatural = false
        lightSynthetic = false
        lightMixed = false

        lightDirFront = false
        lightDirBack = false
        lightDirSide = false
        lightDirMixed = false

        lightCount1 = false
        lightCount2 = false
        lightCount3 = false
    }
}