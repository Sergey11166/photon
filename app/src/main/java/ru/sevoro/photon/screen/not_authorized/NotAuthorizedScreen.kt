package ru.sevoro.photon.screen.not_authorized

import dagger.Provides
import ru.sevoro.photon.R
import ru.sevoro.photon.dagger.DaggerScope
import ru.sevoro.photon.flow_mortar.Screen
import ru.sevoro.photon.screen.base.AbsScreen
import ru.sevoro.photon.screen.root.RootActivity

/**
 * @author Sergey Vorobyev
 */
@Screen(value = R.layout.screen_not_authorized)
class NotAuthorizedScreen : AbsScreen<RootActivity.RootComponent>() {

    override fun createScreenComponent(parentComponent: RootActivity.RootComponent?): Any {
        return DaggerNotAuthorizedScreen_Component.builder()
                .rootComponent(parentComponent)
                .module(Module())
                .build()
    }

    @DaggerScope(NotAuthorizedScreen::class)
    @dagger.Component(dependencies = arrayOf(RootActivity.RootComponent::class),
            modules = arrayOf(Module::class))
    interface Component {
        fun inject(view: NotAuthorizedViewImpl)

        fun inject(presenter: NotAuthorizedPresenterImpl)
    }

    @dagger.Module
    class Module {
        @Provides
        @DaggerScope(NotAuthorizedScreen::class)
        fun provideModel(): AuthModel {
            return AuthModelImpl()
        }

        @Provides
        @DaggerScope(NotAuthorizedScreen::class)
        fun providePresenter(model: AuthModel): NotAuthorizedPresenter {
            return NotAuthorizedPresenterImpl(model)
        }
    }
}