package ru.sevoro.photon.screen.new_photo_card

import android.databinding.Bindable
import ru.sevoro.photon.BR
import ru.sevoro.photon.screen.base.BaseViewModel

/**
 * @author Sergey Vorobyev
 */
class NewPhotoCardVM : BaseViewModel() {

    @get:Bindable
    var photoName: String = ""
        set(value) {
            field = value
            notifyPropertyChanged(BR.photoName)
        }

    fun clearPhotoName() {
        photoName = ""
    }

    @get:Bindable
    var tagName: String = ""
        set(value) {
            field = value
            notifyPropertyChanged(BR.tagName)
        }

    fun clearTagName() {
        tagName = ""
    }

    @get:Bindable
    var isAlbumsExists: Boolean = false
        set(value) {
            field = value
            notifyPropertyChanged(BR.albumsExists)
        }
}