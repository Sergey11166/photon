package ru.sevoro.photon.screen.user

import dagger.Provides
import ru.sevoro.photon.R
import ru.sevoro.photon.dagger.DaggerScope
import ru.sevoro.photon.data.dto.ActivityResultDto
import ru.sevoro.photon.data.dto.PermissionsResultDto
import ru.sevoro.photon.flow_mortar.Screen
import ru.sevoro.photon.screen.base.AbsScreen
import ru.sevoro.photon.screen.root.RootActivity
import rx.subjects.PublishSubject

/**
 * @author Sergey Vorobyev
 */
@Screen(R.layout.screen_profile)
class ProfileScreen(private val userId: String) : AbsScreen<RootActivity.RootComponent>() {

    override fun createScreenComponent(parentComponent: RootActivity.RootComponent?): Any {
        return DaggerProfileScreen_Component.builder()
                .rootComponent(parentComponent)
                .module(Module(userId))
                .build()
    }

    @DaggerScope(ProfileScreen::class)
    @dagger.Component(dependencies = arrayOf(RootActivity.RootComponent::class),
            modules = arrayOf(Module::class))
    interface Component {
        fun inject(view: ProfileViewImpl)

        fun inject(presenter: ProfilePresenterImpl)
    }

    @dagger.Module
    class Module(private val userId: String) {

        @Provides
        @DaggerScope(ProfileScreen::class)
        fun provideModel(): UserModel {
            return UserModelImpl()
        }

        @Provides
        @DaggerScope(ProfileScreen::class)
        fun providePresenter(activityResultSubject: PublishSubject<ActivityResultDto>,
                             permissionsResultSubject: PublishSubject<PermissionsResultDto>,
                             model: UserModel, viewModel: UserVM): ProfilePresenter {
            return ProfilePresenterImpl(userId, activityResultSubject, permissionsResultSubject, model, viewModel)
        }

        @Provides
        @DaggerScope(ProfileScreen::class)
        fun provideViewModel(): UserVM {
            return UserVM()
        }
    }
}