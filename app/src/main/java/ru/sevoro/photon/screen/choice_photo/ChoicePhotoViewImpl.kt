package ru.sevoro.photon.screen.choice_photo

import android.content.Context
import android.util.AttributeSet
import android.view.View
import ru.sevoro.photon.R
import ru.sevoro.photon.screen.base.AbsView
import ru.sevoro.photon.util.getComponent

/**
 * @author Sergey Vorobyev
 */
class ChoicePhotoViewImpl(context: Context, attrs: AttributeSet?) :
        AbsView<ChoicePhotoPresenter>(context, attrs), ChoicePhotoView {

    override fun onFinishInflate() {
        super.onFinishInflate()
        if (!isInEditMode) {
            findViewById<View>(R.id.choice_button).setOnClickListener { presenter.onChoiceButtonClick() }
        }
    }

    override fun getCheckedNavigationItem() = R.id.nav_upload

    override fun initDagger(context: Context) {
        context.getComponent<ChoicePhotoScreen.Component>().inject(this)
    }
}