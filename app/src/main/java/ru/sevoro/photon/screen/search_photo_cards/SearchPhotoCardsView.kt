package ru.sevoro.photon.screen.search_photo_cards

import ru.sevoro.photon.screen.base.FiltersVM

/**
 * @author Sergey Vorobyev
 */
interface SearchPhotoCardsView {

    fun bindSearchViewModel(viewModel: SearchPhotoCardsVM)

    fun bindFiltersViewModel(viewModel: FiltersVM)

    fun showSearchHistory(history: List<String>)

    fun showTags(tags: List<String>)

    fun selectTags(tagsPositions: List<Int>)

    fun selectTag(tagPosition: Int)

    fun unSelectTag(tagPosition: Int)
}