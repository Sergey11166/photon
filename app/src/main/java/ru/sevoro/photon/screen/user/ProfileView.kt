package ru.sevoro.photon.screen.user

import ru.sevoro.photon.dialog.EditAlbumVM
import ru.sevoro.photon.dialog.EditProfileVM

/**
 * @author Sergey Vorobyev
 */
interface ProfileView : UserView {

    fun showChangeAvatarDialog()

    fun showEditProfileDialog(viewModel: EditProfileVM)

    fun closeEditProfileDialog()

    fun showNewAlbumDialog(viewModel: EditAlbumVM)

    fun closeNewAlbumDialog()
}