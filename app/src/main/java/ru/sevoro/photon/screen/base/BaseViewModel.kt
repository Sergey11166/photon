package ru.sevoro.photon.screen.base

import android.databinding.BaseObservable

/**
 * @author Sergey Vorobyev
 */
abstract class BaseViewModel : BaseObservable()
