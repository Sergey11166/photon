package ru.sevoro.photon.screen.album

import android.databinding.Bindable
import ru.sevoro.photon.BR
import ru.sevoro.photon.screen.base.BaseViewModel

/**
 * @author Sergey Vorobyev
 */
class AlbumVM : BaseViewModel() {

    @get:Bindable
    var title: String = ""
        set(value) {
            field = value
            notifyPropertyChanged(BR.title)
        }

    @get:Bindable
    var photoCardCount = ""
        set(value) {
            field = value
            notifyPropertyChanged(BR.photoCardCount)
        }

    @get:Bindable
    var description = ""
        set(value) {
            field = value
            notifyPropertyChanged(BR.description)
        }
}