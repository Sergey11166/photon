package ru.sevoro.photon.dagger

/**
 * @author Sergey Vorobyev
 */
class TestNetworkModule(private val baseUrl: String) : NetworkModule() {

    override fun getBaseUrl() = baseUrl
}