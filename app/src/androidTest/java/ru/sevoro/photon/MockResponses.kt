package ru.sevoro.photon

/**
 * @author Sergey Vorobyev
 */

const val IS_SUCCESS_RESPONSE = "{\n" +
        "    \"success\": true\n" +
        "}"

const val CREATED_ID_RESPONSE = "{\n" +
        "    \"id\": \"5938f5faedc80c538642ab6e\"\n" +
        "}"

const val GET_ALL_PHOTO_CARDS = "[\n" +
        "    {\n" +
        "        \"owner\": \"595aa0930e55f63612195842\",\n" +
        "        \"title\": \"Ещё кофе\",\n" +
        "        \"photo\": \"http://207.154.248.163:5000/uploads/5938f5faedc80c538642ab6e/image_1500233183312.jpg\",\n" +
        "        \"active\": true,\n" +
        "        \"updated\": \"2017-07-24T08:18:49.567Z\",\n" +
        "        \"created\": \"2017-07-16T19:26:23.410Z\",\n" +
        "        \"filters\": {\n" +
        "            \"decor\": \"holiday\",\n" +
        "            \"dish\": \"drinks\",\n" +
        "            \"light\": \"\",\n" +
        "            \"lightDirection\": \"\",\n" +
        "            \"lightSource\": \"\",\n" +
        "            \"nuances\": \"brown, black\",\n" +
        "            \"temperature\": \"hot\"\n" +
        "        },\n" +
        "        \"tags\": [\n" +
        "            \"жареные\"\n" +
        "        ],\n" +
        "        \"favorits\": 0,\n" +
        "        \"views\": 18,\n" +
        "        \"id\": \"596bbddf6378ff2d991b904c\"\n" +
        "    },\n" +
        "    {\n" +
        "        \"owner\": \"595aa0930e55f63612195842\",\n" +
        "        \"title\": \"Крем-брюле\",\n" +
        "        \"photo\": \"http://207.154.248.163:5000/uploads/593cbd984973f3617e5b4c86/image_1499582597881.jpg\",\n" +
        "        \"active\": true,\n" +
        "        \"updated\": \"2017-07-24T08:18:49.397Z\",\n" +
        "        \"created\": \"2017-07-09T06:43:18.024Z\",\n" +
        "        \"filters\": {\n" +
        "            \"decor\": \"simple\",\n" +
        "            \"dish\": \"dessert\",\n" +
        "            \"light\": \"natural\",\n" +
        "            \"lightDirection\": \"direct\",\n" +
        "            \"lightSource\": \"one\",\n" +
        "            \"nuances\": \"red, orange, yellow\",\n" +
        "            \"temperature\": \"cold\"\n" +
        "        },\n" +
        "        \"tags\": [\n" +
        "            \"Десерт\",\n" +
        "            \"Сливки\"\n" +
        "        ],\n" +
        "        \"favorits\": 0,\n" +
        "        \"views\": 22,\n" +
        "        \"id\": \"5961d0866378ff2d991b8ba3\"\n" +
        "    },\n" +
        "    {\n" +
        "        \"owner\": \"595aa0930e55f63612195842\",\n" +
        "        \"title\": \"Мороженое \\\"Ёжик\\\"\",\n" +
        "        \"photo\": \"http://207.154.248.163:5000/uploads/5938f5faedc80c538642ab6e/image_1499752431891.jpg\",\n" +
        "        \"active\": true,\n" +
        "        \"updated\": \"2017-07-24T08:18:49.038Z\",\n" +
        "        \"created\": \"2017-07-11T05:53:51.992Z\",\n" +
        "        \"filters\": {\n" +
        "            \"decor\": \"holiday\",\n" +
        "            \"dish\": \"dessert\",\n" +
        "            \"light\": \"natural\",\n" +
        "            \"lightDirection\": \"\",\n" +
        "            \"lightSource\": \"\",\n" +
        "            \"nuances\": \"violet\",\n" +
        "            \"temperature\": \"cold\"\n" +
        "        },\n" +
        "        \"tags\": [\n" +
        "            \"мороженое\"\n" +
        "        ],\n" +
        "        \"favorits\": 1,\n" +
        "        \"views\": 36,\n" +
        "        \"id\": \"596467ef6378ff2d991b8bc9\"\n" +
        "    }\n" +
        "]"

const val GET_USER = "{\n" +
        "    \"token\": \"278a3dfa2d316e6e09619a2500bec0578425e364\",\n" +
        "    \"name\": \"Sergey\",\n" +
        "    \"login\": \"Sergey11166\",\n" +
        "    \"albums\": [\n" +
        "        {\n" +
        "            \"owner\": \"595aa0930e55f63612195842\",\n" +
        "            \"title\": \"Избранное\",\n" +
        "            \"description\": \"Альбом для понравившихся фотографий\",\n" +
        "            \"active\": true,\n" +
        "            \"photocards\": [\n" +
        "                {\n" +
        "                    \"owner\": \"595aa0930e55f63612195842\",\n" +
        "                    \"title\": \"Ролы\",\n" +
        "                    \"photo\": \"http://st.biglion.ru/cfs7/company_photo/a5/d6/a5d6b66a1dbd2c2401716f535995da4f.JPG\",\n" +
        "                    \"active\": false,\n" +
        "                    \"updated\": \"2017-07-10T07:37:32.889Z\",\n" +
        "                    \"created\": \"2017-06-18T19:05:54.938Z\",\n" +
        "                    \"filters\": {\n" +
        "                        \"dish\": \"meat\",\n" +
        "                        \"nuances\": \"red\",\n" +
        "                        \"decor\": \"holiday\",\n" +
        "                        \"temperature\": \"hot\",\n" +
        "                        \"light\": \"natural\",\n" +
        "                        \"lightDirection\": \"direct\",\n" +
        "                        \"lightSource\": \"one\"\n" +
        "                    },\n" +
        "                    \"tags\": [\n" +
        "                        \"рыба\",\n" +
        "                        \"япония\"\n" +
        "                    ],\n" +
        "                    \"favorits\": 3,\n" +
        "                    \"views\": 30,\n" +
        "                    \"id\": \"5946cf124a96324fc6fae5f4\"\n" +
        "                },\n" +
        "                {\n" +
        "                    \"owner\": \"595aa0930e55f63612195842\",\n" +
        "                    \"title\": \"Мясное рагу в воке\",\n" +
        "                    \"photo\": \"http://207.154.248.163:5000/uploads/5936b4e4569f847fd20b4f93/image_1496908418386.jpg\",\n" +
        "                    \"active\": true,\n" +
        "                    \"updated\": \"2017-07-23T08:44:50.889Z\",\n" +
        "                    \"created\": \"2017-06-08T08:20:43.264Z\",\n" +
        "                    \"filters\": {\n" +
        "                        \"dish\": \"meat\",\n" +
        "                        \"nuances\": \"orange, brown\",\n" +
        "                        \"decor\": \"simple\",\n" +
        "                        \"temperature\": \"hot\",\n" +
        "                        \"light\": \"synthetic\",\n" +
        "                        \"lightDirection\": \"sideLight\",\n" +
        "                        \"lightSource\": \"one\"\n" +
        "                    },\n" +
        "                    \"tags\": [\n" +
        "                        \"мясо\",\n" +
        "                        \"рагу\",\n" +
        "                        \"мясное рагу\",\n" +
        "                        \"мясное блюдо\",\n" +
        "                        \"овощи\",\n" +
        "                        \"вок\",\n" +
        "                        \"жареные\",\n" +
        "                        \"приготовление\",\n" +
        "                        \"процесс\",\n" +
        "                        \"кусочки\",\n" +
        "                        \"пар\",\n" +
        "                        \"горячее\",\n" +
        "                        \"коричневый\",\n" +
        "                        \"черный\",\n" +
        "                        \"темное\",\n" +
        "                        \"стир-фрай\"\n" +
        "                    ],\n" +
        "                    \"favorits\": 9,\n" +
        "                    \"views\": 567,\n" +
        "                    \"id\": \"593908dbedc80c538642ab70\"\n" +
        "                },\n" +
        "                {\n" +
        "                    \"owner\": \"595aa0930e55f63612195842\",\n" +
        "                    \"title\": \"Торт \\\"На воздушном шаре\\\"\",\n" +
        "                    \"photo\": \"http://207.154.248.163:5000/uploads/595aa0930e55f63612195842/image_1500121165235.jpg\",\n" +
        "                    \"active\": true,\n" +
        "                    \"updated\": \"2017-07-23T08:47:29.850Z\",\n" +
        "                    \"created\": \"2017-07-15T12:19:25.416Z\",\n" +
        "                    \"filters\": {\n" +
        "                        \"decor\": \"holiday\",\n" +
        "                        \"dish\": \"dessert\",\n" +
        "                        \"light\": \"natural\",\n" +
        "                        \"lightDirection\": \"backlight\",\n" +
        "                        \"lightSource\": \"one\",\n" +
        "                        \"nuances\": \"red, yellow, blue, white\",\n" +
        "                        \"temperature\": \"cold\"\n" +
        "                    },\n" +
        "                    \"tags\": [\n" +
        "                        \"торт\",\n" +
        "                        \"воздушный шар\"\n" +
        "                    ],\n" +
        "                    \"favorits\": 1,\n" +
        "                    \"views\": 8,\n" +
        "                    \"id\": \"596a084d6378ff2d991b8fe5\"\n" +
        "                }\n" +
        "            ],\n" +
        "            \"isFavorite\": true,\n" +
        "            \"id\": \"595aa0930e55f63612195843\",\n" +
        "            \"views\": 2332,\n" +
        "            \"favorits\": 64\n" +
        "        },\n" +
        "        {\n" +
        "            \"owner\": \"595aa0930e55f63612195842\",\n" +
        "            \"title\": \"салаты\",\n" +
        "            \"description\": \"фотографии салатов\",\n" +
        "            \"active\": true,\n" +
        "            \"photocards\": [\n" +
        "                {\n" +
        "                    \"owner\": \"595aa0930e55f63612195842\",\n" +
        "                    \"title\": \"Фруктовый салат\",\n" +
        "                    \"photo\": \"http://207.154.248.163:5000/uploads/595aa0930e55f63612195842/image_1499891443181.jpg\",\n" +
        "                    \"active\": true,\n" +
        "                    \"updated\": \"2017-07-21T11:29:56.805Z\",\n" +
        "                    \"created\": \"2017-07-12T20:30:43.279Z\",\n" +
        "                    \"filters\": {\n" +
        "                        \"decor\": \"holiday\",\n" +
        "                        \"dish\": \"fruits\",\n" +
        "                        \"light\": \"synthetic\",\n" +
        "                        \"lightDirection\": \"direct\",\n" +
        "                        \"lightSource\": \"three\",\n" +
        "                        \"nuances\": \"red, green, white\",\n" +
        "                        \"temperature\": \"cold\"\n" +
        "                    },\n" +
        "                    \"tags\": [\n" +
        "                        \"фрукты\",\n" +
        "                        \"салат\",\n" +
        "                        \"виноград\"\n" +
        "                    ],\n" +
        "                    \"favorits\": 1,\n" +
        "                    \"views\": 20,\n" +
        "                    \"id\": \"596686f36378ff2d991b8bf7\"\n" +
        "                },\n" +
        "                {\n" +
        "                    \"owner\": \"595aa0930e55f63612195842\",\n" +
        "                    \"title\": \"селедка под шубой\",\n" +
        "                    \"photo\": \"http://207.154.248.163:5000/uploads/595aa0930e55f63612195842/image_1499917019280.jpg\",\n" +
        "                    \"active\": true,\n" +
        "                    \"updated\": \"2017-07-24T07:57:04.237Z\",\n" +
        "                    \"created\": \"2017-07-13T03:36:59.387Z\",\n" +
        "                    \"filters\": {\n" +
        "                        \"decor\": \"holiday\",\n" +
        "                        \"dish\": \"fish\",\n" +
        "                        \"light\": \"synthetic\",\n" +
        "                        \"lightDirection\": \"backlight\",\n" +
        "                        \"lightSource\": \"one\",\n" +
        "                        \"nuances\": \"red, violet\",\n" +
        "                        \"temperature\": \"cold\"\n" +
        "                    },\n" +
        "                    \"tags\": [\n" +
        "                        \"салат\",\n" +
        "                        \"рыба\",\n" +
        "                        \"селедка\",\n" +
        "                        \"шуба\"\n" +
        "                    ],\n" +
        "                    \"favorits\": 0,\n" +
        "                    \"views\": 58,\n" +
        "                    \"id\": \"5966eadb6378ff2d991b8bf8\"\n" +
        "                },\n" +
        "                {\n" +
        "                    \"owner\": \"595aa0930e55f63612195842\",\n" +
        "                    \"title\": \"салат из курицы\",\n" +
        "                    \"photo\": \"http://207.154.248.163:5000/uploads/595aa0930e55f63612195842/image_1500002495876.jpg\",\n" +
        "                    \"active\": true,\n" +
        "                    \"updated\": \"2017-07-22T15:52:03.334Z\",\n" +
        "                    \"created\": \"2017-07-14T03:21:35.972Z\",\n" +
        "                    \"filters\": {\n" +
        "                        \"decor\": \"holiday\",\n" +
        "                        \"dish\": \"meat\",\n" +
        "                        \"light\": \"synthetic\",\n" +
        "                        \"lightDirection\": \"backlight\",\n" +
        "                        \"lightSource\": \"one\",\n" +
        "                        \"nuances\": \"yellow, green, brown\",\n" +
        "                        \"temperature\": \"middle\"\n" +
        "                    },\n" +
        "                    \"tags\": [\n" +
        "                        \"салат\",\n" +
        "                        \"курица\",\n" +
        "                        \"оливки\"\n" +
        "                    ],\n" +
        "                    \"favorits\": 1,\n" +
        "                    \"views\": 13,\n" +
        "                    \"id\": \"596838bf6378ff2d991b8c76\"\n" +
        "                }\n" +
        "            ],\n" +
        "            \"isFavorite\": false,\n" +
        "            \"id\": \"595f89086378ff2d991b8b7d\",\n" +
        "            \"views\": 118,\n" +
        "            \"favorits\": 3\n" +
        "        },\n" +
        "        {\n" +
        "            \"owner\": \"595aa0930e55f63612195842\",\n" +
        "            \"title\": \"десерты\",\n" +
        "            \"description\": \"Фотографии десертов\",\n" +
        "            \"active\": false,\n" +
        "            \"photocards\": [],\n" +
        "            \"isFavorite\": false,\n" +
        "            \"id\": \"5967206e6378ff2d991b8bf9\",\n" +
        "            \"views\": 0,\n" +
        "            \"favorits\": 0\n" +
        "        }\n" +
        "    ],\n" +
        "    \"avatar\": \"http://207.154.248.163:5000/uploads/595aa0930e55f63612195842/image_1499370211772.jpg\",\n" +
        "    \"id\": \"595aa0930e55f63612195842\",\n" +
        "    \"albumCount\": 3,\n" +
        "    \"photocardCount\": 22\n" +
        "}"

const val ALBUM_RESPONSE = "{\n" +
        "        \"owner\": \"595aa0930e55f63612195842\",\n" +
        "        \"title\": \"мясо\",\n" +
        "        \"description\": \"фотографии мясных блюд\",\n" +
        "        \"active\": true,\n" +
        "        \"photocards\": [\n" +
        "            {\n" +
        "                \"owner\": \"595aa0930e55f63612195842\",\n" +
        "                \"title\": \"Сало с луком\",\n" +
        "                \"photo\": \"http://207.154.248.163:5000/uploads/595aa0930e55f63612195842/image_1499890660551.jpg\",\n" +
        "                \"active\": true,\n" +
        "                \"updated\": \"2017-07-23T16:30:17.828Z\",\n" +
        "                \"created\": \"2017-07-12T20:17:40.710Z\",\n" +
        "                \"filters\": {\n" +
        "                    \"decor\": \"simple\",\n" +
        "                    \"dish\": \"meat\",\n" +
        "                    \"light\": \"natural\",\n" +
        "                    \"lightDirection\": \"sideLight\",\n" +
        "                    \"lightSource\": \"one\",\n" +
        "                    \"nuances\": \"red, green, white\",\n" +
        "                    \"temperature\": \"middle\"\n" +
        "                },\n" +
        "                \"tags\": [\n" +
        "                    \"мясное блюдо\",\n" +
        "                    \"мясо\",\n" +
        "                    \"лук\"\n" +
        "                ],\n" +
        "                \"favorits\": 1,\n" +
        "                \"views\": 22,\n" +
        "                \"id\": \"596683e46378ff2d991b8bf6\"\n" +
        "            }\n" +
        "        ],\n" +
        "        \"isFavorite\": false,\n" +
        "        \"id\": \"595f87736378ff2d991b8b7c\",\n" +
        "        \"views\": 22,\n" +
        "        \"favorits\": 1\n" +
        "    }"

const val TAGS_RESPONSE = "[\n" +
        "    \"мясо\",\n" +
        "    \"рагу\",\n" +
        "    \"мясное рагу\",\n" +
        "    \"мясное блюдо\",\n" +
        "    \"овощи\",\n" +
        "    \"вок\",\n" +
        "    \"жареные\",\n" +
        "    \"приготовление\",\n" +
        "    \"процесс\",\n" +
        "    \"кусочки\",\n" +
        "    \"пар\",\n" +
        "    \"горячее\",\n" +
        "    \"коричневый\",\n" +
        "    \"черный\",\n" +
        "    \"темное\",\n" +
        "]"
