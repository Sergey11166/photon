package ru.sevoro.photon.matcher;

import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.support.annotation.ColorRes;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.TextView;

import org.hamcrest.Description;
import org.hamcrest.TypeSafeMatcher;

import java.util.Objects;

import ru.sevoro.photon.util.L;

/**
 * @author Sergey Vorobyev
 */

public class TextColorMatcher extends TypeSafeMatcher<View> {

    @ColorRes
    private final int expectedId;
    private String resourceName;

    TextColorMatcher(@ColorRes int expectedId) {
        super(View.class);
        this.expectedId = expectedId;
    }

    @Override
    protected boolean matchesSafely(View target) {
        if (!(target instanceof TextView)) return false;
        Resources resources = target.getContext().getResources();
        ColorStateList expectedColor = ContextCompat.getColorStateList(target.getContext(), expectedId);
        resourceName = resources.getResourceEntryName(expectedId);

        if (expectedColor == null) {
            L.d("expectedColor == null");
            return false;
        }

        ColorStateList targetColor = ((TextView)target).getTextColors();
        if (targetColor == null) {
            L.d("targetColor == null");
            return false;
        }

        return Objects.equals(expectedColor, targetColor);
    }

    @Override
    public void describeTo(Description description) {
        description.appendText("with textColor from resource id: ");
        description.appendValue(expectedId);
        if (resourceName != null) {
            description.appendText("[");
            description.appendText(resourceName);
            description.appendText("]");
        }
    }
}
