package ru.sevoro.photon.matcher;

import android.support.annotation.ColorRes;
import android.support.annotation.DrawableRes;
import android.view.View;

import org.hamcrest.Matcher;

/**
 * @author Sergey Vorobyev
 */

public class EspressoTestsMatchers {

    public static Matcher<View> withDrawable(@DrawableRes final int resourceId) {
        return new DrawableMatcher(resourceId);
    }

    public static Matcher<View> noDrawable() {
        return new DrawableMatcher(DrawableMatcher.EMPTY);
    }

    public static Matcher<View> hasDrawable() {
        return new DrawableMatcher(DrawableMatcher.ANY);
    }

    public static Matcher<View> hasTint(@ColorRes final int colorId) {
        return new SelectedMatcher(colorId);
    }

    public static Matcher<View> withBackgroundDrawable(@DrawableRes final int resourceId) {
        return new BackgroundDrawableMatcher(resourceId);
    }

    public static Matcher<View> withTextColor(@ColorRes final int resourceId) {
        return new TextColorMatcher(resourceId);
    }
}
