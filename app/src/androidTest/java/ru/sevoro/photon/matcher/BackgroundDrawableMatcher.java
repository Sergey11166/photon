package ru.sevoro.photon.matcher;

import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.support.annotation.DrawableRes;
import android.view.View;

import org.hamcrest.Description;
import org.hamcrest.TypeSafeMatcher;

import java.util.Objects;

/**
 * @author Sergey Vorobyev
 */

public class BackgroundDrawableMatcher extends TypeSafeMatcher<View> {

    @DrawableRes
    private final int expectedId;
    private String resourceName;

    BackgroundDrawableMatcher(@DrawableRes int expectedId) {
        super(View.class);
        this.expectedId = expectedId;
    }

    @Override
    protected boolean matchesSafely(View target) {
        Resources resources = target.getContext().getResources();
        Drawable expectedDrawable = resources.getDrawable(expectedId);
        Drawable drawable = target.getBackground();
        resourceName = resources.getResourceEntryName(expectedId);

        if (expectedDrawable == null || drawable == null) return false;

        Drawable.ConstantState state = drawable.getConstantState();
        Drawable.ConstantState expectedState = expectedDrawable.getConstantState();

        return Objects.equals(state, expectedState);
    }

    @Override
    public void describeTo(Description description) {
        description.appendText("with drawable from resource id: ");
        description.appendValue(expectedId);
        if (resourceName != null) {
            description.appendText("[");
            description.appendText(resourceName);
            description.appendText("]");
        }
    }
}
