package ru.sevoro.photon;

import android.os.SystemClock;
import android.support.test.espresso.contrib.RecyclerViewActions;
import android.support.test.rule.ActivityTestRule;
import android.support.v7.view.menu.ActionMenuItemView;
import android.widget.EditText;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import okhttp3.mockwebserver.Dispatcher;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import okhttp3.mockwebserver.RecordedRequest;
import ru.sevoro.photon.dagger.AppModule;
import ru.sevoro.photon.dagger.DaggerAppComponent;
import ru.sevoro.photon.dagger.LocalModule;
import ru.sevoro.photon.dagger.TestNetworkModule;
import ru.sevoro.photon.screen.root.RootActivity;

import static android.support.test.espresso.Espresso.closeSoftKeyboard;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.Espresso.pressBack;
import static android.support.test.espresso.action.ViewActions.clearText;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.scrollTo;
import static android.support.test.espresso.action.ViewActions.typeTextIntoFocusedView;
import static android.support.test.espresso.assertion.ViewAssertions.doesNotExist;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.hasDescendant;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withParent;
import static android.support.test.espresso.matcher.ViewMatchers.withParentIndex;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.instanceOf;
import static ru.sevoro.photon.MockResponsesKt.ALBUM_RESPONSE;
import static ru.sevoro.photon.MockResponsesKt.GET_ALL_PHOTO_CARDS;
import static ru.sevoro.photon.MockResponsesKt.GET_USER;
import static ru.sevoro.photon.MockResponsesKt.TAGS_RESPONSE;
import static ru.sevoro.photon.data.PreferencesManagerKt.DEFAULT_LAST_MODIFIED;
import static ru.sevoro.photon.data.transformers.RestCallTransformerKt.LAST_MODIFIED_HEADER;
import static ru.sevoro.photon.matcher.EspressoTestsMatchers.withBackgroundDrawable;
import static ru.sevoro.photon.matcher.EspressoTestsMatchers.withDrawable;

/**
 * @author Sergey Vorobyev
 */

public class InstrumentalTest {

    private static final String CORRECT_EMAIL = "mail@mail.com";
    private static final String CORRECT_PASSWORD = "12345678";
    private static final String CORRECT_LOGIN = "Login";
    private static final String CORRECT_USERNAME = "USERNAME";
    private static final String CORRECT_ALBUM_NAME = "ALBUM";
    private static final String CORRECT_ALBUM_DESCRIPTION = "ALBUM_DESCRIPTION";

    @Rule
    public ActivityTestRule<RootActivity> activityTestRule =
            new ActivityTestRule<>(RootActivity.class, true, false);
    private MockWebServer mockWebServer = new MockWebServer();

    @Before
    public void setUp() throws Exception {
        App.appComponent = DaggerAppComponent.builder()
                .networkModule(new TestNetworkModule(mockWebServer.url("").toString()))
                .localModule(new LocalModule())
                .appModule(new AppModule(App.instance))
                .build();

        prepareDispatcherSplashScreen();

        App.appComponent.preferencesManager().deleteToken();
        activityTestRule.launchActivity(null);
        SystemClock.sleep(3100);
    }

    @After
    public void tearDown() throws Exception {
        mockWebServer.shutdown();
    }

    @Test
    public void test() throws Exception {

        homeScreen();
        searchScreen();
        photoCardScreen();
        authorScreen();
        albumScreen();
        profileScreen();
    }

    private void homeScreen() {

        // bottom navigation
        onView(withId(R.id.bottom_navigation)).check(matches(isDisplayed()));
        onView(withId(R.id.nav_home)).check(matches(isDisplayed()));
        onView(withId(R.id.nav_profile)).check(matches(isDisplayed()));
        onView(withId(R.id.nav_upload)).check(matches(isDisplayed()));

        // photocard item
        onView(withId(R.id.recycler))
                .check(matches(hasDescendant(withDrawable(R.drawable.ic_custom_favorites_white_24dp))));

        // toolbar
        onView(withText(R.string.app_name)).check(matches(isDisplayed()));
        onView(withDrawable(R.drawable.ic_custom_gear_black_24dp)).check(matches(isDisplayed()));
        //onView(withDrawable(R.drawable.ic_custom_search_black_24dp)).check(matches(isDisplayed()));
        onView(withDrawable(R.drawable.ic_custom_gear_black_24dp)).perform(click());
        onView(withText(R.string.main_menu_sign_in)).check(matches(isDisplayed()));
        onView(withText(R.string.main_menu_sign_up)).check(matches(isDisplayed()));

        // signIn dialog
        onView(withText(R.string.main_menu_sign_in)).perform(click());
        onView(withId(R.id.email)).check(matches(isDisplayed()));
        onView(withText(R.string.auth_error_email)).check(matches(isDisplayed()));
        onView(withId(R.id.password)).check(matches(isDisplayed()));
        onView(withText(R.string.auth_error_password)).check(matches(isDisplayed()));
        onView(withId(R.id.ok_button)).check(matches(isDisplayed()));
        onView(withId(R.id.cancel_button)).check(matches(isDisplayed()));
        onView(withId(R.id.email)).perform(click());
        onView(withId(R.id.email)).perform(typeTextIntoFocusedView(CORRECT_EMAIL));
        closeSoftKeyboard();
        onView(withId(R.string.auth_error_email)).check(doesNotExist());
        onView(withId(R.id.password)).perform(click());
        onView(withId(R.id.password)).perform(typeTextIntoFocusedView(CORRECT_PASSWORD));
        closeSoftKeyboard();
        onView(withId(R.string.auth_error_password)).check(doesNotExist());
        onView(withId(R.id.ok_button)).perform(click());
        onView(allOf(withId(android.support.design.R.id.snackbar_text),
                withText(R.string.auth_message_logged_in))).check(matches(isDisplayed()));

        // logout
        onView(withDrawable(R.drawable.ic_custom_gear_black_24dp)).perform(click());
        onView(withText(R.string.main_menu_logout)).perform(click());
        onView(allOf(withId(android.support.design.R.id.snackbar_text),
                withText(R.string.auth_message_logged_out))).check(matches(isDisplayed()));

        // signUp dialog
        onView(withDrawable(R.drawable.ic_custom_gear_black_24dp)).perform(click());
        onView(withText(R.string.main_menu_sign_up)).perform(click());
        onView(withId(R.id.login)).check(matches(isDisplayed()));
        onView(withText(R.string.auth_error_login)).check(matches(isDisplayed()));
        onView(withId(R.id.email)).check(matches(isDisplayed()));
        onView(withText(R.string.auth_error_email)).check(matches(isDisplayed()));
        onView(withId(R.id.username)).check(matches(isDisplayed()));
        onView(withText(R.string.auth_error_username)).check(matches(isDisplayed()));
        onView(withId(R.id.password)).check(matches(isDisplayed()));
        onView(withText(R.string.auth_error_password)).check(matches(isDisplayed()));
        onView(withId(R.id.ok_button)).check(matches(isDisplayed()));
        onView(withId(R.id.login)).perform(click());
        onView(withId(R.id.login)).perform(typeTextIntoFocusedView(CORRECT_LOGIN));
        closeSoftKeyboard();
        onView(withText(R.string.auth_error_login)).check(doesNotExist());
        onView(withId(R.id.email)).perform(click());
        onView(withId(R.id.email)).perform(typeTextIntoFocusedView(CORRECT_EMAIL));
        closeSoftKeyboard();
        onView(withText(R.string.auth_error_email)).check(doesNotExist());
        onView(withId(R.id.username)).perform(click());
        onView(withId(R.id.username)).perform(typeTextIntoFocusedView(CORRECT_USERNAME));
        closeSoftKeyboard();
        onView(withText(R.string.auth_error_username)).check(doesNotExist());
        onView(withId(R.id.password)).perform(click());
        onView(withId(R.id.password)).perform(typeTextIntoFocusedView(CORRECT_PASSWORD));
        closeSoftKeyboard();
        onView(withText(R.string.auth_error_password)).check(doesNotExist());
        onView(withId(R.id.ok_button)).perform(click());
        onView(allOf(withId(android.support.design.R.id.snackbar_text),
                withText(R.string.auth_message_logged_in))).check(matches(isDisplayed()));
    }

    private void searchScreen() {
        onView(instanceOf(ActionMenuItemView.class)).perform(click());
        onView(withText(R.string.search_tab)).check(matches(isDisplayed()));
        onView(withText(R.string.search_tab_filters)).check(matches(isDisplayed()));

        onView(withText(R.string.search_search_input_hint)).check(matches(isDisplayed()));
        onView(withId(R.id.search_input)).perform(click());
        onView(withId(R.id.search_input)).perform(typeTextIntoFocusedView("meat"));
        closeSoftKeyboard();
        onView(withDrawable(R.drawable.ic_custom_cancel_black_24dp)).perform(click());
        onView(withText(R.string.search_search_input_hint)).check(matches(isDisplayed()));
        onView(withId(R.id.search_input)).perform(click());
        onView(withId(R.id.search_input)).perform(typeTextIntoFocusedView("meat"));
        closeSoftKeyboard();

        onView(withId(R.id.search_checked_button)).perform(click());
        onView(allOf(withId(android.support.design.R.id.snackbar_text),
                withText(R.string.message_filters_applied))).check(matches(isDisplayed()));
        SystemClock.sleep(3000);
        onView(instanceOf(ActionMenuItemView.class)).perform(click());
        onView(allOf(instanceOf(EditText.class), withText("meat"))).check(matches(isDisplayed()));
        onView(withId(R.id.search_promts)).check(matches(hasDescendant(withText("meat"))));
        onView(withDrawable(R.drawable.ic_custom_cancel_black_24dp)).perform(click());
        onView(withText(R.string.search_search_input_hint)).check(matches(isDisplayed()));
        onView(withId(R.id.search_checked_button)).perform(click());
        onView(instanceOf(ActionMenuItemView.class)).perform(click());

        onView(allOf(
                withParent(withId(R.id.tags_container)),
                withParentIndex(0),
                withBackgroundDrawable(R.drawable.border_tag)
        )).check(matches(isDisplayed()));

        onView(allOf(withParent(withId(R.id.tags_container)), withParentIndex(0))).perform(click());

        onView(allOf(
                withParent(withId(R.id.tags_container)),
                withParentIndex(0),
                withBackgroundDrawable(R.drawable.border_tag_accent)
        )).check(matches(isDisplayed()));

        onView(withId(R.id.search_button)).perform(scrollTo(), click());
        onView(allOf(withId(android.support.design.R.id.snackbar_text),
                withText(R.string.message_filters_applied))).check(matches(isDisplayed()));
        onView(withText(R.string.snack_bar_action_reset_filters)).perform(click());
    }

    private void photoCardScreen() {
        onView(withId(R.id.recycler)).perform(RecyclerViewActions.actionOnItemAtPosition(0, click()));
        onView(withDrawable(R.drawable.ic_custom_back_black_24dp)).check(matches(isDisplayed()));
        onView(withText(R.string.photocard_title)).check(matches(isDisplayed()));
        onView(withId(R.id.photo)).check(matches(isDisplayed()));
        onView(withId(R.id.title)).check(matches(isDisplayed()));
        onView(withId(R.id.avatar)).check(matches(isDisplayed()));
        onView(withId(R.id.username)).check(matches(isDisplayed()));
        onView(withText(R.string.profile_albums)).check(matches(isDisplayed()));
        onView(withText(R.string.profile_cards)).check(matches(isDisplayed()));
        onView(withId(R.id.album_count)).check(matches(isDisplayed()));
        onView(withId(R.id.cards_count)).check(matches(isDisplayed()));
        onView(withId(R.id.tags_container)).check(matches(isDisplayed()));

        // overflow menu
        onView(withDrawable(R.drawable.ic_custom_menu_black_24dp)).perform(click());
        onView(withText(R.string.photocard_menu_favorite)).perform(click());
        onView(allOf(withId(android.support.design.R.id.snackbar_text),
                withText(R.string.photocard_message_already_is_favorite))).check(matches(isDisplayed()));
        onView(withDrawable(R.drawable.ic_custom_menu_black_24dp)).perform(click());
        onView(withText(R.string.photocard_menu_share)).check(matches(isDisplayed()));
        onView(withText(R.string.photocard_menu_save_to_gallery)).check(matches(isDisplayed()));
        pressBack();
    }

    private void authorScreen() {
        // user info
        onView(withId(R.id.avatar)).perform(click());
        onView(withDrawable(R.drawable.ic_custom_back_black_24dp)).check(matches(isDisplayed()));
        onView(withText(R.string.author_title)).check(matches(isDisplayed()));
        onView(withId(R.id.avatar)).check(matches(isDisplayed()));
        onView(withId(R.id.login)).check(matches(isDisplayed()));
        onView(withId(R.id.username)).check(matches(isDisplayed()));
        onView(withText(R.string.profile_albums)).check(matches(isDisplayed()));
        onView(allOf(withParent(withId(R.id.content)), withText(R.string.profile_cards))).check(matches(isDisplayed()));
        onView(withId(R.id.album_count)).check(matches(isDisplayed()));
        onView(withId(R.id.cards_count)).check(matches(isDisplayed()));

        // album item
        onView(withId(R.id.albums_container)).check(matches(hasDescendant(withText("Избранное"))));
        onView(withId(R.id.albums_container)).check(matches(hasDescendant(withText(R.string.profile_cards))));
        onView(withId(R.id.albums_container)).check(matches(hasDescendant(withDrawable(R.drawable.ic_custom_favorites_white_24dp))));
        onView(withId(R.id.albums_container)).check(matches(hasDescendant(withDrawable(R.drawable.ic_custom_views_white_24dp))));
        onView(withId(R.id.albums_container)).check(matches(hasDescendant(withId(R.id.favorites))));
        onView(withId(R.id.albums_container)).check(matches(hasDescendant(withId(R.id.views))));
    }

    private void albumScreen() {
        // album info
        onView(allOf(withParent(withId(R.id.albums_container)), withParentIndex(0))).perform(click());
        onView(withDrawable(R.drawable.ic_custom_back_black_24dp)).check(matches(isDisplayed()));
        onView(withText(R.string.album_title)).check(matches(isDisplayed()));
        onView(withText("Избранное")).check(matches(isDisplayed()));
        onView(withText(R.string.profile_cards)).check(matches(isDisplayed()));
        onView(withId(R.id.cards_count)).check(matches(isDisplayed()));
        onView(withText("Альбом для понравившихся фотографий")).check(matches(isDisplayed()));

        // photo card item
        onView(allOf(withParent(withId(R.id.cards_container)), withParentIndex(0))).perform(click());
        onView(withText(R.string.photocard_title)).check(matches(isDisplayed()));
        pressBack();
    }

    private void profileScreen() {
        // toolbar
        onView(withId(R.id.nav_profile)).perform(click());
        onView(withId(R.id.toolbar)).check(matches(hasDescendant(withText(R.string.profile_title))));
        onView(withDrawable(R.drawable.ic_custom_menu_black_24dp)).perform(click());

        // logout
        onView(withDrawable(R.drawable.ic_custom_circular_power_on_black_24dp)).check(matches(isDisplayed()));
        onView(withText(R.string.profile_menu_logout)).perform(click());
        onView(withDrawable(R.drawable.ic_custom_menu_black_24dp)).check(doesNotExist());
        onView(withText(R.string.profile_not_authorized_description)).check(matches(isDisplayed()));
        onView(withId(R.id.sign_up_button)).perform(click());
        onView(withText(R.string.sign_up_title)).check(matches(isDisplayed()));
        closeSoftKeyboard();
        onView(withId(R.id.cancel_button)).perform(click());
        onView(withId(R.id.sign_in_button)).perform(click());
        onView(withId(R.id.email)).perform(click());
        onView(withId(R.id.email)).perform(typeTextIntoFocusedView(CORRECT_EMAIL));
        closeSoftKeyboard();
        onView(withId(R.id.password)).perform(click());
        onView(withId(R.id.password)).perform(typeTextIntoFocusedView(CORRECT_PASSWORD));
        closeSoftKeyboard();
        onView(withId(R.id.ok_button)).perform(click());

        // create album
        onView(withDrawable(R.drawable.ic_custom_menu_black_24dp)).perform(click());
        onView(withDrawable(R.drawable.ic_custom_add_to_album_black_24dp)).check(matches(isDisplayed()));
        onView(withText(R.string.profile_menu_new_album)).perform(click());
        onView(withText(R.string.new_album_title)).check(matches(isDisplayed()));
        onView(withId(R.id.cancel_button)).check(matches(isDisplayed()));
        onView(withText(R.string.profile_error_name)).check(matches(isDisplayed()));
        onView(withId(R.id.name)).perform(click());
        onView(withId(R.id.name)).perform(typeTextIntoFocusedView(CORRECT_ALBUM_NAME));
        closeSoftKeyboard();
        onView(withText(R.string.profile_error_name)).check(doesNotExist());
        onView(withText(R.string.profile_error_description)).check(matches(isDisplayed()));
        onView(withId(R.id.description)).perform(click());
        onView(withId(R.id.description)).perform(typeTextIntoFocusedView(CORRECT_ALBUM_DESCRIPTION));
        closeSoftKeyboard();
        onView(withText(R.string.profile_error_description)).check(doesNotExist());
        onView(withId(R.id.ok_button)).perform(click());
        onView(withId(R.id.albums_container)).check(matches(hasDescendant(withText(CORRECT_ALBUM_NAME))));

        // edit profile
        onView(withDrawable(R.drawable.ic_custom_menu_black_24dp)).perform(click());
        onView(withDrawable(R.drawable.ic_custom_edit_black_24dp)).check(matches(isDisplayed()));
        onView(withText(R.string.profile_menu_edit_profile)).perform(click());
        onView(withText(R.string.edit_profile_title)).check(matches(isDisplayed()));
        onView(withId(R.id.cancel_button)).check(matches(isDisplayed()));
        onView(withText(R.string.auth_error_login)).check(doesNotExist());
        onView(withText(R.string.auth_error_username)).check(doesNotExist());
        onView(withId(R.id.login)).perform(clearText());
        onView(withId(R.id.username)).perform(clearText());
        onView(withText(R.string.auth_error_login)).check(matches(isDisplayed()));
        onView(withText(R.string.auth_error_username)).check(matches(isDisplayed()));
        onView(withId(R.id.login)).perform(click());
        onView(withId(R.id.login)).perform(typeTextIntoFocusedView(CORRECT_LOGIN));
        closeSoftKeyboard();
        onView(withId(R.id.username)).perform(click());
        onView(withId(R.id.username)).perform(typeTextIntoFocusedView(CORRECT_USERNAME));
        closeSoftKeyboard();
        onView(withText(R.string.auth_error_login)).check(doesNotExist());
        onView(withText(R.string.auth_error_username)).check(doesNotExist());
        onView(withId(R.id.ok_button)).perform(click());
        onView(withText(CORRECT_LOGIN)).check(matches(isDisplayed()));
        onView(withText("/ " + CORRECT_USERNAME)).check(matches(isDisplayed()));

        // change avatar
        onView(withDrawable(R.drawable.ic_custom_menu_black_24dp)).perform(click());
        onView(withDrawable(R.drawable.ic_custom_change_avatar_black_24dp)).check(matches(isDisplayed()));
        onView(withText(R.string.profile_menu_change_avatar)).perform(click());
        onView(withText("Сделать снимок")).check(matches(isDisplayed()));
        onView(withText("Выбрать из галереи")).check(matches(isDisplayed()));
        onView(withText("Отмена")).check(matches(isDisplayed()));
        pressBack();
    }

    private void prepareDispatcherSplashScreen() {
        final Dispatcher dispatcher = new Dispatcher() {
            @Override
            public MockResponse dispatch(RecordedRequest request) throws InterruptedException {
                MockResponse response = new MockResponse();
                switch (request.getPath()) {
                    case "/photocard/list?limit=0":
                    case "/photocard/list?limit=60":
                        return response
                                .setResponseCode(200)
                                .setHeader(LAST_MODIFIED_HEADER, DEFAULT_LAST_MODIFIED)
                                .setBody(GET_ALL_PHOTO_CARDS);

                    case "/user/595aa0930e55f63612195842":
                    case "/user/SignIn":
                    case "/user/SignUp":
                        return response
                                .setResponseCode(200)
                                .setBody(GET_USER);

                    case "/user/595aa0930e55f63612195842/album":
                        return response
                                .setResponseCode(200)
                                .setBody(ALBUM_RESPONSE);

                    case "/photocard/tags":
                        return response.setResponseCode(200)
                                .setBody(TAGS_RESPONSE);

                    default:
                        return response.setResponseCode(404);
                }
            }
        };
        mockWebServer.setDispatcher(dispatcher);
    }
}
