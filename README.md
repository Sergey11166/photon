## Выпускная работа с [курсов по андроид разработке](http://www.skill-branch.ru/android) ##

## [Техническое задание](https://docs.google.com/document/d/1a9S6txkxCWEDtSI5omuzOPLjuYVjE7r5gXTPBTBsTAc) ##

## Используемые технологии: ##
- Java
- Kotlin
- MVP
- DataBinding
- Dagger2
- Flow
- Mortar
- RealmDB
- RxJava
- Espresso
- Circle CI

![1.png](screenshot/Screenshot_20180219-132711.png)
![2.png](screenshot/Screenshot_20180219-132717.png)
![3.png](screenshot/Screenshot_20180219-132742.png)
![4.png](screenshot/Screenshot_20180219-132821.png)
![5.png](screenshot/Screenshot_20180219-132836.png)
![6.png](screenshot/Screenshot_20180219-132920.png)
![7.png](screenshot/Screenshot_20180219-132932.png)
![8.png](screenshot/Screenshot_20180219-133028.png)

